$(document).on('submit', '.comment-submit-form', function (e) {
    e.preventDefault();
    var comment_list = $(this).siblings('.comment-list');
    var error_message = $(this).children('.error');
    var commentInput = $(this).children('textarea[name="comment"]');
    var _token = $(this).children('input[name="_token"]').val();
    var comment = commentInput.val();
    if (comment == '') {
        $(this).children('.error').css({color: 'red'}).text('Comment is empty!').show();
        return false;
    } else {
        $(this).children('.error').hide();
    }
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: {
            _token: _token,
            comment: comment
        },
        success: function (data) {
            error_message.css({color: 'green'}).text(data.message).show();
            // comment_list.prepend('<div class="comment-block"><div class="comment-wrapper"><div class="comment-col-10"><div class="comment-content"><div class="comment-head"><span class="name">' + data.user.full_name + '</span><span class="date">' + data.created_at + '</span></div><div class="comment"><p>' + data.comment.comment + '</p></div><div class="like" style="margin-top: 15px;"><button type="button" class="fa fa-thumbs-o-up btn-like-comment" data-href="' + data.comment_like_url + '" style="font-size: 16px; border: none; background-color: transparent; cursor: pointer;"></button> <span>&middot;</span> <span class="like-count">0</span> <span>Likes</span></div></div></div></div></div>');
            commentInput.val('');
            return false;
        },
        error: function (response) {
            var data = JSON.parse(response.responseText);
            error_message.css({color: 'red'}).text(data.message).show();
            return false;
        }
    });
});

$(document).on('click', '.btn-like-comment', function(e) {
    e.preventDefault();
    var btn = $(this);
    var count = $(this).siblings('.like-count');
    var href = $(this).data('href');
    $.ajax({
        type: 'GET',
        url: href,
        success: function (data) {
            if (data.status == 1) {
                btn.removeClass('fa-thumbs-o-up').addClass('fa-thumbs-up');
            } else {
                btn.removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');
            }
            count.text(data.count);
            return false;
        }
    });
});
