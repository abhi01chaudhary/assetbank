
$(function(){

	//Responsive Toggle
	 $('.toggle-head').click(function () {
      $(this).toggleClass('on');     
      $(this).parent().siblings().find(".toggle-head").removeClass("on");
      $(this).parent(".toggle").find(".toggle-body").slideToggle("medium");
      $(this).parent().siblings().find(".toggle-body").slideUp("medium");
      return false;
    });

	 //list toggle
     $('.list-toggle').click(function () {
      $(this).toggleClass('on');     
      $(this).parent().siblings().find(".list-toggle").removeClass("on");
      $(this).parent(".list-toggle-wrapper").find(".list-body").slideToggle("medium");
      $(this).parent().siblings().find(".list-body").slideUp("medium");
      return false;
    });


	 //Banner Image Resize
	$(window).resize(function() {
	    $('#login-banner').height($(window).height() - $('#login-banner').offset().top);
	});

	$(window).trigger('resize');

});