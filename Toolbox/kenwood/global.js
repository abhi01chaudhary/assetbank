var isLtIE7 = /*@cc_on@*//*@if (@_jscript_version < 5.7)1; /*@end@*/0;

/**
* --------------------------------------------------------------------
* Author: Marcela Maneos, Netcel
* --------------------------------------------------------------------
*/

window._gaq = window._gaq || [];//prevent tracking errors

var KENWOOD = {
    modal: {
        overlay: function () {
            //run only if there are overlay items in-page
            if ($('.triggerOverlay').size() > 0) {

                //Show the parent as it could of been hidden by default
                $('.triggerOverlay').parent().show();

                //add close link
                var closeBtn = $('#overlay a.close');
                var closeBtnTxt = closeBtn.attr('title');
                closeBtn.prepend('<span>' + closeBtnTxt + '</span>');

                $('body').on('click', '#overlay a.close', function (e) { e.preventDefault(); })

                var invervalId = null;
                var exposeMask = null,
					$body = $('html');

                //assign the jQuery Tools overlay plugin
                $(".triggerOverlay").overlay({
                    top: '0',
                    mask: {
                        color: '#000',
                        loadSpeed: 200,
                        opacity: 0.7
                    },
                    target: '#overlay',
                    fixed: false,

                    onLoad: function () {
                        var wrap = this.getOverlay().find('.contentWrap');
                        var overlay = this;

                        exposeMask = exposeMask || $('#exposeMask');

                        if (invervalId == null) {
                            invervalId = setInterval(function () {
                                if (exposeMask.css('display') != 'none') {
                                    exposeMask.css('height', '');
                                    exposeMask.height($(document).height())
                                }
                            }, 1000 / 60);
                        }

                        if (wrap.size() > 0) {
                            var $trigger = this.getTrigger(),
								$input = $trigger.parent().find('input'),
								overlayURL,
								overlayMode,
								overlayTitle = $input.attr('title');

                            if ($input.length != 0) {
                                overlayURL = $input.attr('value')
                                overlayMode = $input.attr('class');
                            } else {
                                overlayURL = $trigger.attr('href') + ' .frame.media';
                                overlayMode = $trigger.attr('data-mode') || 'PMM';
                                overlayTitle = $trigger.attr('data-title') || '';
                            }

                            if (overlayURL != null) {
                                if (overlayMode == "" || overlayMode == "PMM") {
                                    $(wrap).load(overlayURL,
                                    function () {
                                        $('#exposeMask').height($(document).height());

                                        if (overlayMode == "PMM") {
                                            setupMultiMedia(overlay.getOverlay());
                                        }
                                    });
                                }
                                else if (overlayMode == "PCP") {
                                    //Product Comparison from multiple product listing
                                    var productIds = getSelectedProductIds();

                                    if (productIds.length > 0 && productIds.length < 4) {
                                        overlayURL = overlayURL + "&selprod=" + productIds.join(",");
                                        $(wrap).load(overlayURL);
                                    }
                                    else {
                                        $(wrap).html("<div class='media frame'>" + $("div#errorContainer").html() + "</div>");
                                    }
                                }
                                else if (overlayMode == "VL") {
                                    $(wrap).load(overlayURL,
                                    function () {
                                        setupVideoGallery();
                                    });
                                }
                                else if (overlayMode == "youtube") {
                                    $(wrap).html("<div class='media frame'><iframe src='" + overlayURL + "' width='640' height='480' class='youtube' frameborder='0' allowfullscreen></iframe></div>");
                                }
                                else if (overlayMode == "videomark") {
                                    $(wrap).html("<div class='media frame'><iframe src='" + overlayURL + "&height=407&width=714&KeepThis=true' width='740' height='465' frameborder='0'></iframe></div>");
                                }
                                else if (overlayMode == "etale") {
                                    $(wrap).html("<div class='media etale frame'><h3>" + overlayTitle + "</h3><iframe src='" + overlayURL + "' class='etale' frameborder='0'></iframe></div>");
                                }
                            }
                        }
                    },
                    onClose: function (evt) {
                        this.getOverlay().trigger('dispose').find('.contentWrap').empty();

                        if (invervalId != null) {
                            clearInterval(invervalId);
                            invervalId = null;
                        }
                    }
                });

            }
        },

        internalLink: function (el) {
            $(el).click(function (evt) {
                evt.preventDefault();
                var wrap = $('#overlay .contentWrap');
                var newURL = $(this).find('input').attr('value');
                if (wrap.size() > 0 && newURL != null) {
                    wrap.empty();
                    wrap.load(newURL);
                }
            });
        }
    },


    widgets: {

        relatedContent: function (el) {
            var el = $(el);
            el.each(function () {
                var $collapsibleHeading = $('.rTabs li', $(this));
                var $collapsibleContent = $('.js-body', $(this));
                var closeButtonText = $('.js-close', $collapsibleContent).attr('title');
                $('<a href="#" class="close" title="' + closeButtonText + '">' + closeButtonText + '</a>').prependTo($collapsibleContent);

                //add index classes
                $collapsibleHeading.each(function (i) {
                    $(this).attr('rel', 'jsID' + i).addClass('collapsed');
                });
                $collapsibleContent.each(function (i) {
                    $(this).wrapInner('<div class="jsID' + i + '"></div>');
                });

                function closeTab(tab, id) {
                    $('.' + id, el).parent().slideUp();
                    tab.addClass('collapsed').removeClass('selected');
                };
                function openTab(tab, id) {
                    $('.' + id, el).parent().slideDown();
                    tab.removeClass('collapsed').addClass('selected');
                    $('.' + id, el).find('ul li:first a').focus();
                };
                function closeAll() {
                    $collapsibleContent.slideUp();
                    $collapsibleHeading.addClass('collapsed').removeClass('selected');
                }

                $('a.close', el).click(function () {
                    closeAll();
                    return false;
                });

                $collapsibleHeading.click(function () {
                    var thisBody = $(this).attr('rel');
                    if ($(this).is('.collapsed')) {
                        closeAll();
                        openTab($(this), thisBody);
                    } else {
                        closeTab($(this), thisBody);
                    }
                    return false;
                });

            });
        },

        /* top navigation */
        topNavDropDowns: function (el) {
            if ($(el).length <= 0) return false;
            var tabLI = $(el);

            var dropdown = $('.dropdown', tabLI);
            var sectionNav = $('ul.sectionNav', dropdown);
            var hoverLI = $('> li', sectionNav);

            hoverLI.each(function () {

                var overviewBlock = $(this).parent().parent().find('div.overview');
                var overviewBlockTxt = $(overviewBlock).html();
                var productNav = $(this).find('div.productNav');
                var productNavTxt = $(productNav).html();
                var jsNav = $(this).find('div.jsNav');


                $(this).hover(function () {
                    if (productNavTxt) {
                        $(this).find('>a').addClass('selected');
                        productNav.show();
                        overviewBlock.hide();

                        var jsNavHeight = jsNav.height();
                        var thisHeight = $(this).parent('ul.sectionNav');

                        if (jsNavHeight > thisHeight.height()) {
                            thisHeight.height(jsNavHeight)
                        }
                    }
                }, function () {
                    if (productNavTxt) {
                        $('div.productNav', tabLI).hide();
                        $('a.selected', tabLI).removeClass('selected');
                        overviewBlock.show();
                    }
                });
            });


        },
        /* @end */


        /* country selector */
        countrySelector: function (el) {
            var lang = $(el);
            $('a.selected', lang).click(function () {
                if ($(this).is('.opened')) {
                    $(this).removeClass('opened').next('div.selectLang').attr('aria-hidden', true).slideUp();
                } else {
                    $(this).addClass('opened').next('div.selectLang').attr('aria-hidden', false).slideDown();
                }
                return false;
            });

            //close link
            var close = $('<a href="#" role="button" title="' + $('p.close').attr('title') + '"></a>').prependTo('.lang p.close');

            //custom close event
            lang.bind('close', function () {
                $(this).find('a.selected').removeClass('opened').end().find('.selectLang').slideUp().removeClass('opened');
                return false;
            });

            //close on esc key
            $(document).bind('keydown.el', function (ev) {
                if (ev.which == 27) { lang.trigger('close'); }
            });

            //close button click
            close.click(function () {
                lang.trigger('close');
                return false;
            });
        },


        //show more info drop-down baxes (product sub-category)
        showMoreInfo: function (el) {
            var el = $(el).filter(':not(.ko-template, .ko-template *)'); // .frameWrap .moreInfo
            var btn = $('<a href="#" class="js-btn" role="button">More info</a>')
            el.attr('aria-hidden', false);
            $('.frameWrap:not(.ko-template, .ko-template *)').prepend(btn);
            var toggleButton = $('.frameWrap .js-btn');

            //events
            toggleButton
				.bind('collapse', function () {
				    $(this)
					.removeClass('expanded')
					.parent('.frameWrap').find('.moreInfo').slideUp(function () {
					    $(this).attr('aria-hidden', true);
					});
				})
				.bind('expand', function () {
				    $(this)
					.addClass('expanded')
					.parent('.frameWrap').find('.moreInfo').slideDown(function () {
					    $(this).attr('aria-hidden', false);
					});
				})
				.click(function () {
				    if ($(this).is('.expanded')) {
				        $(this).trigger('collapse');
				    } else {
				        toggleButton.trigger('collapse');
				        $(this).trigger('expand');
				    }
				    return false;
				});
        },

        /* select category */
        selectCategory: function (el) {
            var cat = $(el);

            $('a.selected', cat).click(function () {
                if ($(this).is('.opened')) {
                    $(this).removeClass('opened').parent(cat).find('.dropDown').attr('aria-hidden', true).slideUp();
                } else {
                    $(this).addClass('opened').parent(cat).find('.dropDown').attr('aria-hidden', false).slideDown();
                }
                return false;
            });

            //close link
            var close = $('<a href="#" role="button" title="' + $('p.close').attr('title') + '"></a>').prependTo('.dropDown p.close');

            //custom close event
            cat.bind('close', function () {
                $(this).find('a.selected').removeClass('opened').end().find('.dropDown').slideUp().removeClass('opened');
                return false;
            });

            //close on esc key
            $(document).bind('keydown.el', function (ev) {
                if (ev.which == 27) { cat.trigger('close'); }
            });

            //close button click
            close.click(function () {
                cat.trigger('close');
                return false;
            });
        },

        collapsibleContent: function (el) {
            var el = $(el);

            var urlHash = window.location.hash.slice(1);

            el.each(function () {
                var $collapsibleHeading = $('.js-header', $(this));
                var $collapsibleContent = $('.js-body', $(this));

                //compare tab ID with URL hash, if it's identical add the 'js-opened' class
                if (urlHash != '') {
                    if (($(this).find('.js-get-URL-Hash')).length) {
                        var thisID = $(this).find('.js-get-URL-Hash').attr('id');
                        if (urlHash == thisID) {
                            $collapsibleHeading.addClass('js-opened');
                            //Firebug bug: scroll to ID  
                            $('html, body').animate({ scrollTop: ($('#' + urlHash).offset().top) }, 1);
                        }
                    }
                };

                //modify markup & attributes
                $collapsibleHeading.wrapInner('<a href="#" role="button"></a>');

                //Collapse all except the ones with class="js-opened"
                $collapsibleHeading.each(function () {
                    if ($(this).is('.js-opened')) {
                        $(this).next($collapsibleContent).attr('aria-hidden', false);
                    } else {
                        $(this).addClass('collapsed').next($collapsibleContent).hide().attr('aria-hidden', true);
                    }
                });

                $collapsibleHeading.click(function () {
                    if ($(this).is('.collapsed')) {
                        $(this)
                            .removeClass('collapsed')
                            .next($collapsibleContent).slideDown(function () {
                                $(this).attr('aria-hidden', false);
                            });
                    } else {
                        $(this).addClass('collapsed');
                        $(this).next($collapsibleContent).slideUp(function () {
                            $(this).attr('aria-hidden', true);
                        });
                    }
                    return false;
                });
            });
        }, /* end collapsibleContent*/


        // Autosuggest
        autoSuggest: function (el) {
            el = $(el);

            var lang = document.documentElement.lang;
            var isOpen = false;

            el.autocomplete({
                minLength: 3,
                source: function (request, response) {
                    try {
                        //var url = '/Service/AutoSuggest.txt';//dev

                        var url = '/Service/AutoSuggest.ashx'//live

                        var data = {
                            term: request.term,
                            lang: lang,
                            myurl: $(location).attr('href')
                        };

                        $.ajax(url, { data: data, method: 'GET', dataType: 'json' }).then(
                            function (data) {
                                var output = [];

                                var results = data.s;

                                for (var i = 0, l = results.length; i < l; i++) {
                                    output.push(results[i]);
                                }

                                if (output.length == 1 && output[0] === '') {
                                    output.length = 0;
                                }

                                response(output);
                            },
                            function () {//lookup failed
                                if (console && console.log) {
                                    console.log('failure', arguments);
                                }
                                response([]);
                            }
                        );
                    }
                    catch (e) {
                        if (console && console.log) {
                            console.log('search lookup error: ', e);
                        }

                        response([]);
                    }
                },
                open: function (event, ui) {
                    var autocomplete = $(".ui-autocomplete");
                    var oldTop = autocomplete.offset().top;

                    var newTop = oldTop + 10;

                    autocomplete.css("top", newTop + 'px');

                    isOpen = true;
                },
                close: function () {
                    isOpen = false;
                }
            })
            .on("autocompleteselect", function (event, ui) {
                //alert( JSON.stringify( ui ) );
                el.val(ui.item.value);
                $('.siteSearch input[type="image"]').click();
            });

            $(window).resize(function () {
                if (isOpen) {
                    el.autocomplete("search");
                }
            });
        }
    },

    mobile: {
        mobileOnly: function () {

            //mobile: link scroll to top
            $('a.js-scrollTop').click(function () {
                var obj = $('html').scrollTop() !== 0 ? 'html' : 'body';
                $(obj).animate({ scrollTop: 0 }, "slow");
                //return false;
            });

            //mobile: top navigation
            var mobileTopNav = $('.js-accordion');
            var toggleLink = $('>li >a', mobileTopNav);
            var toggleBody = $('ul', mobileTopNav);
            toggleBody.hide().end().css('display', 'block');
            //Open/close item on click
            toggleLink.click(function () {
                $(this).toggleClass("active").next().slideToggle("slow");
                return false;
            });

            //mobile: search site
            var mobileSearch = $('#mobileSearch');
            $('a.js-mobile-search').click(function () {
                mobileSearch.animate({ 'top': '0' }, 500);
                mobileSearch.find('input.inputTxt').val('');
            });
            function closeMobileSearch() {
                mobileSearch.animate({ 'top': '-400px' }, 500);
            }
            $('a.js-mobile-search-close').click(function () {
                closeMobileSearch();
                return false;
            });
        }
    },

    generic: {
        printPage: function (el) {
            el = $(el);

            el.each(function (index, el) {
                var el = $(el);
                var val = el.attr('title');
                var printLink = el.find('a');

                if (printLink.length === 0) {
                    printLink = $('<a href="#">' + val + '</a>');
                    el.prepend(printLink);
                }

                printLink.click(function (e) {
                    this.blur();
                    if (window.print) window.print();
                    return false;
                });
            });
        },

        tableZebra: function (el) {
            if ($(el).length > 0) { //check if element exist
                var $el = $(el);
                $el.each(function () {
                    $(this).find('tr:even').addClass('line');
                });
            }
        },

        //Promotions 
        PromoBoxes: function (el) {
            if ($(el).length > 0) {
                var $el = $(el);


                $el.each(function () {
                    var promoSet = $(this);
                    var promoSetItems = $(this).find('li').length;

                    //helping classes 
                    promoSet.find('li').first().addClass('first');
                    if (promoSetItems >= 3) {
                        if (promoSet.parent('div.colMain').length) {
                            promoSet.find('li.promo-small:nth-child(3n+1)').addClass('first');
                        } else {
                            promoSet.find('li.promo-small:nth-child(4n+1)').addClass('first');
                        }
                    }

                    function syncHeights() {
                        //synch height of the elements              
                        var synchEl = promoSet.find('li a>div');
                        synchEl.css('min-height', 0);//clear min height

                        var maxHeight = 0, item;
                        synchEl.each(function () {
                            item = $(this);
                            if (item.height() > maxHeight) {
                                maxHeight = item.height();
                            }
                        });
                        synchEl.css({ 'min-height': maxHeight });
                    }

                    syncHeights();

                    promoSet.find('img').load(syncHeights);

                });

            }
        },

        //recipe details, keyfeatures OL list: add image numbers
        styleOrderedList: function (list) {
            if ($(list).length > 0) {
                var list = $(list);
                list.each(function () {
                    $(this).find('li').each(function (i) {
                        $(this).addClass('k' + (i + 1));
                    });
                });
            }
        },

        /*  Take title attribute of the input element and copy that value into the value attribute. */
        clearInputText: function () {
            $(':input[title]').each(function () {
                var $this = $(this);
                if ($this.val() === '') {
                    $this.val($this.attr('title'));
                }
                $this.focus(function () {
                    if ($this.val() === $this.attr('title')) {
                        $this.val('');
                    }
                });
                $this.blur(function () {
                    if ($this.val() === '') {
                        $this.val($this.attr('title'));
                    }
                });
            });
        },

        /* Global language page */
        globalLang: function (selector) {
            var el = $(selector);
            if (el.length == 0) { return }
            var hotspots = $('.hotspots li a', el);
            hotspots.click(function () {
                hotspots.parent('li').removeClass('selected');
                $(this).parent('li').addClass('selected');
            });
        },
        /* @end */

        /* KeyFeatures */
        keyFeatures: function (el) {
            if ($(el).length == 0) { return }

            var winWidth = $(window).width();
            if (winWidth > 650) {

                var hContainer = $(el); //keyFeatures     
                var hSpots = $('.hotspots li a', hContainer);
                var hText = $('.keyText', hContainer);

                hContainer.addClass('js-style'); //for styling

                // append text to hotspots
                hSpots.each(function (i) {
                    var thisId = $(this).attr('href');
                    var thisBody = $(thisId, hText).html();
                    $(this).parent('li').append('<div class="keyInfoBox radius5">' + thisBody + '</div>');
                });

                //remove list, not needed anymore
                hText.remove();

                //remove duplicated  headers, hide the box, add close button
                var closeBox = '<span class="close"></span>';
                var keyBox = $('.keyInfoBox');
                keyBox
                	.hide()
                	.prepend(closeBox)
                	.find('h3')
                	.remove();

                //close box
                function closeKeyInfoBox() {
                    $('.keyInfoBox').fadeOut();
                    hSpots.removeClass('on');
                };
                keyBox.find('.close').click(function () {
                    closeKeyInfoBox();
                });

                hSpots.bind('click', function () {
                    if ($(this).is('.on')) {
                        closeKeyInfoBox();
                    } else {
                        closeKeyInfoBox();
                        $(this).addClass('on').parent('li').find('.keyInfoBox').fadeIn();
                    }
                    return false;
                });
            };

        },
        /* @end */

        /* ShowHide */
        showHide: function () {
            $('body').on('click nc:doClose nc:doOpen nc:doToggle', '.showHide-btn', function (evt) {
                var $this = $(this).closest('.showHide'),
                    $content = $this.find('.showHide-content'),
                    isOpened = $this.is('.showHide-opened');

                evt.preventDefault();

                if ((isOpened && evt.type === 'nc:doOpen') || (!isOpened && evt.type === 'nc:doClose')) {
                    return;//don't do anything
                }

                if (isOpened) {
                    $content.slideUp();
                    $this.removeClass('showHide-opened');
                } else {
                    //slide down and mark as opened
                    $content.slideDown();
                    $this.addClass('showHide-opened');
                }

                //if( evt.type == 'click' ) {
                $this.trigger(isOpened ? 'nc:close' : 'nc:open', { type: evt.type });
                //}
            });
        }
        /* @end */
    }
};

function getSelectedProductIds() {
    var selectedProductIds = new Array();
    var checkBoxCollection = $("div.category ul li input:checked");
    for (var i = 0; i < checkBoxCollection.length; i++) {
        var hidden = $(checkBoxCollection[i]).parent().find("input[type=hidden]");
        selectedProductIds.push($(hidden).val());
    }
    return selectedProductIds;
}




/* Initialise the Kenwood object */
$(function () {

    // for screens >= 600px only
    var winW = $(window).width();

    // iOS scale bug fix (/js/lib_min.js)
    MBP.scaleFix();

    //Overlay (/js/lib_min.js)
    KENWOOD.modal.overlay();

    //Star rating (/js/lib_min.js)
    starRating.create('.starRating');

    function initTooltips() {
        var $glossaries = $(".js-glossary:not(.ncj-tooltip)");

        if ($glossaries.length > 0) {//if there are any glossaries
            //alter HTML
            $glossaries.addClass('ncj-tooltip').attr('data-options', '"popupClasses":"glossaryPopup radius5", "hideDelay":0.5').each(function () {
                var $this = $(this),
                    $firstChild = $this.children().first();

                if ($firstChild.is('a')) {
                    $this.attr('data-options', $this.attr('data-options') + ',"textTarget":"' + $firstChild.attr('href') + '"');
                } else {
                    $(this).wrapInner($('<a href="#"></a>').click(function () { return false; }));
                }
            });

            //recheck widgets 
            Netcel.Class.init($);
        }
    }

    initTooltips();

    Netcel._initTooltips = initTooltips;

    //date picker
    $.datepicker.setDefaults({ changeMonth: true, changeYear: true, constrainInput: true, maxDate: "+0d", yearRange: "-100:+0" });

    function validateDatePicker(inputField) {
        var $inputField = $(inputField),
            value = $inputField.val(),
            dateFormat = $inputField.datepicker("option", "dateFormat");
        isValid = true;

        try {
            $.datepicker.parseDate(dateFormat, value);
        } catch (e) {
            isValid = false;
        }

        return isValid;
    }

    $('.js-datepicker')
        /*.on( 'change', function(e){//testing code
            var $this = $(this);
            
            var isValid = validateDatePicker( $this );
            
            if( isValid ){
                $this.closest('.field').removeClass( 'errorBox' ).find( '.error' ).hide();
            } else {
                $this.closest('.field').addClass( 'errorBox' ).find( '.error' ).show();
            }
        } )*/
        .datepicker({});

    //recipe details, keyfeatures OL list: add image numbers
    KENWOOD.generic.styleOrderedList('ol.imgNumbers');

    //Key features
    KENWOOD.generic.keyFeatures('.keyFeatures');

    //Hotspots (global language page)
    KENWOOD.generic.globalLang('#globalLang');

    //Hide form field value on focus
    KENWOOD.generic.clearInputText();

    //Related content drop-downs
    KENWOOD.widgets.relatedContent('div#related');

    //Generic collapse-expand content
    KENWOOD.widgets.collapsibleContent('.js-show-hide');

    //generic collapse-expand content
    KENWOOD.widgets.showMoreInfo('.frameWrap .moreInfo');

    //Choose your country
    KENWOOD.widgets.countrySelector('.lang');

    //Choose category
    KENWOOD.widgets.selectCategory('#categorySelect');

    // Search autosuggest
    KENWOOD.widgets.autoSuggest('#Search,#search');

    //Top navigation
    KENWOOD.widgets.topNavDropDowns('#topNav ul.tabs > li');

    //Print button	
    KENWOOD.generic.printPage('.print');

    //zebra table, every second column different styling
    KENWOOD.generic.tableZebra('table.zebra');

    //mobile only
    KENWOOD.mobile.mobileOnly();

    //init addthis once DOM loaded
    if (window.addthis) {
        addthis.init();
    }
    //Promo campaign collection page -  Flexslider (/js/lib_min.js)
    if ($('div.collectionCarousel').length > 0) {

        // 1. colour range navigation - styling      
        var collectionList = $('div.collectionCarousel .collectionNav');
        var totalColours = collectionList.find('li').length;
        collectionList.addClass('c' + totalColours);

        // 2. make the images responsive, calculate img's parent container (LI width in %)
        var productRange = $('.collectionCarousel ul.range');
        var rangeWidthMax = 960; //container width

        productRange.each(function () {
            var thisRange = $(this);
            var range = {};
            range.contentWidth = 0; // width of actual content
            range.Width = 0;

            thisRange.find('img').each(function () {
                var imgSize = parseFloat($(this).parent('a').attr('data-width'));
                range.Width += imgSize;
            });

            thisRange.find('img').each(function () {
                var imgSize = parseFloat($(this).parent('a').attr('data-width'));
                var imgParentSize = imgSize / rangeWidthMax * 100;
                $(this).parent('a').parent('li').attr('style', 'width:' + imgParentSize + '%');
                range.contentWidth += imgParentSize;
            });
            //center the image
            var LImargin = parseFloat(100 - range.contentWidth) / 2;
            if (LImargin > 0) {
                thisRange.find('li:first').css('margin-left', LImargin + '%');
            }
        });
        // 3. create slide show
        if (winW >= 800) {
            $('.collectionCarousel').flexslider({
                animation: "fade",
                slideshowSpeed: 5000,
                manualControls: ".collectionNav > li",
                directionNav: false
            });
        } else {
            //mobiles - remove animation
            $('.collectionCarousel').flexslider({
                animation: "slide",
                slideshowSpeed: 5000,
                manualControls: ".collectionNav > li",
                directionNav: false,
                keyboardNav: false
            });
        };
        //add selected class (different styling to active class)       
        collectionList.find('li').click(function () {
            if (winW >= 800) {
                collectionList.find('li').removeClass('selected');
                $(this).addClass('selected');
            }
            return false;
        });
    };
    /* @end campaign collection page */

    if (winW >= 768) {
        //promotions
        KENWOOD.generic.PromoBoxes('ul.promotions');

        //fix IE7 floating problem
        if ($.browser.msie && parseInt($.browser.version, 10) <= 7) {
            $('.category .list4col > li, #content .allProducts dl').synchHeights();
            $('.category .list3col > li').synchHeights();
        }

        $('#compare .summary').synchHeights();
        $('.list3col.js-synchHeights .col .box').synchHeights();
        $('.list2col.js-synchHeights .col .box').synchHeights();
    }

    if (winW >= 600) {
        // Tabs (/js/lib_min.js)
        $('div.dynamicTabs').tabs();

        //Home carousel - Flexslider (/js/lib_min.js)
        $('.homeCarousel .inner').flexslider({
            animation: "slide",
            controlNav: true,
            controlsContainer: ".homeCarousel"
        });

        //Recipes carousel - Flexslider (/js/lib_min.js)
        $('.imageSlider .inner').flexslider({
            animation: "slide",
            //pausePlay: true,
            controlsContainer: ".imageSlider"
        });

        //Sub category carousel - Flexslider (/js/lib_min.js)
        $('.subCatCarousel .inner').flexslider({
            animation: "slide",
            controlsContainer: ".subCatCarousel",
            manualControls: ".flex-custom-nav li",
            controlNav: true
        });
    };

    //responsive image sizes
    if (winW >= 800) {
        $('img[data-hisrc]').each(function () {
            $(this).attr('src', $(this).attr('data-hisrc'));
        })

        $('div[data-hisrc]').each(function () {
            $(this).css('background-image', 'url(' + $(this).attr('data-hisrc') + ')');
        })
    }

    //carousel wings click code
    $('.wingsCarousel .wing.wingRight').click(function (e) {
        e.preventDefault();
        $('.wingsCarousel .ncj-megaslides-next a').click();
    }).mouseenter(function (e) {
        $('.wingsCarousel .ncj-megaslides-next a').addClass('hover');
    }).mouseleave(function (e) {
        $('.wingsCarousel .ncj-megaslides-next a').removeClass('hover');
    })
	.css('cursor', 'pointer')

    $('.wingsCarousel .wing.wingLeft').click(function (e) {
        e.preventDefault();
        $('.wingsCarousel .ncj-megaslides-prev a').click();
    }).mouseenter(function (e) {
        $('.wingsCarousel .ncj-megaslides-prev a').addClass('hover');
    }).mouseleave(function (e) {
        $('.wingsCarousel .ncj-megaslides-prev a').removeClass('hover');
    })
	.css('cursor', 'pointer')

    //full carousel next/prev click code
    $('.fullCarousel').on('click', '.blockUI.blockOverlay', function (e) {
        e.preventDefault();

        if (e.screenX > $(window).width() / 2) {
            $('.fullCarousel .ncj-megaslides-next a').click();
        }
        else {
            $('.fullCarousel .ncj-megaslides-prev a').click();
        }
    }).on('click', '.ncj-megaslides-slides', function (e) {
        if (e.target != e.currentTarget) {
            return;
        }

        var carousel = $(this).closest('.ncj-megaslides').widget().pop()

        var slideWidth = carousel._frames[carousel.get_frame() - 1].width();
        var windowWidth = $(window).width();
        var edgeWidth = Math.max(0, (windowWidth - slideWidth) * 0.5);

        if (e.screenX > edgeWidth + slideWidth) {
            $('.fullCarousel .ncj-megaslides-next a').click();
        }
        else if (e.screenX < edgeWidth) {
            $('.fullCarousel .ncj-megaslides-prev a').click();
        }
    });



    //recipe page overlays
    var overlayHtmlStr = '<div class="mediaOverlay" style="position:fixed"><div class="fade"></div><a href="#" class="close">X</a><div class="content"></div></div>';


    var html = $(overlayHtmlStr).appendTo($('body'));
    var pageOverlay = new MediaOverlay(html);

    $('.triggerMediaOverlayNonCarousel').click(function (e) {
        if (e.isDefaultPrevented()) {
            return;
        }

        var $this = $(this);

        if ($this.hasClass('innerMediaOverlay') && $(window).width() >= 640) {
            var mediaOverlay = $this.data('mediaOverlay');

            if (!mediaOverlay) {
                var $overlay = $(overlayHtmlStr).addClass('innerMediaOverlay').css('position', '');

                if ($this.attr('data-holder-class')) {
                    $this.closest('.' + $this.attr('data-holder-class')).append($overlay);
                } else {
                    $this.append($overlay);
                }

                mediaOverlay = new MediaOverlay($overlay);
                $this.data('mediaOverlay', mediaOverlay);
            }

            mediaOverlay.show(this.href);
        }
        else//use page overlay
        {
            pageOverlay.show(this.href);
        }

        e.preventDefault();
    });

    //implement checkbox buttons
    var checkboxButtonUpdate = function (e) {
        var $this = $(this)

        var isChecked = !!$this.attr('checked');

        $this.closest('.checkbox-button')[(isChecked ? 'addClass' : 'removeClass')]('checked');

    };
    $('.checkbox-button input[type=checkbox]').on('change', checkboxButtonUpdate).each(checkboxButtonUpdate).attr('tabindex', -1)

    //commerce address code
    function linkFormFields(e) {
        var linked = $(this).data('linked-to');

        if (linked && linked.length > 0) {
            linked.val($(this).val());
        }
    }

    $('.use-as-billing-address input[type=checkbox]').on('change', function (e) {
        var $this = $(this)

        var isChecked = !!$this.attr('checked');

        if (isChecked) {
            if (e.originalEvent) {
                $('.billing-form').stop(true).animate({ opacity: '0.5' }).find('input:not(.disabled)').attr('readonly', '1').end().find('select:not(.disabled)').attr('disabled', '1');
            }
            else {
                $('.billing-form').css({ opacity: '0.5' }).find('input:not(.disabled)').attr('readonly', '1').end().find('select:not(.disabled)').attr('disabled', '1');
            }

            //link form fields
            $('.billing-form').find('input[data-linked-to], select[data-linked-to]').each(function () {
                $($(this).attr('data-linked-to')).data('linked-to', $(this)).on('input paste keypress keyup change', linkFormFields).trigger('change');
            });
        }
        else {
            $('.billing-form').stop(true).animate({ opacity: '1' }).find('input:not(.disabled)').attr('readonly', null).end().find('select:not(.disabled)').attr('disabled', null);

            //unlink form fields
            $('.billing-form').find('input[data-linked-to], select[data-linked-to]').each(function () {
                $($(this).attr('data-linked-to')).data('linked-to', null).off('input paste keypress keyup change', linkFormFields);
            });

        }
    }).trigger('change');

    //make checkboxes into psuedo radio buttons
    $(document).on('change', 'input[type=checkbox][data-name], [data-name]>input[type=checkbox]', function (e) {
        var t = this;
        var $this = $(t);

        if (!$this.is('[data-name]')) {
            $this = $this.closest('[data-name]');
        }

        if (t.checked) {
            $set = $('input[type=checkbox][data-name="' + $this.attr('data-name') + '"], [data-name="' + $this.attr('data-name') + '"]>input[type=checkbox]');

            $set.each(function (i, e) {
                if (e != t) {
                    e.checked = false;
                }
            })
        }
    });

    //generic show/hide
    KENWOOD.generic.showHide();
});

//fix addthis button styles
$('.addthis_button_google_plusone').attr('g:plusone:size', 'medium');
$('.addthis_button_facebook_like').attr('fb:like:layout', 'button_count');

//addthis config
var addthis_config = {
    ui_click: true
};

//Postcode lookup autocomplete code
$('.js-addressLookup').each(function () {
    var $autocomplete = $(this);

    //only run if data supplied
    if (!$autocomplete.is('[data-addresLookup]')) {
        return;
    }

    var config = JSON.parse($autocomplete.attr('data-addresLookup'));

    function findRequest(term, response) {
        lastSearchTerm = term;

        try {
            $.ajax({
                url: searchUrl,
                data: { Key: key, SearchTerm: useLastId ? term : term, LastId: useLastId ? lastId : '', SearchFor: 'Everything', Country: config.CountryCode, LanguagePreference: config.LanguagePreference, OrderBy: 'UserLocation', '$block': 'true', '$cache': '' },
                dataType: "json",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var items = data.Items;

                    for (var i = 0; i < items.length; i++) {
                        items[i].value = items[i].Text;
                    }

                    response(items);
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log('ajax error', arguments);
                    response([]);
                    return false;
                }
            });

        }
        catch (e) {
            alert('Search error');
            response([]);
        }

        useLastId = false;
    }

    function retrieveRequest(id) {
        $.ajax({
            url: retrieveUrl,
            data: { Key: key, Id: id },
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            success: onComplete,
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                console.log('ajax error', arguments);
                response([]);
                return false;
            }
        });

        function onComplete(data) {
            var item = data.Items[0];

            //trigger a change event to update .net validation

            if (item.Company.length != 0) {
                $('#' + config.HouseNumberId).val(item.Company).trigger('change');
            }

            if (item.SubBuilding.length != 0) {
                $('#' + config.HouseNumberId).val(item.SubBuilding).trigger('change');
            }

            if (item.BuildingName.length != 0) {
                $('#' + config.AddressLine1Id).val(item.BuildingName).trigger('change');

                if (item.BuildingNumber.length != 0 && item.SecondaryStreet.length != 0) {
                    $('#' + config.AddressLine2Id).val(item.BuildingNumber + ' ' + item.SecondaryStreet).trigger('change');
                }

                if (item.BuildingNumber.length != 0 && item.Street.length != 0) {
                    $('#' + config.AddressLine2Id).val(item.BuildingNumber + ' ' + item.Street).trigger('change');
                }
            } else {
                if (item.BuildingNumber.length != 0) {
                    if (item.Street.length != 0 && item.SecondaryStreet.length == 0) {
                        $('#' + config.AddressLine1Id).val(item.BuildingNumber + ' ' + item.Street).trigger('change');
                    }

                    if (item.Street.length == 0 && item.SecondaryStreet.length != 0) {
                        $('#' + config.AddressLine1Id).val(item.BuildingNumber + ' ' + item.SecondaryStreet).trigger('change');
                    }
                }
            }

            if (config.IsSwapCityFieldsEnabled) {
                if (item.District.length !== 0)
                    $('#' + config.AddressCityId).val(item.District).trigger('change');
                else {
                    $('#' + config.AddressCityId).val(item.ProvinceName).trigger('change');
                }

                $('#' + config.AddressCountyId).val(item.City).trigger('change');
            } else {
                $('#' + config.AddressCityId).val(item.City).trigger('change');

                if (item.District.length !== 0)
                    $('#' + config.AddressCountyId).val(item.District).trigger('change');
                else {
                    $('#' + config.AddressCountyId).val(item.ProvinceName).trigger('change');
                }
            }

            $('#' + config.NeighbourhoodId).val(item.Neighbourhood).trigger('change');
            $('#' + config.AddressPostcodeId).val(item.PostalCode).trigger('change');

            if (config.AddressRegionId) {
                $('#' + config.AddressRegionId + ' option:contains("' + item.ProvinceName + '")').attr('selected', 'selected');
            }
        }
    }

    //init vars
    var key = config.LicenseKey,
        lastSearchTerm = '',
        lastId = '',
        useLastId = false,
        searchUrl = config.SearchUrl,
        retrieveUrl = config.RetrieveUrl;

    $autocomplete.autocomplete({
        minLength: 1,
        search: function (event, ui) {
            var $indicator = $(event.target).closest('.fl').find('.autocompleteLoadingIndicator');
            $indicator.css('visibility', 'visible');
        },
        response: function (event, ui) {
            $(event.target).closest('.fl').find('.autocompleteLoadingIndicator').css('visibility', 'hidden');
        },
        source: function (request, response) {
            findRequest(request.term, response);
        },
        select: function (event, ui) {
            var item = ui.item;

            if (item.Next == "Find") {
                //do another search
                lastId = item.Id;
                useLastId = true;

                setTimeout(function () {
                    $autocomplete.autocomplete('search', item.Text);
                }, 0);

            } else {
                //clear old values
                lastSearchTerm = lastId = '';

                //request actual address
                retrieveRequest(item.Id);
            }

            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        var $content = $('<a></a>');

        if (item.Next == "Find") {
            $content.html(item.Text + ' <span class="autocomplete-result-description">' + item.Description + '</span>');
            $content.addClass('autocomplete-result_find');
        } else {
            $content.text(item.Text);
            $content.addClass('autocomplete-result_retrieve');
        }

        return $("<li></li>")
            .data("item.autocomplete", item)
            .append($content)
            .appendTo(ul);
    };
});