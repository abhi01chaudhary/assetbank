    $('#fileupload').bind('fileuploadsubmit', function (e, data) {

        var file = data.files[0];


        const type = file.type.split(';')[0].split('/')[1];


        if (type == 'mp4') {

            var video = document.createElement('video');

            video.addEventListener('loadedmetadata', function () {

                // console.log(video.videoWidth + ' × ' + video.videoHeight);
                var aspectRatio = (video.videoWidth / video.videoHeight);

                if (video.duration > 15) {
                    data.context.find('button').prop('disabled', false);
                    swal("Video must be of 15 seconds");
                    return false;
                }

                if (aspectRatio != 16/9) {
                    data.context.find('button').prop('disabled', false);
                    swal("Video must be of 16:9 aspect ratio!");
                    return false;
                }


            });

            video.src = file;

        } else {
            var image = document.createElement('img');
            console.log(image);

            image.addEventListener('load', function () {
                console.log(image.width + ' × ' + image.height);
            });
            var aspectRatio = (image.width / image.height);

            if (aspectRatio != 16/9) {
                data.context.find('button').prop('disabled', false);
                swal("Image must be of 16:9 aspect ratio!");
                return false;
            }


            image.src = file;
        }
     
    });