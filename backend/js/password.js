jQuery( document ).ready( function( $ ) {
    (function(){

        var url;

        // Create course form submission
        $( document ).on('click' , '.edit-user-form' , function(e){

            url = $(this).attr('data-url')

            //console.log(url);

            $('.user-edit').modal();

        });



        $('.user-edit').on('shown.bs.modal', function(){


            $( ".user-edit .modal-body" ).load( url, function(response, status, xhr){

                if(status == 'error'){
                    var msg = 'Sorry but there was an error: ';
                    $( ".ajax-errors" ).html( msg + xhr.status + " " + xhr.statusText );
                }

            });
        });

        $('.user-edit').on('hidden.bs.modal', function(){

            var prep_content = $('.prep').html();

            $( ".user-edit .modal-body").html(prep_content);

        });

    })();


    // User update form
    (function(){

        $(document).on('submit', '.update-user', function(){

            // remove prior message which might have accumulated during earlier update
            $( '.error-message' ).each(function( ) {
                $(this).removeClass('make-visible');
                $(this).html('');
            });


            $( 'input' ).each(function( ) {
                $(this).removeClass('errors');
            });



            // current form under process
            var current_form = $(this);

            // === Dynamically get all the values of input data
            var request_data = {};

            request_data['_token']  = $(this).find('input[name=_token]').val();
            request_data['_method'] = $(this).find('input[name=_method]').val();

            current_form.find('[name]').each(function(){
                request_data[$(this).attr("name")] = $(this).val();
            });



            $.post(
                $(this).prop('action'),
                request_data,
                function(data){

                    console.log(data);

                    if(data.status == 'success'){

                        $('.user-edit').modal('hide');

                        current_form.find('[name]').each(function(){
                            $(this).val('');
                        });

                        if(window.location.href == data.url+"/create"){
                            window.location.href = data.url;
                        }else if(window.location.href.indexOf("password/reset") > -1){
                            window.location.href = data.url;
                        }else if(data.url == APP_URL+"/admin/user"){
                            window.location.href = data.url;
                        }else if(data.url == APP_URL+"/admin/user/create"){
                            window.location.href = data.url;
                        }else if(data.url == APP_URL+"admin/state/create"){
                            window.location.href = data.url;
                        }
                        else{
                            location.reload();
                        }


                    }else if(data.status == 'fails'){

                        for (var key in data.errors) {

                            // skip loop if the property is from prototype
                            if (!data.errors.hasOwnProperty(key)) continue;

                            var error_message = data.errors[key];

                            current_form.find("[name="+key+"]").addClass('errors');

                            var parent = current_form.find("[name="+key+"]").parent();
                            parent.find('.error-message').addClass('make-visible').html(error_message);
                            if(data.errors.hasOwnProperty('g-recaptcha-response')){
                                $('.captcha-box .error-message').addClass('make-visible').html(data.errors['g-recaptcha-response']);
                            }
                        }
                    }

                }
            );


            return false;

        });

    })();

});
