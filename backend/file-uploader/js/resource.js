$("input[name=file]").change(function () {

    if (this.files && this.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            var img = $('<img>').attr('src', e.target.result);
           
            var htmlval = $('.file').html(img);

        };

        reader.readAsDataURL(this.files[0]);

    }
})


$(function () {
    
    'use strict';

    // var current_sub_theme_id = 0;

    // $('#sub_theme_id').on('change', function(){
    //     current_sub_theme_id = $(this).val();
    //     console.log(current_sub_theme_id);
    // });

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        // 'id':$('#sub_theme_id').val()
        },
        xhrFields: {withCredentials: true},
        url: 'store',
        imageMinHeight: 1080,
        imageMinWidth: 1080,
        options: {
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            processQueue: {
                action: 'validate',
                acceptFileTypes: '@',
                disabled: '@disableValidation'
            }
        },
    });
    $('#fileupload-resource').fileupload({
        // Uncomment the following to send cross-domain cookies:
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        // 'id':$('#sub_theme_id').val()
        },
        xhrFields: {withCredentials: true},
        url: resource_upload_url,
        imageMinHeight: 1080,
        imageMinWidth: 1080,
        options: {
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            processQueue: {
                action: 'validate',
                acceptFileTypes: '@',
                disabled: '@disableValidation'
            }
        },
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );


    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        xhrFields: {withCredentials: true},
        url: 'show',
        dataType: 'json',
        context: $('#fileupload')[0],


    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });


    $('#fileupload').bind('fileuploadsubmit', function (e, data) {

        var inputs = data.context.find(':input');

        var title_error = data.context.find(':input[name=title]').val();

        // var url = window.location.href;
        //
        // console.log(url);
        //
        // var theme_id = url.match("/theme/(.*)/sub-theme/");
        // var sub_theme_id = url.match("/sub-theme/(.*)/resource-group/");
        // var resouce_group_id = url.match("/resource-group/(.*)/resouce/create");
        //
        // console.log(theme_id[1]);
        // console.log(sub_theme_id[1]);
        // console.log(resouce_group_id);


        if((title_error == '')){

            data.context.find('button').prop('disabled', false);
            swal("The title and source fields are mandatory.");
            return false;
        }

        data.formData = inputs.serializeArray();

        // data.formData.push({
        //     name: 'sub_theme_id',
        //     value: $('#sub_theme_id').val()
        // })

        // console.log(data.formData);
    });

});