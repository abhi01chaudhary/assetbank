/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $, window */

$("input[name=file]").change(function () {

    if (this.files && this.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {
            var img = $('<img>').attr('src', e.target.result);
           
            var htmlval = $('.file').html(img);

        };

        reader.readAsDataURL(this.files[0]);

    }
});

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        xhrFields: {withCredentials: true},
        url: 'store',
        imageMinHeight: 1080,
        imageMinWidth: 1080,
        options: {
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            processQueue: {
                action: 'validate',
                acceptFileTypes: '@',
                disabled: '@disableValidation'
            }
        },
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );


    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        xhrFields: {withCredentials: true},
        url: 'show',
        dataType: 'json',
        context: $('#fileupload')[0],


    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });


    $('#fileupload').bind('fileuploadsubmit', function (e, data) {

        // var file = data.context.find(':input[name=file]');

        // console.log(file);

        var inputs = data.context.find(':input');

        var global_rights = data.context.find(':input[name=global_rights]').val();

        var title_error = data.context.find(':input[name=title]').val();

        var source_error = data.context.find(':input[name=source]').val();

        var tags_error = data.context.find(':input[name=tags]').val();

        // if (inputs.filter(function () {
        //         return !this.value && $(this).prop('required');
        //     }).first().focus().length) {
        //     data.context.find('button').prop('disabled', false);
        //     return false;
        // }

        if((title_error == '') || (source_error == '') || (tags_error == '')){
            data.context.find('button').prop('disabled', false);
            swal("The title, tags and source fields are mandatory.");
            return false;
        }

        if ( global_rights  == null || global_rights == '')
        {

            data.context.find('button').prop('disabled', false);
            swal("Please select global rights ");
            return false;
        }

        if ( global_rights  == '0')
        {

            data.context.find('button').prop('disabled', false);
            swal("All images must have GLOBAL RIGHTS of usage.");
            return false;
        }

        data.formData = inputs.serializeArray();
    });

    // $('#fileupload').fileupload({
    //     stop: function (e) {
    //         $(this).removeClass('fileupload-processing');
    //         jQuery('#success').show();
    //     }, // .bind('fileuploadstop', func);
    // });


});