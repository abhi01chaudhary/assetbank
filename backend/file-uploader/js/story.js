/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        xhrFields: {withCredentials: true},
        disableValidation: false,
        aspectRatio: 16 / 9,
        options: {
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            processQueue: {
                action: 'validate',
                acceptFileTypes: '@',
                disabled: '@disableValidation'
            }
        },
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );


    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        xhrFields: {withCredentials: true},
        url: 'resource/store',
        dataType: 'json',
        context: $('#fileupload')[0],


    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });

    $('#fileupload').bind('add', function (e, data) {
        var content = data.files[0];

        var reader = new FileReader();

        reader.readAsDataURL(content);

        reader.onload = function (ev) {

            var file = reader.result;

            console.log(file);

            const type = file.split(';')[0].split('/')[1];

            console.log(type);

            if (type == 'mp4' || type == 'ogg') {

                var video = document.createElement('video');

                video.addEventListener('loadedmetadata', function (ev) {

                    var aspectRatio = (video.videoWidth / video.videoHeight);

                    if (video.duration > 15) {
                        ev.preventDefault();

                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);

                        console.log('true');

                        swal("Video must be of 15 seconds");
                        return false;
                    }

                    if (aspectRatio != 16 / 9) {
                        ev.preventDefault();
                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);
                        swal("Video must be of 16:9 aspect ratio!");
                        return false;
                    }

                });

                video.src = file;

            } else {

                var image = document.createElement('img');

                image.addEventListener('load', function () {
                    console.log(image.width + ' × ' + image.height);
                    var aspectRatio = (image.width / image.height);

                    if (aspectRatio != 16 / 9) {
                        ev.preventDefault();
                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);
                        swal("Image must be of 16:9 aspect ratio!");
                        return false;
                    }
                });
                image.src = file;
            }
        };
    });
    $('#fileupload').bind('fileuploadadd', function (e, data) {

        var content = data.files[0];

        var reader = new FileReader();

        reader.readAsDataURL(content);

        reader.onload = function (ev) {

            var file = reader.result;

            console.log(file);

            const type = file.split(';')[0].split('/')[1];

            console.log(type);

            if (type == 'mp4' || type == 'mov') {

                var video = document.createElement('video');

                video.addEventListener('loadedmetadata', function (ev) {

                    var aspectRatio = (video.videoWidth / video.videoHeight);

                    if (video.duration > 15) {
                        ev.preventDefault();

                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);

                        console.log('true');

                        swal("Video must be of 15 seconds");
                        return false;
                    }

                    if (aspectRatio != 16 / 9) {
                        ev.preventDefault();
                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);
                        swal("Video must be of 16:9 aspect ratio!");
                        return false;
                    }

                });

                video.src = file;

            } else {

                var image = document.createElement('img');

                image.addEventListener('load', function () {
                    console.log(image.width + ' × ' + image.height);
                    var aspectRatio = (image.width / image.height);

                    if (aspectRatio != 16 / 9) {
                        ev.preventDefault();
                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);
                        //data.context.find('tr').last().append('<p style="color: #ff3839;">Invalid Media</p>');

                        swal("Image must be of 16:9 aspect ratio!");
                        return false;
                    }
                });
                image.src = file;
            }
        };
        setTimeout(function () {
            $('button.start').click();
        },1000);
    });

    $('#fileupload').bind('fileuploadsubmit', function (e, data) {

        var content = data.files[0];


        var reader = new FileReader();

        reader.readAsDataURL(content);

        reader.onload = function (ev) {

            var file = reader.result;

            console.log(file);

            const type = file.split(';')[0].split('/')[1];

            console.log(type);

            if (type == 'mp4' || type == 'ogg') {

                var video = document.createElement('video');

                video.addEventListener('loadedmetadata', function (ev) {

                    var aspectRatio = (video.videoWidth / video.videoHeight);

                    if (video.duration > 15) {
                        ev.preventDefault();

                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);

                        console.log('true');

                        swal("Video must be of 15 seconds");
                        return false;
                    }

                    if (aspectRatio != 16 / 9) {
                        ev.preventDefault();
                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);
                        swal("Video must be of 16:9 aspect ratio!");
                        return false;
                    }

                });

                video.src = file;

            } else {

                var image = document.createElement('img');

                image.addEventListener('load', function () {
                    console.log(image.width + ' × ' + image.height);
                    var aspectRatio = (image.width / image.height);

                    if (aspectRatio != 16 / 9) {
                        ev.preventDefault();
                        data.context.find('button').prop('disabled', true);
                        data.context.find('button.cancel').prop('disabled', false);
                        swal("Image must be of 16:9 aspect ratio!");
                        return false;
                    }
                });
                image.src = file;
            }
        };
        // setTimeout(function () {
        //     $('button.start').click();
        // },1000);

        var inputs = data.context.find(':input');

        var global_rights = data.context.find(':input[name=global_rights]').val();

        var title_error = data.context.find(':input[name=title]').val();

        var source_error = data.context.find(':input[name=source]').val();

        var tags_error = data.context.find(':input[name=tags]').val();


        // if (inputs.filter(function () {
        //         return !this.value && $(this).prop('required');
        //     }).first().focus().length) {
        //     data.context.find('button').prop('disabled', false);
        //     return false;
        // }

        if ((title_error == '') || (source_error == '') || (tags_error == '')) {
            data.context.find('button').prop('disabled', false);
            swal("The title, tags and source fields are mandatory.");
            return false;
        }

        if (global_rights == null || global_rights == '') {

            data.context.find('button').prop('disabled', false);
            swal("Please select global rights ");
            return false;
        }

        if (global_rights == '0') {

            data.context.find('button').prop('disabled', false);
            swal("All images must have GLOBAL RIGHTS of usage.");
            return false;
        }
        // return $data;

        data.formData = inputs.serializeArray();
    });

    // $('#fileupload').fileupload({
    //     stop: function (e) {
    //         $(this).removeClass('fileupload-processing');
    //         jQuery('#success').show();
    //     }, // .bind('fileuploadstop', func);
    // });


});