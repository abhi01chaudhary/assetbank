<?php

use Illuminate\Database\Seeder;

class ThemeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $themes = [
            array(
                'name'=>'Summer',
                'slug'=>'summer'
            ),
            array(
                'name'=>'Winter',
                'slug'=>'winter'
            ),
            array(
                'name'=>'Spring',
                'slug'=>'spring'
            ),
            array(
                'name'=>'Autum',
                'slug'=>'autum'
            ),
            array(
                'name'=>'Colors',
                'slug'=>'colors'
            )
        ];
        \App\Models\Theme::insert($themes);
    }
}
