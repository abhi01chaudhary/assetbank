<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRoles =[
            array(
                'role_type'=>'toolbox_admin',
            )
        ];
        \App\Models\UserRole::insert($userRoles);
    }
}
