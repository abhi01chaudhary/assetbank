<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Country::insert([
            'market_region_id'=>1,
            'name'=>'United Kingdom',
            'status'=>1
        ]);
    }
}
