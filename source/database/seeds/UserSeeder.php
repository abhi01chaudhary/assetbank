<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =
            array(
                'user_role_id'=>1,
                'country_id'=>226,
                'full_name'=>'Delonghi CMS',
                'email'=>'delonghi-cms@superadmin.com',
                'password'=>Hash::make('password'),
                'contact_number'=>'9849898311',
                'status'=>1
            );
        \App\Models\User::insert($users);
    }
}
