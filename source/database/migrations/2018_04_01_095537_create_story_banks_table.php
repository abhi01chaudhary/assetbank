<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoryBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('story_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('story_id')->unsigned();
            $table->foreign('story_id')
                    ->references('id')
                    ->on('stories')
                    ->onDelete('cascade');
            $table->string('story');
            $table->string('file_type');
            $table->string('file_name');
            $table->text('title');
            $table->string('tags');
            $table->string('source');
            $table->string('product_featured')->nullable();
            $table->boolean('global_rights')->default(1);
            $table->string('order')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('story_banks');
    }
}
