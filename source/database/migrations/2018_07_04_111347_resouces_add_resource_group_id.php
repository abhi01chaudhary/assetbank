<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResoucesAddResourceGroupId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resources', function ($table) {
            $table->integer('resource_group_id')->unsigned();
            $table->foreign('resource_group_id')
                ->references('id')
                ->on('resource_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resources', function ($table) {
            $table->dropForeign(['resource_group_id']);
            $table->dropColumn('resource_group_id');
        });
    }
}
