<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function(){
   return 'test';
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('login', 'ContentController@getLogin');
    Route::post('login', 'ContentController@postLogin');
    Route::get('logout', 'ContentController@doLogout');
});

Route::group(['namespace' => 'Frontend', 'middleware' => 'frontend-user'], function () {

    Route::get('/', 'ContentController@index');
    Route::get('theme/{id}', 'SubThemeController@index');
    Route::post('theme/{id}/subtheme/{sub_theme_id}/add-comment', 'SubThemeController@add_comment');
    Route::get('theme/{id}/subtheme/{sub_theme_id}/comment/{comment_id}/like', 'SubThemeController@like');

    Route::get('resource/download/{id}', 'ResourceZipController@downloadResource');

});


//Route::get('sm',function (){
//   $banks = \App\Models\ImageBank::where('approved_by_media','Pending')->get();
//    foreach ($banks as $bank){
//        $bank->update([
//           'mail_sent_to_sm'=>0,
//            'sm_mail_sent_to_admin'=>0
//        ]);
//    }
//    echo 'done';
//});
//
//Route::get('brand',function (){
//    $banks = \App\Models\ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->get();
//    foreach ($banks as $bank){
//        $bank->update([
//            'mail_sent_to_brand'=>0,
//            'brand_mail_sent_to_admin'=>0
//        ]);
//    }
//    echo 'done';
//});

Route::get('flush', function () {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    echo 'done';
});
Route::get('country', function () {
    $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
    foreach ($countries as $country) {
        \App\Models\Country::create([
            'name' => $country
        ]);
    }
});
Route::get('send-mail', function () {
    \Illuminate\Support\Facades\Mail::raw('Text', function ($message) {
        $message->to('dev@yoursdigitally.co');
    });
    echo 'done';
});

Route::get('toolbox-user/register', function () {
    return view('frontend.register', compact('countries'))->with('title', 'Register user for Toolbox');
});


Route::group(['namespace' => 'Admin'], function () {

    Route::post('register-user/toolbox', 'RegisterController@RegisterToolboxUser');

//    Route::get('/', 'DashboardController@getLogin');
    Route::get('admin', 'DashboardController@getLogin');
    Route::post('admin', 'DashboardController@postLogin');
    Route::get('admin/logout', 'DashboardController@getLogout');

    Route::get('user/register', 'RegisterController@getRegister');
    Route::post('user/register', 'RegisterController@postRegister');

    Route::get('user/forget-password-form', 'RegisterController@getPasswordResetForm');
    Route::post('user/set-reset-email', 'RegisterController@sendResetEmail');
    Route::get('/password/reset/{remember_token}', 'RegisterController@getPasswordLink');
    Route::post('password/reset/{remember_token}', 'RegisterController@passwordReset');

    Route::post('story-order', 'StoryController@storyOrder');

    Route::post('admin/resource/{id}/update', 'ResourceController@update');
});

Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {

    Route::get('admin/story/approval-social-media', 'StoryController@approvalBySocialMedia');
    Route::post('admin/approve-story-by-social-media/{id}', 'StoryController@approvedBySM');
    Route::post('admin/reject-story-by-social-media/{id}', 'StoryController@rejectedBySM');
    Route::get('admin/rejected-story-by-social-media', 'StoryController@rejectedStoryByMedia');
    Route::get('admin/story/approval-gtm', 'StoryController@approvalByGTM');
    Route::post('admin/approve-story-by-gtm/{id}', 'StoryController@approvedByGTM');
    Route::post('admin/reject-story-by-gtm/{id}', 'StoryController@rejectedByGTM');
    Route::get('admin/rejected-story-by-gtm', 'StoryController@rejectedStoryByGTM');
    Route::get('admin/story/{id}/edit', 'StoryController@edit');
    Route::delete('admin/delete-story/{id}', 'StoryController@removeStory');
    Route::post('admin/bulk-story-delete', 'StoryController@deleteBulkStory');
    Route::get('admin/downloadable-story', 'StoryController@getDownloadableStory');
    Route::get('admin/download-story/{id}', 'StoryController@downloadStory');
    Route::post('admin/story/{id}/update', 'StoryController@update');
    Route::any('admin/story/create-zip', 'StoryZipArchiveController@getIndex');
    Route::any('admin/story/delete', 'StoryController@fileUnlink');
    Route::get('admin/story/show', 'StoryController@show');


    Route::get('admin/user/approve-user', 'UserController@getApprovalRequest');
    Route::post('admin/approve-user/{id}', 'UserController@approveUserRequest');

    Route::get('admin/dashboard', 'DashboardController@index');
    Route::post('admin/profile/update-password', 'DashboardController@updatePassword');
    Route::post('admin/profile/update-basic-information', 'DashboardController@updateBasicInformation');
    Route::get('admin/profile', 'DashboardController@getProfile');

    Route::resource('admin/user', 'UserController');
    Route::resource('admin/market-region', 'MarketController');
    Route::resource('admin/country', 'CountryController');

    Route::get('admin/story/create', 'StoryController@create');
    Route::get('admin/story', 'StoryController@index');
    Route::get('admin/story/{id}', 'StoryController@viewStory');

    Route::post('admin/story/store', 'StoryController@store');

    Route::get('admin/image/create', 'ImageBankController@create');
    Route::get('admin/image', 'ImageBankController@index');
    Route::get('admin/image/approval-social-media', 'ImageBankController@approvalBySocialMedia');
    Route::get('admin/image/approval-gtm', 'ImageBankController@approvalByGTM');
    Route::get('admin/image/show', 'ImageBankController@show');
    Route::get('admin/image/{id}/edit', 'ImageBankController@edit');
    Route::post('admin/image/{id}/update', 'ImageBankController@update');

    Route::post('admin/image/store', 'ImageBankController@store');

    Route::any('admin/image/delete', 'ImageBankController@fileUnlink');

    Route::post('admin/approve-image-by-social-media/{id}', 'ImageBankController@approvedBySM');
    Route::post('admin/reject-image-by-social-media/{id}', 'ImageBankController@rejectedBySM');
    Route::post('admin/approve-image-by-gtm/{id}', 'ImageBankController@approvedByGTM');
    Route::post('admin/reject-image-by-gtm/{id}', 'ImageBankController@rejectedByGTM');
    Route::get('admin/rejected-image-by-social-media', 'ImageBankController@rejectedImageByMedia');
    Route::get('admin/rejected-image-by-gtm', 'ImageBankController@rejectedImageByGTM');

    Route::get('admin/downloadable-image', 'ImageBankController@getDownloadableImage');
    Route::post('admin/bulk-image-delete', 'ImageBankController@deleteBulkImage');
    Route::delete('admin/delete-image/{id}', 'ImageBankController@removeImage');
    Route::get('admin/download-image/{id}', 'ImageBankController@downloadImage');
    Route::get('admin/uploaded-image/market/{id}', 'ImageBankController@uploadedByLocalMarket');

    Route::any('admin/image/create-zip', 'ZipArchiveController@getIndex');

    Route::resource('admin/theme', 'ThemeController');
//    Route::resource('admin/sub-theme', 'SubThemeController');
    Route::resource('admin/resource', 'ResourceController');

    Route::get('admin/comments', 'CommentController@index');
    Route::get('admin/comments/{id}/enable', 'CommentController@enable');
    Route::get('admin/comments/{id}/disable', 'CommentController@disable');
    Route::delete('admin/comments/{id}', 'CommentController@destroy');

    Route::post('admin/resource/store', 'ResourceController@store');

    Route::get('theme-listing', function () {
        return view('admin.theme.theme-listing-page')->with('title', 'theme-listing');
    });
});

Route::group(['namespace' => 'Admin', 'middleware' => ['super-admin']], function () {

    Route::get('admin/change-view/toolbox', 'DashboardController@change_view_toolbox');
    Route::get('admin/change-view/content-hub', 'DashboardController@change_view_content_hub');

    Route::post('admin/resource/{id}/delete', 'ResourceController@destroy');

    Route::get('admin/theme/{theme_id}/sub-theme/create', 'SubThemeController@create');
    Route::post('admin/theme/{theme_id}/sub-theme/create', 'SubThemeController@store');
    Route::put('admin/theme/{theme_id}/sub-theme/{id}/edit', 'SubThemeController@update');
    Route::get('admin/theme/{theme_id}/sub-theme/{id}/edit/', 'SubThemeController@edit');
    Route::get('admin/theme/{theme_id}/sub-theme/{id}', 'SubThemeController@show');
    Route::delete('admin/theme/{theme_id}/sub-theme/{id}', 'SubThemeController@destroy');

    Route::get('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/create', 'ResourceGroupController@create');
    Route::post('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/create', 'ResourceGroupController@store');
    Route::put('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{id}/edit', 'ResourceGroupController@update');
    Route::get('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{id}/edit/', 'ResourceGroupController@edit');
    Route::get('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{id}', 'ResourceGroupController@show');
    Route::delete('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{id}', 'ResourceGroupController@destroy');

    Route::get('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{resource_group_id}/resource/create', 'ResourceController@create');
    Route::post('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{resource_group_id}/resource/store', 'ResourceController@store');

    Route::get('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{resource_group_id}/resource/{id}/edit', 'ResourceController@edit');
    Route::post('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{resource_group_id}/resource/{id}/update', 'ResourceController@update');

    Route::post('admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{resource_group_id}/resource/{id}/delete', 'ResourceController@delete');

});

