{!! Form::model($country, [
        'route' => ['country.update', $country->id],
        'class' => 'form-horizontal update-user',
        'method'=> 'PUT'
    ])
!!}

<div class="row">
    <div class="col-md-10">
        @include('admin.country.form')
    </div>
</div>

<div class="text-right border-top">
    <button type="submit" class="btn btn-warning">Update</button>
</div>
</div>


{!! Form::close() !!}

