@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            
            All Stories
            
            <span class="pull-right" ><a href="{{ URL::previous() }}" style="font-size: 15px; background:#b6bd00;padding: 9px 35px; color: #fff;">Back</a></span>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Story</th>
                                    <th>Story Information</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded At</th>
                                </tr>
                                </thead>
                                <tbody>
 
                                <?php 

                                    $i = 1; 

                                    $newvar = App\Models\StoryBank::where('story_id','=', $stories->id)->orderBy('order','ASC')->get();

                                ?>

                                @foreach($newvar as $bank)

                                    <?php 
                                    
                                        $path = URL::asset('storage/backend/stories/bank/'.'Story-'.$stories->id.'-'.$stories->slug.'/'.$bank->story);
                                     
                                    ?>

                                    <tr id="{{ $bank->id }}">
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            
                                            @if ( pathinfo( $path, PATHINFO_EXTENSION) == 'psd')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'tif')
                                                
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'mp4')

                                                <a href="{{ URL::asset($path) }}" target="_blank">

                                                    <video src="{{ URL::asset($path) }}" width="100px" height="100px" controls></video>
                                                   
                                                </a>

                                            @else

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @endif
                                           
                                        </td>
                                        <td>
                                            Title: {{ $bank->title }}<br>
                                            Tags: {{ $bank->tags }}<br>
                                            Source: {{ $bank->source }}<br>
                                            Product Featured: {{ $bank->product_featured }}<br>
                                        </td>

                                        <td>
                                            @if($stories->user)
                                            {{ $stories->user->full_name }}<br>
                                             ({{ $stories->user->country->name }})
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            {{ $bank->created_at }}
                                        </td>

                                    </tr>
    
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
    <style>
        /*.btn:hover {
            background: none;
            cursor: none;
        }*/
    </style>

    <script>
         $( "tbody" ).sortable({
            cancel: ":input,button,[contenteditable]",
           
                axis: "y",
               
                    update: function (event, ui) {

                    var data = $(this).sortable('toArray');

                    console.log(data);

                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "data": data,
                        },
                        type:'POST',
                        url: '{{ url('story-order') }}',
                        success:function(response){
                            console.log(response);
                        }
                    });
                }
         });
    </script>

@stop