@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Stories
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Story</th>
                                    <th>View</th>
                                    <th>Approved By Social</th>
                                    <th>Approved By Brand</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded At</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($data as $datum)
                                    <tr>
                                        <td>{{ $i++ }}</td>

                                        <td>
                                            {{ 'Story-'.$datum->id.'-'.$datum->slug }}
                                        </td>

                                        <td>
                                            <a href="{{ url('admin/story/'.$datum->id) }}" class="btn btn-primary">View this Story</a>
                                        </td>

                                        <td>
                                            @if($datum->approved_by_media == 'Pending')
                                                <span class="btn pending" style="background-color: #FF7E00">Pending</span>
                                            @elseif($datum->approved_by_media == 'Approved')
                                                <span class="btn btn-success">Approved</span>
                                            @else
                                                <span class="btn btn-danger">Rejected</span>
                                            @endif
                                        </td>


                                        <td>
                                            @if($datum->approved_by_gtm == 'Pending')
                                                <span class="btn pending" style="background-color: #FF7E00">Pending</span>
                                            @elseif($datum->approved_by_gtm == 'Approved')
                                                <span class="btn btn-success">Approved</span>
                                            @else
                                                <span class="btn btn-danger">Rejected</span>
                                            @endif
                                        </td>

                                        <td>
                                            @if($datum->user)
                                                {{ $datum->user->full_name }}<br>
                                                ({{ $datum->user->country->name }})
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            {{ $datum->created_at }}
                                        </td>


                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
    <!-- <style>
        .pending:hover {
            background: none;
            cursor: none;
        }
    </style> -->

@stop