@extends('admin.main')
@section('content')
    <section class="content-header">
        <h1>
            Add a Theme
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

            @include('admin.flash.message')

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-12">
                                {{ Form::open(['url'=>'admin/sub-theme', 'class'=>'form-horizontal','files'=>true]) }}

                                    @include('admin.sub-theme.form')
                             
                                    <div class="text-right border-top">
                                        <button type="submit" class="btn btn-warning">Create</button>
                                    </div>

                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>

        $(function () {

            $(document).on('change', '.theme_name' , function (data) {

                var theme_name = $(this).val();

                theme_name = theme_name.replace(/\s+/g, '-').toLowerCase();

                $('.generate_slug').val(theme_name);

            });

        });

    </script>
@stop