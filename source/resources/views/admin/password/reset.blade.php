<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::asset('backend/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('backend/font-awesome/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::asset('backend/font-awesome/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('backend/dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('backend/plugins/iCheck/square/blue.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('backend/css/delonghi.css') }}">

    {{ Html::script('backend/js/html5shiv.min.js') }}
    {{ Html::script('backend/plugins/jQuery/jQuery-2.1.4.min.js') }}
    {{ Html::script('backend/js/respond.min.js') }}

    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" style="width: 475px">
    <div class="login-logo">
        <a href="{{ url('/') }}">{{ Html::image('backend/images/logo_kenwood.gif', '') }}
        </a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        @include('admin.flash.message')
        <form class="form-horizontal update-user" role="form" method="POST" action="{{ url('/password/reset/'.$remember_token) }}">
            {!! csrf_field() !!}

            <input type="hidden" name="token" >

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-8">
                    <input type="email" class="form-control" name="email" readonly value="{{ $email  }}">

                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Password</label>

                <div class="col-md-8">
                    <input type="password" class="form-control" name="password">

                    <span class="error-message"></span>

                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Confirm Password</label>
                <div class="col-md-8">
                    <input type="password" class="form-control" name="password_confirmation">

                    <span class="error-message"></span>

                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Reset Password
                    </button>
                </div>
            </div>
        </form>

    </div><!-- /.login-box-body -->


</div><!-- /.login-box -->

<!-- Bootstrap 3.3.5 -->
<script src="{{ URL::asset('backend/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('backend/js/password.js') }}"></script>
<!-- iCheck -->
<script src="{{ URL::asset('backend/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};

</script>

</body>
</html>
