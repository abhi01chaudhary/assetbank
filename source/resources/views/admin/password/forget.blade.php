{!! Form::open([
        'url' => ['user/set-reset-email'],
        'class' => 'form-horizontal update-user',
        'method'=> 'POST'
    ])
!!}

<div class="row">
    <div class="col-md-10">
        <div class="box-body">
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
                <div class="col-sm-5">
                    {!! Form::text('email', null , ['class'=> 'form-control', 'placeholder' => 'Email', 'id'=>"email"]) !!}
                    <span class="error-message"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-right border-top">
    <button type="submit" class="btn btn-warning">Send Reset Email</button>
</div>
</div>


{!! Form::close() !!}
<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};

</script>