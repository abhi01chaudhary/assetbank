<script>
    var resource_upload_url = '{{ $url  }}';
</script>
<div id="fileupload" class="table-wrap">

    <div class="row fileupload-buttonbar">
        <div class="col-lg-7">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <label class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
                <input type="file" name="file" class="hidden" multiple>
            </label>
            <button type="submit" class="btn btn-primary start">
                <i class="glyphicon glyphicon-upload"></i>
                <span>Start Bulk upload</span>
            </button>

<!--             <button type="button" class="btn btn-danger delete">
                <i class="glyphicon glyphicon-trash"></i>
                <span>Delete</span>
            </button> -->

            <!-- <input type="checkbox" class="toggle"> -->
            <!-- The global file processing state -->
            <span class="fileupload-process"></span>
            <p style="margin-top: 20px">All Media MUST:<br>
                - Be in jpeg, png, jpg, gif, psd, tiff or svg, docx, pptx, mp4, mov format<br>
                - Any images with text must be supplied as a layered file</p>
        </div>
        <!-- The global progress state -->
        <div class="col-lg-5 fileupload-progress fade">
            <!-- The global progress bar -->
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
            <!-- The extended global progress state -->
            <div class="progress-extended">&nbsp;</div>
        </div>
    </div>
    <!-- The table listing the files available for upload/download -->
    <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
            <strong class="error text-danger"></strong>
        </td>

<td class="title">
    <label class="title-label">Title*: <input name="title" class="form-control col-md-12" placeholder="eg Platter of Dim Sum: Steamed Shrimp Dumplings (Har Gow)"></label><br>
</td>

<td>
    <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
</td>

<td>
    <p class="size">Processing...</p>
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
</td>
<td>
    {% if (!i && !o.options.autoUpload) { %}
        <button class="btn btn-primary start" disabled>
            <i class="glyphicon glyphicon-upload"></i>
            <span>Start</span>
        </button>
    {% } %}
    {% if (!i) { %}
        <button class="btn btn-warning cancel">
            <i class="glyphicon glyphicon-ban-circle"></i>
            <span>Cancel</span>
        </button>
    {% } %}
</td>
</tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
      <td>
         {% if (file.error) { %}

                <div><span class="label label-danger">Error</span> {%=file.error%}</div>

            {% } else { %}

            <div>
                <span class="label label-success">Success</span>
                 {%=file.message%}
             </div>

            {% } %} 

            <br>

            {% if (file.title_error) { %}
                <div><span class="label label-danger">Error</span> {%=file.title_error%}</div>
            {% } %} <br>

        </td>

        {{--<td>--}}
            {{--<span class="preview">--}}
                {{--{% if (file.thumbnailUrl) { %}--}}
                    {{--<a href="{%=file.thumbnailUrl%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img width="80" height="80" src="{%=file.thumbnailUrl%}"></a>--}}
                {{--{% } %}--}}
            {{--</span>--}}
        {{--</td>--}}

        <td class="title">
            <label class="title-label">Title*: <input name="title" class="form-control col-md-10" value="{%=file.title%}" disabled ></label>

        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
          <!--   {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %} -->
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<style>
    .title input{
        display: block;
    }

    canvas {
        width: 100px;
        height: 100px;
    }
   .title-label {
        width: 100%;
    }
    @media (max-width: 1200px){
        .table-wrap {
            overflow-x: auto;
        }
    }

    @media (max-width:767px) {
        td, th {
            min-width: 150px;
        }
    }

    audio, canvas, progress, video {
        display: inline-block;
        vertical-align: baseline;
        width: 150px;
    }

</style>






