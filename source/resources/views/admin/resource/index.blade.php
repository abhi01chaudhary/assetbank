@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            
            All Resources
            
            <span class="pull-right" ><a href="{{ URL::previous() }}" style="font-size: 15px; background:#b6bd00;padding: 9px 35px; color: #fff;">Back</a></span>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

               @include('admin.flash.message')
               
                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Resource</th>
                                    <th>Theme</th>
                                    <th>Sub Theme</th>
                                    <th>Title</th>
                                    <th>Source</th>
                                    <th>Uploaded By</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                    $i = 1;
                                ?>
 
                                @foreach($resources as $resource)

                                    <?php

                                        $subtheme = App\Models\SubTheme::where('id', $resource->sub_theme_id)->first();

                                        $path = url('/').'/image/resources/'.$resource->resource;

                                        $user = App\Models\User::where('id', $resource->user_id)->first();
                
                                    ?>

                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>
                                            
                                            @if ( pathinfo( $path, PATHINFO_EXTENSION) == 'psd')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'tif')
                                                
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>
                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'docx')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                   <p>{{ $resource->title  }}.docx</p>
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'pptx')

                                                {{--<a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">--}}
                                                    {{ $resource->name  }}
                                                {{--</a>--}}

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION ) == 'mp4')

                                                <a href="{{ URL::asset($path) }}" target="_blank">

                                                    <video src="{{ URL::asset($path) }}" width="100px" height="100px" controls></video>
                                                   
                                                </a>

                                            @else

                                                <a href="{{ URL::asset($path) }}" target="_blank">
                                                    <!-- {{ Html::image(asset($path)) }} -->
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                    
                                                </a>

                                            @endif
                                           
                                        </td>
                                        <td>
                                            @foreach( $subtheme->theme as $theme )
                                                <li>{{ $theme->name }}</li>
                                            @endforeach
                                        </td>
                                        <td>
                                            {{ $subtheme->name }}
                                        </td>
                                        <td>
                                         {{ $resource->title }}<br>
                                        </td>
                                        <td>
                                         {{ $resource->source }}<br>
                                        </td>
                                        <td>
                                            {{ $user->full_name }}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary btn-sm"
                                               href="{{ url('admin/resource/' . $resource->id . '/edit') }}"><i
                                                        class="fa fa-edit"></i>
                                            </a>
                                            {{--<form action="{{ url('admin/resource/'.$resource->id.'/delete') }}"--}}
                                                  {{--method="POST" class="delete-user-form">--}}

                                                {{--{!! csrf_field() !!}--}}

                                                {{--<button type="submit" class="btn btn-sm btn-danger">--}}
                                                    {{--<i class="flaticon-delete-button"></i>--}}
                                                {{--</button>--}}
                                            {{--</form>--}}
                                        </td>
                                    </tr>
    
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
    <style>
        /*.btn:hover {
            background: none;
            cursor: none;
        }*/
    </style>
@stop