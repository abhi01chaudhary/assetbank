@extends('admin.main')

@section('content')
    <section class="content-header">
        <h1>
            Add User
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

            {{--@include('flash.message')--}}
            {{--@include('errors.list')--}}

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-12">
                                {{ Form::open(['url'=>'admin/user', 'class'=>'form-horizontal update-user']) }}

                                @include('admin.user.form')


                                <div class="text-right border-top">
                                    <button type="submit" class="btn btn-warning">Create</button>
                                </div>


                                {{ Form::close() }}
                            </div>


                        </div>
                    </div>
                </div><!-- /.box -->


            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop