@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered dt-responsive table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Country</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $user->full_name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <?php
                                            $roleId = $user->user_role_id;
                                            $role = \App\Models\UserRole::where('id',$roleId)->first();
                                            ?>
                                            {{ $role->role_type }}
                                        </td>
                                        <td>
                                            <?php
                                            $countryId = $user->country_id;
                                            $country = \App\Models\Country::where('id',$countryId)->first();
                                            ?>
                                            {{ $country->name }}
                                        </td>
                                        <td>
                                            @if($user->status == 1)
                                                Active
                                            @else
                                                InActive
                                            @endif
                                        </td>

                                        <td>

                                            <button type="button" class="btn btn-primary btn-sm edit-user-form"
                                                    data-url="{{ route('user.edit', array($user->id)) }}">
                                                <i class="flaticon-edit"></i>
                                            </button>

                                            <form action="{{ route('user.destroy', array($user->id)) }}"
                                                  method="DELETE" class="delete-user-form">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="flaticon-delete-button"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach


                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 100,
                'responsive':true,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>

@stop