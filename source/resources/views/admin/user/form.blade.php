
<div class="box-body">

    <div class="form-group">
        <label for="email" class="col-sm-3 control-label">Email<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
        <div class="col-sm-5">
            {!! Form::text('email', null , ['class'=> 'form-control', 'placeholder' => 'Email', 'id'=>"email"]) !!}
            <span class="error-message"></span>
        </div>
    </div>

    <div class="form-group">
        <label for="country" class="col-sm-3 control-label">Choose Country<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-5">
            {{ Form::select('country_id', $countries, null, ['placeholder'=>'--Select Country--', 'class'=>'form-control']) }}
            <span class="error-message"></span>
        </div>

    </div>

    @if(Request::segment(4) != 'edit')
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Password<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-5">
                {!! Form::input('password', 'password', null , ['class'=> 'form-control', 'placeholder' => 'Password', 'id'=>"password"]) !!}
                <span class="error-message"></span>
            </div>
        </div>
         <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Confirm Password<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-5">
                {!! Form::input('password', 'password_confirmation', null , ['class'=> 'form-control', 'placeholder' => 'Confirm Password', 'id'=>"password_confirmation"]) !!}
                <span class="error-message"></span>
            </div>
        </div>
    @endif

    <div class="form-group">
        <label for="full_name" class="col-sm-3 control-label">Full Name<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
        <div class="col-sm-5">
            {!! Form::text('full_name', null , ['class'=> 'form-control', 'placeholder' => 'Full Name', 'id'=>"full_name"]) !!}
            <span class="error-message"></span>
        </div>
    </div>



    <div class="form-group">
        <label for="country" class="col-sm-3 control-label">User Role<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-5">
            {{ Form::select('user_role_id', $roles, null, ['placeholder'=>'--Select User Role--', 'class'=>'form-control']) }}
            <span class="error-message"></span>
        </div>

    </div>

</div>







