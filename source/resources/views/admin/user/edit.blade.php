{!! Form::model($user, [
        'route' => ['user.update', $user->id],
        'class' => 'form-horizontal update-user',
        'method'=> 'PUT'
    ])
!!}

<div class="row">
    <div class="col-md-10">
        @include('admin.user.form')
    </div>
</div>

<div class="text-right border-top">
    <button type="submit" class="btn btn-warning">Update</button>
</div>
</div>


{!! Form::close() !!}

