<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }}</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ URL::asset('backend/images/favicon.ico') }}" sizes="32x32"/>

    <!-- Bootstrap 3.3.5 -->

{{ Html::style('backend/bootstrap/css/bootstrap.min.css') }}

<!-- Font Awesome -->

{{ Html::style('backend/font-awesome/font-awesome.min.css') }}

<!-- Ionicons -->

{{ Html::style('backend/font-awesome/ionicons.min.css') }}


{{ Html::style('backend/plugins/datepicker/datepicker3.css') }}

{{ Html::style('backend/plugins/daterangepicker/daterangepicker-bs3.css') }}


{{ Html::style('backend/fonts/font/flaticon.css') }}

<!-- Select2 -->

{{ Html::style('backend/plugins/select2/select2.min.css') }}

{{ Html::style('backend/choosen/css/chosen.min.css') }}

{{ Html::style('backend/css/style.css') }}

<!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
          media="screen">

{{ Html::style('backend/dist/css/AdminLTE.min.css') }}


{{ Html::style('backend/dist/css/skins/skin-blue.min.css') }}


{{ Html::style('backend/plugins/iCheck/all.css') }}
{{ Html::style('backend/dist/css/sweetalert.css') }}


<!-- DataTables -->


    {{--{{ Html::style('backend/plugins/datatables/dataTables.bootstrap.css') }}--}}
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.16/r-2.2.0/datatables.min.css"/>

    {{ Html::style('backend/css/radiobutton.css') }}

    {{ Html::style('backend/css/gallery.css') }}

    {{ Html::style('backend/plugins/timepicker/bootstrap-timepicker.css') }}
    {{  Html::style('backend/css/delonghi.css') }}



    {{--{!! Html::script("http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js") !!}--}}


    {{ Html::script('backend/js/html5shiv.min.js') }}

    {{ Html::script('backend/js/respond.min.js') }}

    {{ Html::script('backend/plugins/jQuery/jQuery-2.1.4.min.js') }}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>


    {{ Html::script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') }}

    {{--{{ Html::script('backend/chart/highcharts.js') }}--}}

</head>


<body class="hold-transition skin-blue sidebar-mini">


<div class="wrapper">

    <!-- Main Header -->

    <header class="main-header">

        <!-- Logo -->

        <a href="{{ url('admin/dashboard') }}" class="logo">

            <!-- mini logo for sidebar mini 50x50 pixels -->

            <span class="logo-mini">{{ Html::image('backend/images/kenwood_mobile.png', '',['width'=>'100%']) }}</span>

            <!-- logo for regular state and mobile devices -->

            <span class="logo-lg">

               {{ Html::image('backend/images/logo_kenwood.gif', '') }}

            </span>

        </a>


        <!-- Header Navbar -->

        <nav class="navbar navbar-static-top" role="navigation">

            <!-- Sidebar toggle button-->

            @if(session('view') != 'toolbox')
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

                    <span class="sr-only">Toggle navigation</span>

                </a>
        @endif

        <!-- Navbar Right Menu -->


            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">


                    @if(Auth::user())

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">{{ Auth::user()->full_name }} <span
                                        class="caret"></span></a>
                            <ul class="dropdown-menu navbar nav nav-pills nav-stacked">
                                @if(Auth::user()->user_role_id == 1)
                                    <li>
                                        @if(session('view') != 'toolbox')
                                            <a href="{{ url('admin/change-view/toolbox') }}">ToolBox</a>
                                        @else
                                            <a href="{{ url('admin/change-view/content-hub') }}">Content Hub</a>
                                        @endif
                                    </li>
                                    <li role="separator" class="divider"></li>
                                @endif
                                <li>
                                    <a href="{{ url('admin/profile') }}">Profile</a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/logout') }}">Sign out</a>
                                </li>
                            </ul>
                        </li>

                    @endif

                </ul>

            </div>

        </nav>

    </header>

    <!-- Left side column. contains the logo and sidebar -->

    @if(session('view') != 'toolbox')
        <aside class="main-sidebar">

            <section class="sidebar">

                <!-- Sidebar Menu -->

                <ul class="sidebar-menu nav" id="side-menu">
                    @if(Auth::user()->user_role_id == 1)
                        <li class="treeview {{ (isset($active_menu) && $active_menu == 'user') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>User</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu {{ (isset($active_menu) && $active_menu == 'user') ? 'menu-open' : '' }}" {{ (isset($active_menu) && $active_menu == 'user') ? 'style="display: block;"' : '' }}>
                                <li><a href="{{ url('admin/user/create') }}">Add New</a></li>
                                <li>
                                    <a href="{{ url('admin/user/approve-user') }}">Approve User
                                        <span>
                                            @if(\App\Models\User::where('status','=',0)->count() != 0)
                                                <span class="badge request-count">{{\App\Models\User::where('status','=',0)->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                                <li><a href="{{ url('admin/user') }}">All User</a></li>
                            </ul>
                        </li>
                    @endif

                    <li class="treeview {{ (isset($active_menu) && $active_menu == 'image') ? 'active' : '' }}">
                        <a href="#">
                            <i class="fa fa-picture-o"></i>
                            <span>Image</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ (isset($active_menu) && $active_menu == 'image') ? 'menu-open' : '' }}" {{ (isset($active_menu) && $active_menu == 'image') ? 'style="display: block;"' : '' }}>
                            @if(Auth::user())
                                <li>
                                    <a href="{{ url('admin/image/create') }}">Add New</a>
                                </li>
                            @endif
                            @if(Auth::user()->user_role_id == 4)
                                <li>
                                    <a href="{{ url('admin/uploaded-image/market/'.Auth::user()->id) }}">
                                        Uploaded Image
                                        <span>
                                                @if(\App\Models\ImageBank::where('user_id','=',Auth::user()->id)->count() != 0)
                                                <span class="badge request-count">{{\App\Models\ImageBank::where('user_id','=',Auth::user()->id)->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth::user()->user_role_id != 4)
                                <li>
                                    <a href="{{ url('admin/image') }}">All Images</a>
                                </li>
                            @endif
                            @if(Auth::user()->user_role_id == 2 || Auth::user()->user_role_id == 1)
                                <li>
                                    <a href="{{ url('admin/image/approval-social-media') }}">
                                        To be approved By Social
                                        <span>
                                                @if(\App\Models\ImageBank::where('approved_by_media','Pending')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\ImageBank::where('approved_by_media','=','Pending')->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 3 || Auth::user()->user_role_id == 1)

                                <li>
                                    <a href="{{ url('admin/image/approval-gtm') }}">
                                        To be approved By Brand
                                        <span>
                                            @if(\App\Models\ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2 )
                                <li>
                                    <a href="{{ url('admin/rejected-image-by-social-media') }}">
                                        Rejected Image by Social
                                        <span>
                                                @if(\App\Models\ImageBank::where('approved_by_media','Rejected')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\ImageBank::where('approved_by_media','Rejected')->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 3 )
                                <li>
                                    <a href="{{ url('admin/rejected-image-by-gtm') }}">
                                        Rejected Image by Brand
                                        <span>
                                            @if(\App\Models\ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Rejected')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Rejected')->count()}} </span>
                                            @endif
                                      </span>
                                    </a>
                                </li>
                            @endif


                            @if(Auth::user()->user_role_id == 1)
                                <li>
                                    <a href="{{ url('admin/downloadable-image') }}">
                                        Downloadable Image
                                        <span>
                                        @if(\App\Models\ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Approved')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Approved')->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                        </ul>
                    </li>

                    <li class="treeview {{ (isset($active_menu) && $active_menu == 'instagram-story') ? 'active' : '' }}">
                        <a href="#">
                            <i class="fa fa-instagram"></i>
                            <span>Instagram Story</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ (isset($active_menu) && $active_menu == 'instagram-story') ? 'menu-open' : '' }}" {{ (isset($active_menu) && $active_menu == 'instagram-story') ? 'style="display: block;"' : '' }}>
                            @if(Auth::user())
                                <li>
                                    <a href="{{ url('admin/story/create') }}">Create a Story</a>
                                </li>
                            @endif
                            @if(Auth::user()->user_role_id == 4)
                                <li>
                                    <a href="{{ url('#') }}">
                                        Uploaded Image
                                        <span>
                                                @if(\App\Models\ImageBank::where('user_id','=',Auth::user()->id)->count() != 0)
                                                <span class="badge request-count">{{\App\Models\ImageBank::where('user_id','=',Auth::user()->id)->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id != 4)
                                <li>
                                    <a href="{{ url('admin/story')  }}">All Stories</a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 2 || Auth::user()->user_role_id == 1)
                                <li>
                                    <a href="{{ url('admin/story/approval-social-media')  }}">
                                        To be approved By Social
                                        <span>
                                            <?php
                                            $pendingStoryCount = 0;
                                            $stories = \App\Models\Story::where('approved_by_media', 'Pending')->get();
                                            foreach ($stories as $story) {
                                                if ($story->bank()->count() > 0) {
                                                    $pendingStoryCount = $pendingStoryCount + 1;
                                                }
                                            }
                                            ?>
                                            @if($pendingStoryCount != 0)
                                                <span class="badge request-count">{{ $pendingStoryCount }} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 3 || Auth::user()->user_role_id == 1)

                                <li>
                                    <a href="{{ url('admin/story/approval-gtm')  }}">
                                        To be approved By Brand
                                        <span>
                                            @if(\App\Models\Story::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\Story::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2 )
                                <li>
                                    <a href="{{ url('admin/rejected-story-by-social-media') }}">
                                        Rejected Story by Social
                                        <span>
                                            @if(\App\Models\Story::where('approved_by_media','Rejected')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\Story::where('approved_by_media','Rejected')->count()}} </span>
                                            @endif
                                          </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 3 )
                                <li>
                                    <a href="{{ url('admin/rejected-story-by-gtm') }}">
                                        Rejected story by Brand
                                        <span>
                                            @if(\App\Models\Story::where('approved_by_media','Approved')->where('approved_by_gtm','Rejected')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\Story::where('approved_by_media','Approved')->where('approved_by_gtm','Rejected')->count()}} </span>
                                            @endif
                                        </span>
                                    </a>
                                </li>
                            @endif

                            @if(Auth::user()->user_role_id == 1)
                                <li>
                                    <a href="{{ url('admin/downloadable-story')  }}">
                                        Downloadable Story
                                        <span>
                                            @if(\App\Models\Story::where('approved_by_media','Approved')->where('approved_by_gtm','Approved')->count() != 0)
                                                <span class="badge request-count">{{\App\Models\Story::where('approved_by_media','Approved')->where('approved_by_gtm','Approved')->count()}} </span>
                                            @endif
                                        </span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>

                <!--   <li class="treeview {{ (isset($active_menu) && $active_menu == 'theme') ? 'active' : '' }}">
                        <a href="#">
                            <i class="fa fa-folder-o"></i>
                            <span>Theme</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ (isset($active_menu) && $active_menu == 'theme') ? 'menu-open' : '' }}" {{ (isset($active_menu) && $active_menu == 'theme') ? 'style="display: block;"' : '' }}>
                            <li><a href="{{ url('admin/theme/create') }}">Add Theme</a></li>
                            <li><a href="{{ url('admin/theme') }}">All Theme</a></li>
                        </ul>
                    </li> -->

                    {{--<li class="treeview {{ (isset($active_menu) && $active_menu == 'sub-theme') ? 'active' : '' }}">
                        <a href="#">
                            <i class="fa fa-folder-o"></i>
                            <span>Sub Theme</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ (isset($active_menu) && $active_menu == 'sub-theme') ? 'menu-open' : '' }}" {{ (isset($active_menu) && $active_menu == 'sub-theme') ? 'style="display: block;"' : '' }}>
                            <li><a href="{{ url('admin/sub-theme/create') }}">Add Sub Theme</a></li>
                            <li><a href="{{ url('admin/sub-theme') }}">All Sub Theme</a></li>
                        </ul>
                    </li>--}}

                    {{--<li class="treeview {{ (isset($active_menu) && $active_menu == 'resources') ? 'active' : '' }}">--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-file-zip-o"></i>--}}
                    {{--<span>Resources</span>--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu {{ (isset($active_menu) && $active_menu == 'resources') ? 'menu-open' : '' }}" {{ (isset($active_menu) && $active_menu == 'resources') ? 'style="display: block;"' : '' }}>--}}
                    {{--<li><a href="{{ url('admin/resource/create') }}">Add Resource</a></li>--}}
                    {{--<li><a href="{{ url('admin/resource/') }}">All Resources</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                <!--    @if(Auth::user()->user_role_id == 1)
                    <li class="{{ (isset($active_menu) && $active_menu == 'comments') ? 'active' : '' }}">
                            <a href="{{ url('admin/comments') }}">
                                <i class="fa fa-comments"></i>
                                <span>Comments</span>
                            </a>
                        </li>
                    @endif -->

                    @if(Auth::user()->user_role_id == 1)
                        <li class="treeview {{ (isset($active_menu) && $active_menu == 'country') ? 'active' : '' }}">
                            <a href="#">
                                <i class="fa fa-map-marker"></i>
                                <span>Country</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu {{ (isset($active_menu) && $active_menu == 'country') ? 'menu-open' : '' }}" {{ (isset($active_menu) && $active_menu == 'country') ? 'style="display: block;"' : '' }}>
                                <li><a href="{{ url('admin/country/create') }}">Add New</a></li>
                                <li><a href="{{ url('admin/country') }}">All Countries</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
                <!-- /.sidebar-menu -->

            </section>

            <!-- /.sidebar -->

        </aside>
    @endif

<!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper" style="{{ session('view') == 'toolbox' ? 'margin-left: 0' : '' }}">


        @yield('content')


    </div>
    <!-- /.content-wrapper -->


<!--  <div class="footer">
        <p>&copy; <?php echo date('Y'); ?> Asset Upload Hub. Made with &hearts; in Nepal by<a
                    href="http://yoursdigitally.co/"> Rillmark.</a></p>
    </div> -->


    @include('admin.partials.popUpModal')

    <div class="control-sidebar-bg"></div>

</div>


<script type="text/javascript">

    var APP_URL = "{!! json_encode(url('/')) !!}";

</script>


<![endif]-->


{{ Html::script('backend/bootstrap/js/bootstrap.min.js') }}



{{ Html::script('backend/plugins/datepicker/bootstrap-datepicker.js') }}

{{ Html::script('backend/plugins/daterangepicker/daterangepicker.js') }}

{{ Html::script('backend/dist/js/sweetalert.min.js') }}


{!! Html::script('backend/plugins/iCheck/icheck.min.js') !!}



{{ Html::script('backend/plugins/select2/select2.full.min.js') }}

{{ Html::script('backend/choosen/js/chosen.jquery.min.js') }}

{{ Html::script('backend/plugins/ckeditor/ckeditor.js') }}


{{--{{ Html::script('backend/plugins/datatables/jquery.dataTables.min.js') }}--}}
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/r-2.2.0/datatables.min.js"></script>
{{ Html::script('backend/plugins/datatables/dataTables.bootstrap.min.js') }}


{{ Html::script('backend/dist/js/app.min.js') }}

{{ Html::script('backend/js/delonghi.js') }}


{{ Html::script('backend/plugins/timepicker/bootstrap-timepicker.min.js') }}


<!-- page script -->


<script>
    $(function () {
        $(".select2").select2();
    });
</script>

<style>
    .select2-results__option[aria-selected=true] {
        display: none;
    }
</style>


<script>

    $(document).ready(function () {

        $('#date-range').daterangepicker();

    });


</script>


{{--<script>--}}

{{--$(function () {--}}

{{--var url = window.location;--}}

{{--var element = $('ul.sidebar-menu a').filter(function () {--}}

{{--return this.href == url || url.href.indexOf(this.href) == 0;--}}

{{--}).addClass('active').parent().parent().addClass('in').parent();--}}

{{--if (element.is('li')) {--}}

{{--element.addClass('active');--}}

{{--}--}}
{{--});--}}

{{--</script>--}}

<script>
    $("a.thumbnail").fancybox({
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'speedIn': 1200,
        'speedOut': 600,
        'overlayShow': false,

    });
</script>
<script>
    $(document).ready(function () {
        $('#hide-123').hide();
    });
</script>


<Style>
    /*.navbar-custom-menu {*/
    /*background: #9b9b15;*/
    /*}*/

    .login-avatar:before {
        content: "";
        display: block;
        background: url({{ URL::asset("backend/dist/img/login-icon.png") }}) no-repeat top;
        height: 18px;
        width: 18px;
        float: left;
        position: absolute;
        top: 26px;
        margin-left: -29px;
    }

    .login-avatar:after {
        content: "";
        display: block;
        background: url({{ URL::asset("backend/dist/img/arrow.png") }}) no-repeat right;
        height: 18px;
        width: 18px;
        float: left;
        position: absolute;
        top: 26px;
        right: 0px;
        margin-right: 6px;

    }

    .user-menu a.dropdown-toggle {
        padding-left: 31px;
    }

    .nav > li > a {
        padding: 10px 29px;
    }

    .user-menu a.dropdown-toggle {
        padding-left: 39px;
    }


</Style>
</body>

</html>
