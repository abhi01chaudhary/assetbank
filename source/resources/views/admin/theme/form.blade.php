<div class="box-body">

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Theme Name<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-5">

            {!! Form::text('name', null, ['class'=> 'form-control theme_name', 'placeholder' => 'Theme name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>

    </div>

    {{--<div class="form-group">--}}
        {{--<label for="slug" class="col-sm-2 control-label">Slug<span class=help-block"--}}
                                                                   {{--style="color: #b30000">&nbsp;* </span></label>--}}
        {{--<div class="col-sm-5">--}}

            {{--{!! Form::text('slug', null, ['class'=> 'form-control generate_slug', 'placeholder' => 'slug', 'id'=>"slug"]) !!}--}}
            {{--@if ($errors->has('slug'))--}}
                {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('slug') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}

        {{--</div>--}}
    {{--</div>--}}


    @if(Request::segment(4) != 'edit')
        <div class="form-group">
            <label for="block_image" class="col-sm-2 control-label">Theme Block Image<span class=help-block"
                                                                                     style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-3">
                <input onchange="document.getElementById('image').src = window.URL.createObjectURL(this.files[0])"
                       name="block_image" type="file" placeholder="" id="block_image">
                <p>Image will be shown in home page</p>
                <div>
                    <img class="img-thumbnail" width="150px" id="image"/>
                </div>

                @if ($errors->has('block_image'))
                    <span class="help-block">
                        <strong>{{ $errors->first('block_image') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @else
        <div class="form-group">
            <label for="block_image" class="col-sm-2 control-label">Theme Block Image<span class=help-block"
                                                                                     style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-3">
                <input onchange="document.getElementById('image').src = window.URL.createObjectURL(this.files[0])"
                       name="block_image" type="file" placeholder="" value="{{ $theme->block_image }}">
                <p>Image will be shown in home page</p>
                <div>
                    {{ Html::image($theme->block_image,'',['width'=>'100px','id'=>'image', 'class'=>'table-team-img img-thumbnail']) }}
                </div>
                @if ($errors->has('block_image'))
                    <span class="help-block">
                        <strong>{{ $errors->first('block_image') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @endif

    @if(Request::segment(4) != 'edit')
        <div class="form-group">
            <label for="sub_block_image" class="col-sm-2 control-label">Theme Header Block Image<span class=help-block"
                                                                                             style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-3">
                <input onchange="document.getElementById('sub_image').src = window.URL.createObjectURL(this.files[0])"
                       name="sub_block_image" type="file" placeholder="" id="sub_block_image">
                <p>Image will be shown in theme page as a header</p>
                <div>
                    <img class="img-thumbnail" width="150px" id="sub_image"/>
                </div>

                @if ($errors->has('sub_block_image'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sub_block_image') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @else
        <div class="form-group">
            <label for="sub_block_image" class="col-sm-2 control-label">Theme Header Block Image<span class=help-block"
                                                                                             style="color: #b30000">&nbsp;* </span></label>
            <div class="col-sm-3">
                <input onchange="document.getElementById('image').src = window.URL.createObjectURL(this.files[0])"
                       name="sub_block_image" type="file" placeholder="" value="{{ $theme->sub_block_image }}">
                <p>Image will be shown in theme page as a header</p>
                <div>
                    {{ Html::image($theme->sub_block_image,'',['width'=>'100px','id'=>'image', 'class'=>'table-team-img img-thumbnail']) }}
                </div>
                @if ($errors->has('sub_block_image'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sub_block_image') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @endif

</div>







