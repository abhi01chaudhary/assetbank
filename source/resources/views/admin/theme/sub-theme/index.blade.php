@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <strong>{{ $theme->name  }}</strong>
            <a class="btn btn-primary" href="{{ url('admin/theme/'.$theme->id.'/sub-theme/create') }}">Add Sub Theme</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/theme') }}">{{ $theme->name  }}</a></li>
            <li class="active">All Sub Themes</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <!-- <th>Slug</th> -->
                                    <th>Theme</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($subthemes as $subtheme)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <a href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id) }}">{{ $subtheme->name }}</a>
                                        </td>
                                        <!-- <td>{{ $subtheme->slug }}</td> -->

                                        <td>
                                            {{$subtheme->theme ? $subtheme->theme->name : '-'}}
                                        </td>

                                        <td>
                                            @if($subtheme->status == 1)
                                                <span class="label label-success">Enabled</span>
                                            @else
                                                <span class="label label-danger">Disabled</span>
                                            @endif
                                        </td>

                                        <td>
                                            {{ $subtheme->created_at }}
                                        </td>

                                        <td>
                                            <a class="btn btn-primary btn-sm"
                                               href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id) }}">View</a>

                                            <a class="btn btn-primary btn-sm"
                                               href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/edit') }}"><i
                                                        class="fa fa-edit"></i></a>

                                            <form action="{{ url('admin/theme/' . $theme->id . '/sub-theme/'.$subtheme->id) }}"
                                                  method="DELETE" class="delete-user-form">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-sm">
                                                    <i>Delete</i>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach


                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
    <style>
        /*.btn:hover {
            background: none;
            cursor: none;
        }*/
    </style>

@stop