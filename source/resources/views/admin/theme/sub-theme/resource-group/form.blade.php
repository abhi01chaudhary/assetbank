<div class="form-group">
    <label for="sub-theme" class="col-sm-2 control-label">Content Group Name<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class'=> 'form-control theme_name', 'placeholder' => 'Content Group Name', 'id'=>"name"]) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

