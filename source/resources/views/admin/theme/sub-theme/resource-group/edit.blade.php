@extends('admin.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Resource Group
            <small>#{{ $resource_group->id }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/theme') }}">Themes</a></li>
            <li><a href="{{ url('admin/theme/' . $theme->id) }}">Sub Themes</a></li>
            <li><a href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id) }}">Resource Groups</a>
            </li>
            <li class="active">Edit Resource Group</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

            @include('admin.flash.message')

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-12">
                                {!! Form::model($resource_group, [
                                        'url'=>'admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/resource-group/' . $resource_group->id . '/edit',
                                        'class' => 'form-horizontal',
                                        'method'=> 'PUT',
                                        'files'=>true
                                    ])
                                !!}
                                @include('admin.theme.sub-theme.resource-group.form')

                                <div class="text-right border-top">
                                    <button type="submit" class="btn btn-warning">Update</button>
                                </div>

                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop