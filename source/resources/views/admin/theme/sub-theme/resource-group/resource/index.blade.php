@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <strong>{{ $resource_group->name }}</strong>
            <a class="btn btn-primary" href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/resource-group/'.$resource_group->id .'/resource/create') }}">Add Content</a>
            {{--<a class="pull-right" href="{{ url('admin/theme/' . $theme->id) }}"><i class="fa fa-angle-left"></i> Back</a>--}}
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/theme') }}">{{ $theme->name  }}</a></li>
            <li><a href="{{ url('admin/theme/' . $theme->id) }}">{{ $subtheme->name  }}</a></li>
            <li><a href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id) }}">{{ $resource_group->name }}</a>
            </li>
            <li class="active">Add Content</li>
        </ol>
        
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Content</th>
                                    <th>Theme</th>
                                    <th>Sub Theme</th>
                                    <th>Title</th>
                                    <th>Content Group</th>
                                    <th>Uploaded By</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
 
                                @foreach($resources as $resource)

                                    <?php

                                        $path = url('/').'/image/resources/'.$resource->resource;

                                        $user = App\Models\User::where('id', $resource->user_id)->first();

                                    ?>

                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            
                                            @if ( pathinfo( $path, PATHINFO_EXTENSION) == 'psd')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'tif')
                                                
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'docx')

                                                <a href="{{ URL::asset($path) }}">
                                                    <p>{{ $resource->title  }}.docx</p>
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'pptx')

                                                <a href="{{ URL::asset($path) }}">
                                                    <p>{{ $resource->title  }}.pptx</p>
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION ) == 'mp4')

                                                <a href="{{ URL::asset($path) }}" target="_blank">

                                                    <video src="{{ URL::asset($path) }}" width="100px" height="100px" controls></video>
                                                   
                                                </a>
                                            @else
                                                <a href="{{ URL::asset($path) }}" target="_blank">
                                                    <!-- {{ Html::image(asset($path)) }} -->
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                    
                                                </a>

                                            @endif
                                           
                                        </td>
                                        <td>
                                            {{ $theme->name }}
                                        </td>
                                        <td>
                                            {{ $subtheme->name }}
                                        </td>
                                        <td>
                                            {{ $resource->title }}<br>
                                        </td>
                                        <td>
                                            {{ $resource_group->name }}<br>
                                        </td>
                                        <td>
                                            {{ $user->full_name }}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary btn-sm"
                                               href="{{ url('admin/theme/'.$theme->id.'/sub-theme/'.$subtheme->id.'/resource-group/'.$resource_group->id.'/resource/'. $resource->id . '/edit') }}"><i
                                                        class="fa fa-edit"></i>
                                            </a>
                                            <form action="{{ url('admin/theme/'.$theme->id.'/sub-theme/'.$subtheme->id.'/resource-group/'.$resource_group->id.'/resource/'.$resource->id.'/delete') }}"
                                                  method="POST" class="delete-user-form">

                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="flaticon-delete-button"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
    
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
@stop