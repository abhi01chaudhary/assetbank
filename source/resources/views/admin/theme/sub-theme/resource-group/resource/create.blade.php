@extends('admin.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Content to <strong>{{ $resource_group->name }}</strong>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/theme') }}">{{ $theme->name  }}</a></li>
            <li><a href="{{ url('admin/theme/' . $theme->id) }}">{{ $subtheme->name  }}</a></li>
            <li><a href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id) }}">{{ $resource_group->name  }}</a>
            <li><a href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/resource-group/' . $resource_group->id ) }}">All Content</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

            @include('admin.flash.message')

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-12">
                                {{ Form::open(['url'=>'admin/resource', 'class'=>'form-horizontal','files'=>true]) }}

                                @include('admin.resource.form')

                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>

        $(function () {

            $(document).on('change', '.theme_name' , function (data) {

                var theme_name = $(this).val();

                theme_name = theme_name.replace(/\s+/g, '-').toLowerCase();

                $('.generate_slug').val(theme_name);

            });

        });

    </script>

    <script src="{{ URL::asset('backend/file-uploader/js/resource.js') }}"></script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="{{ URL::asset('backend/file-uploader/js/vendor/jquery.ui.widget.js') }}"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <!-- blueimp Gallery script -->
    <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-image.js') }}"></script>
    <!-- The File Upload video preview plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-video.js') }}"></script>
    <!-- The File Upload validation plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-validate.js') }}"></script>
    <!-- The File Upload user interface plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-ui.js') }}"></script>
    <!-- The main application script -->
    <script src="{{ URL::asset('backend/file-uploader/js/cors/jquery.xdr-transport.js') }}"></script>

@stop