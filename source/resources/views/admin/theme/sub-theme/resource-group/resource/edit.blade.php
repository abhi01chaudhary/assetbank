@extends('admin.main')
@section('content')

    <section class="content-header">
        <h1>
            Edit Resource

            <span class="pull-right" ><a href="{{ URL::previous() }}"><i class="fa fa-angle-left"></i> Back</a></span>

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

            @include('admin.flash.message')

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">

                            <?php

                                $path = url('/').'/image/resources/'.$resource->resource;

//                                dd(pathinfo( $path, PATHINFO_EXTENSION));

                            ?>

                            {!! Form::model($resource, [
                                'url' => 'admin/theme/'.$theme->id.'/sub-theme/'.$sub_theme->id.'/resource-group/'.$resource_group->id.'/resource/'.$resource->id.'/update',
                                'class' => 'form-horizontal',
                                'method'=> 'POST',
                                'files'=> true
                                ])
                            !!}

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="box-body">

                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Theme</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="{{ $theme->name }}" class="form-control" disabled>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Sub Theme</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="{{ $sub_theme->name }}" class="form-control" disabled>
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Resource Group</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="{{ $resource_group->name }}" class="form-control" disabled>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Resource</label>
                                            <div class="col-sm-2">

                                                @if ( pathinfo( $path, PATHINFO_EXTENSION) == 'psd')

                                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                        <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                    </a>

                                                @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'tif')

                                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                        <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                    </a>

                                                @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'docx')

                                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                       <p>{{ $resource->title  }}.docx</p>
                                                    </a>

                                                @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'pptx')

                                                    {{--<a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">--}}
                                                        {{ $resource->name  }}
                                                    {{--</a>--}}

                                                @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'mp4')

                                                    <a href="{{ URL::asset($path) }}" target="_blank">

                                                        <video src="{{ URL::asset($path) }}" width="100px" height="100px" controls></video>

                                                    </a>

                                                @else

                                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                        <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                    </a>

                                                @endif

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Title<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
                                            <div class="col-sm-8">
                                                {!! Form::text('title', null , ['class'=> 'form-control', 'placeholder' => 'Title', 'id'=>"title"]) !!}
                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            {{ Form::hidden('url', URL::previous() ) }}

                            <div class="text-right border-top">
                                <button type="submit" class="btn btn-warning">Update</button>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

@stop