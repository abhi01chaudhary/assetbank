@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <strong>{{ $subtheme->name }}</strong>
            <a class="btn btn-primary" href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/resource-group/create') }}">Add Content Group</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/theme') }}">{{ $theme->name  }}</a></li>
            <li><a href="{{ url('admin/theme/' . $theme->id) }}">{{ $subtheme->name  }}</a></li>
            </li>
            <li class="active">All Content Groups</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Subtheme</th>
                                    <th>Created At</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($resource_groups as $resource_group)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <a href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/resource-group/' . $resource_group->id) }}">{{ $resource_group->name }}</a>
                                        </td>
                                        <td>
                                            {{$resource_group->subtheme ? $resource_group->subtheme->name : '-'}}
                                        </td>
                                        <td>
                                            {{ $resource_group->created_at }}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary btn-sm"
                                               href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/resource-group/' . $resource_group->id) }}">View</a>

                                            <a class="btn btn-primary btn-sm"
                                               href="{{ url('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id . '/resource-group/' . $resource_group->id . '/edit') }}"><i
                                                        class="fa fa-edit"></i></a>

                                            <form action="{{ url('admin/theme/' . $theme->id . '/sub-theme/'.$subtheme->id . '/resource-group/' . $resource_group->id) }}"
                                                  method="DELETE" class="delete-user-form">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-sm">
                                                    <i>Delete</i>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach


                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
    <style>
        /*.btn:hover {
            background: none;
            cursor: none;
        }*/
    </style>

@stop