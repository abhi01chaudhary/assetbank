@extends('admin.main')

@section('content')

	<section class="content-header">
		<div class="content-dashboard">
			<div class="row">
				<div class="col-sm-4">
					<div class="panel">
						<div class="panel-body">
							<a href="{{ url('admin/theme/create') }}">Add Theme</a>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel">
						<div class="panel-body">
							<a href="{{ url('admin/theme') }}">All Theme</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@stop