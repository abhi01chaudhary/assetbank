@extends('admin.main')
@section('content')
    <section class="content-header">
        <h1>
            Add a Theme
            {{--<a class="pull-right" href="{{ url('admin/theme') }}"><i class="fa fa-angle-left"></i> Back</a>--}}
            {{--<span class="pull-right" ><a href="{{ url('admin/dashboard') }}"><i class="fa fa-angle-left"></i>Back</a></span>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/theme') }}">Themes</a></li>
            <li class="active">Add a Theme</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

            @include('admin.flash.message')

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-12">
                                {{ Form::open(['url'=>'admin/theme', 'class'=>'form-horizontal','files'=>true]) }}

                                @include('admin.theme.form')

                                <div class="text-right border-top">
                                    <button type="submit" class="btn btn-warning">Create</button>
                                </div>


                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>

        $(function () {

            $(document).on('change', '.theme_name', function (data) {

                var theme_name = $(this).val();

                theme_name = theme_name.replace(/\s+/g, '-').toLowerCase();

                $('.generate_slug').val(theme_name);

            });

        });

    </script>
@stop