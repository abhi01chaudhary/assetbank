@extends('admin.main')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">

    	@if(session('view') != 'toolbox')

			<h1>
	            Kenwood Social Media Asset Upload Hub
	            <small>Admin Panel</small>
        	</h1>

       	@else 

			<h1>
	            Content Tool box
	            <small>Admin Panel</small>
        	</h1>

        	 <section class="content">

				@include('admin.flash.message')

				<div class="content-dashboard">
					<div class="row">

						<div class="col-sm-4">
							<div class="panel">
								<div class="panel-body panel-theme">
									<h3>
										<a href="{{ url('admin/theme/create') }}" style="font-size: 20px">Add New</a>
										<br>
										<a href="{{ url('admin/theme') }}">All Themes</a>
									</h3>
								</div>
							</div>
						</div>


						<div class="col-sm-4">
							<div class="panel">
								<div class="panel-body all-theme">
									<h4><a href="{{ url('admin/theme') }}">Recent Themes</a></h4>
									<hr style="margin: 5px 0;">
									<ul>
										@foreach($themes as $theme)
											<li><a href="{{ url('admin/theme') }}">{{ $theme->name }}</a></li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="panel">
								<div class="panel-body all-theme">
									<h4><a href="{{ url('admin/comments') }}">Comments</a></h4>
									<hr style="margin: 5px 0;">
									<ul class="comments-list">
										@foreach($comments as $comment)
											<li>
												<a href="{{ url('admin/comments') }}">
													<h6>{{ $comment->user->full_name }}</h6>
													<p>{{ \Illuminate\Support\Str::words($comment->comment, 10,'....') }}</p>
												</a>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
	        </section><!-- /.content -->
		
    	@endif
       
    <!-- Main content -->
      
@stop
