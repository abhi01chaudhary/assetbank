<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ URL::asset('backend/images/favicon.ico') }}" sizes="32x32"/>

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::asset('backend/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('backend/font-awesome/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::asset('backend/font-awesome/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('backend/dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('backend/plugins/iCheck/square/blue.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('backend/css/delonghi.css') }}">

    {{ Html::script('backend/js/html5shiv.min.js') }}
    {{ Html::script('backend/plugins/jQuery/jQuery-2.1.4.min.js') }}
    {{ Html::script('backend/js/respond.min.js') }}

    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/') }}">{{ Html::image('backend/images/logo_kenwood.gif', '') }}
        </a>
        <p>Kenwood Social Media Asset Upload Hub</p>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <h1 class="sub-heading">Register for an account</h1>
        @include('admin.flash.message')
        {!! Form::open(['url'=>'user/register','method'=>'POST','class'=>'signup_form update-user']) !!}
        <div class="form-section">
            <label>Name :</label>
            {!! Form::text('full_name', null , ['placeholder' => 'Full Name']) !!}

            <span class="error-message"></span>

        </div>
        <div class="form-section">
            <label>Email :</label>

            {!! Form::text('email', null , ['placeholder' => 'Email']) !!}
            <span class="error-message"></span>

        </div>
        <div class="form-section">
            <label>Password :</label>
            {!! Form::input('password', 'password', null , ['placeholder' => 'Password']) !!}
            <span class="error-message"></span>

        </div>
        <div class="form-section">
            <label>Confirm Password :</label>
            {!! Form::input('password', 'password_confirmation', null , [ 'placeholder' => 'Confirm Password']) !!}
            <span class="error-message"></span>

        </div>

        <div class="form-section">
            <label>Country :</label>

            {{ Form::select('country_id', $countries, null, ['placeholder'=>'--Select Country--', 'class'=>'form-control']) }}
            <span class="error-message"></span>

        </div>

        <div class="form-section captcha-box">


            <div id="html_element" >
            </div>

            <span class="error-message "></span>



        </div>

        <button type="submit" class="sign-up">Register</button>
        {!! Form::close() !!}
        <div class="or">
            Or
        </div>
        <div class="sign-in">
            <a href="{{ url('/') }}">Log In</a>
        </div>
    </div><!-- /.login-box-body -->


</div><!-- /.login-box -->


<!-- Bootstrap 3.3.5 -->
<script src="{{ URL::asset('backend/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ URL::asset('backend/plugins/iCheck/icheck.min.js') }}"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
</script>
<script type="text/javascript">
    var onloadCallback = function () {
        grecaptcha.render('html_element', {
            'sitekey': '6LeRUS0UAAAAAK8GYQWOCCSrvmHr8prV6YSE3HeZ'
        });
    };
</script>


<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<script>
    (function () {

        $(document).on('submit', '.update-user', function () {

            // remove prior message which might have accumulated during earlier update
            $('.error-message').each(function () {
                $(this).removeClass('make-visible');
                $(this).html('');
            });


            $('input').each(function () {
                $(this).removeClass('errors');
            });


            // current form under process
            var current_form = $(this);

            // === Dynamically get all the values of input data
            var request_data = {};

            request_data['_token'] = $(this).find('input[name=_token]').val();
            request_data['_method'] = $(this).find('input[name=_method]').val();

            current_form.find('[name]').each(function () {
                request_data[$(this).attr("name")] = $(this).val();
            });


            $.post(
                    $(this).prop('action'),
                    request_data,
                    function (data) {

                        console.log(data);

                        if (data.status == 'success') {

                            $('.user-edit').modal('hide');

                            current_form.find('[name]').each(function () {
                                $(this).val('');
                            });

                            location.reload();


                        } else if (data.status == 'fails') {

                            for (var key in data.errors) {

                                // skip loop if the property is from prototype
                                if (!data.errors.hasOwnProperty(key)) continue;

                                var error_message = data.errors[key];

                                current_form.find("[name=" + key + "]").addClass('errors');

                                var parent = current_form.find("[name=" + key + "]").parent();
                                parent.find('.error-message').addClass('make-visible').html(error_message);
                                if(data.errors.hasOwnProperty('g-recaptcha-response')){
                                    $('.captcha-box .error-message').addClass('make-visible').html(data.errors['g-recaptcha-response']);
                                }
                            }
                        }

                    }
            );


            return false;

        });

    })();


</script>

<style type="text/css">
    .form-section {
        margin-bottom: 25px;
    }

    .form-section label {
        float: left;
        width: 25%;
    }

    .form-section input, .form-section select {
        padding: 8px 8px;
        width: 70%;
        border-radius: 0px;
    }

    .form-section select {
        width: 70%;
    }

    /*.signup_form {*/
    /*width: 20%;*/
    /*!*border: 1px solid #ccc;*!*/
    /*margin: 0 auto;*/
    /*padding: 25px;*/
    /*}*/
    .sign-up {
        background: #b6bd00;
        border: none;
        color: #fff;
        padding: 10px 35px;
        font-size: 14px;
        width: 100%;
    }

    .login-box {
        width: 536px;
    }

    .sub-heading {
        text-align: center;
        font-size: 18px;
        text-transform: capitalize;
        margin-bottom: 20px;
        font-weight: bold;
    }

    .sub-heading span {
        font-weight: normal;
        font-size: 16px;

    }

    .error-message {
        /*display: none;*/
        padding: 3px 8px;
        color: #fff;
        background-color: #dd4b39;
        margin-top: 12px;
        font-size: 13px;
        border-radius: 3px;
        position: relative;
        margin-left: 25%;
    }

    .or {
        text-align: center;
        position: relative;
        margin-top: 25px;
    }

    .or:before {
        content: "";
        display: block;
        width: 45%;
        position: absolute;
        border: 1px solid #b6bd00;
        color: #000;
        left: 0;
        vertical-align: middle;
        top: 8px;
    }

    .or:after {
        content: "";
        display: block;
        width: 45%;
        position: absolute;
        border: 1px solid #b6bd00;
        color: #000;
        right: 0;
        vertical-align: middle;
        top: 8px;
    }

    .sign-in {
        text-align: center;
        margin-top: 10PX;
    }

    .sign-in a {
        color: #FFF;
        padding: 8px 37px;
        background: #b6bd00;
        display: inline-block;
    }

</style>
</body>
</html>
