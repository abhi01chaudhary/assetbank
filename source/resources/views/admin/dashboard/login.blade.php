<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ URL::asset('backend/images/favicon.ico') }}" sizes="32x32"/>

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::asset('backend/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('backend/font-awesome/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::asset('backend/font-awesome/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('backend/dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('backend/plugins/iCheck/square/blue.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('backend/css/delonghi.css') }}">

    {{ Html::script('backend/js/html5shiv.min.js') }}
    {{ Html::script('backend/plugins/jQuery/jQuery-2.1.4.min.js') }}
    {{ Html::script('backend/js/respond.min.js') }}

    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/') }}">{{ Html::image('backend/images/logo_kenwood.gif', '') }}
        </a>
        <p>Kenwood Social Media Asset Upload Hub</p>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="text-login">Please enter your email address and password to log in, or <a href="{{ url('user/register') }}">register here</a></p>

        @include('admin.flash.message')
        {!! Form::open(['url'=>'admin']) !!}
        <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <input type="hidden" name="_token" value="{{Session::token()}}">

        <div>
            <p style="color: red">{{ $errors->first('email') }}</p>
        </div>

      <!--   <div class="form-group has-feedback">

            <div class="col-md-6">

                <div id="html_element"></div>


                <p style="color: red;">{{ $errors->first('g-recaptcha-response') }}</p>

            </div>
        </div> -->

        <div class="row">
            {{--<div class="">--}}

            {{--</div><!-- /.col -->--}}
            <div class="col-xs-12 ">
                    <button type="submit" class="btn btn-warning login-text">Log In</button>
            </div><!-- /.col -->
        </div>

        {!! Form::close() !!}
        <button data-url="{{ url('user/forget-password-form') }}" class="forget-password edit-user-form">Forgotten Your Password?</button>

        <div class="or">
            Or
        </div>
        <div class="sign-up">
            <a href="{{ url('user/register') }}">Register for an account</a>
        </div>
    </div><!-- /.login-box-body -->


</div><!-- /.login-box -->

@include('admin.partials.popUpModal')

<!-- Bootstrap 3.3.5 -->
<script src="{{ URL::asset('backend/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('backend/js/password.js') }}"></script>
<!-- iCheck -->
<script src="{{ URL::asset('backend/plugins/iCheck/icheck.min.js') }}"></script>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
</script>
<script type="text/javascript">
    var onloadCallback = function() {
        grecaptcha.render('html_element', {
            'sitekey' : '6LeRUS0UAAAAAK8GYQWOCCSrvmHr8prV6YSE3HeZ'
        });
    };
</script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};

</script>

<style>
    .or{
        text-align: center;
        position: relative;
        margin-top: 25px;
    }
    .or:before{
        content: "";
        display: block;
        width: 45%;
        position: absolute;
        border: 1px solid #b6bd00;
        color: #000;
        left: 0;
        vertical-align: middle;
        top:8px;
    }
    .or:after{
        content: "";
        display: block;
        width: 45%;
        position: absolute;
        border: 1px solid #b6bd00;
        color: #000;
        right: 0;
        vertical-align: middle;
        top: 8px;
    }
    .sign-up{
        text-align: center;
        margin-top: 10PX;
    }
    .sign-up a {
        color: #FFF;
        padding: 8px 37px;
        /*background: #3b68a4;*/
        background: #b6bd00;
        display: inline-block;
    }
    .forget-password{
        background: none;
        color: #3b68a4;
        margin-top: 9px;
        border: 0px;


    }
    .login-text{
        width: 100%;
    }
    .text-login{
        font-size: 14px;
        border-top:1px solid #ccc;
        padding-top: 15px;

    }
    .login-logo, .register-logo {
        width: 85%;
        margin: 30px auto 5px auto;
    }
</style>
</body>
</html>
