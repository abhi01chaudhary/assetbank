@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Comments
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Sub Theme</th>
                                    <th>Like</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($comments as $comment)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $comment->user->full_name }}</td>
                                        @if($comment->subtheme == null)
                                            <td>Sub theme not found</td>
                                        @else
                                            <td>{{ $comment->subtheme->name }}</td>
                                        @endif


                                        <td class="text-center">{{ $comment->likes->count() }}</td>
                                        <td>
                                            @if($comment->status == 1)
                                                <a class="label label-success"
                                                   href="{{ url('admin/comments/' . $comment->id . '/disable') }}"
                                                   title="Change status to Disabled">Enabled</a>
                                            @else
                                                <a class="label label-danger"
                                                   href="{{ url('admin/comments/' . $comment->id . '/enable') }}"
                                                   title="Change status to Enable">Disabled</a>
                                            @endif
                                        </td>

                                        <td>
                                            {{ $comment->created_at }}
                                        </td>

                                        <td>
                                            <a class="btn btn-primary btn-sm" href="#" data-toggle="modal"
                                               data-target="#comment-id-{{ $comment->id }}">View</a>
                                            <form action="{{ url('admin/comments/'.$comment->id) }}"
                                                  method="DELETE" class="delete-user-form">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-sm">
                                                    <i>Delete</i>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach


                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    @foreach ($comments as $comment)
        <div class="modal fade" id="comment-id-{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="comment-id-{{ $comment->id }}-label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="comment-id-{{ $comment->id }}-label">{{ $comment->user->full_name }}</h4>
                    </div>
                    <div class="modal-body">
                        {{ $comment->comment }}
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
    <style>
        /*.btn:hover {
            background: none;
            cursor: none;
        }*/
    </style>

@stop