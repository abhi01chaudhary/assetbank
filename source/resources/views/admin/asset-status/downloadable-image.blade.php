@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Downloadable Images
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <button style="margin-bottom: 10px" class="btn btn-primary downloadable" data-url="{{ url('admin/image/create-zip') }}">Download All Selected</button>
                           @if(Auth::user()->user_role_id == 1) <button style="margin-bottom: 10px" class="btn btn-danger delete-all" data-url="{{ url('admin/bulk-image-delete') }}">Delete All Selected</button>@endif

                            <table  class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>Select all <input type="checkbox" id="master"></th>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Image Information</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($images as $image)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="sub_chk" data-id="{{$image->id}}">
                                        </td>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            @if (pathinfo(URL::asset('storage/backend/images/bank/'.$image->image), PATHINFO_EXTENSION) == 'psd')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/psd.jpg') }}" alt="" width="100px">
                                                </a>

                                            @elseif(pathinfo(URL::asset('storage/backend/images/bank/'.$image->image), PATHINFO_EXTENSION) == 'tif')
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/tif.png') }}" alt="" width="100px">
                                                </a>

                                            @else

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}" alt="" width="100px">
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            Title: {{ $image->title }}<br>
                                            Tags: {{ $image->tags }}<br>
                                            Source: {{ $image->source }}<br>
                                            Product Featured: {{ $image->product_featured }}<br>
                                            {{--Global Rights: @if($image->global_rights == 1) <span--}}
                                                    {{--class="btn btn-success" style="float: none">Yes</span> @else <span--}}
                                                    {{--class="btn btn-danger" style="float: none">No</span> @endif--}}
                                        </td>

                                        <td>
                                            @if($image->user)
                                                {{ $image->user->full_name }}<br>
                                                ({{ $image->user->country->name }})
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            {{ $image->created_at }}
                                        </td>

                                        <td>
                                            <a href="{{ url('admin/download-image/'.$image->id) }}"  type="button" class="btn btn-primary btn-sm">
                                                Download
                                            </a>

                                            <form action="{{ url('admin/delete-image/'.$image->id) }}"
                                                  method="DELETE" class="delete-user-form">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="flaticon-delete-button"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script type="text/javascript">
        $(document).ready(function () {

            $('#master').on('click', function(e) {
                if($(this).is(':checked',true))
                {
                    $(".sub_chk").prop('checked', true);
                } else {
                    $(".sub_chk").prop('checked',false);
                }
            });

            $('.downloadable').on('click', function(e) {

                var allVals = [];
                $(".sub_chk:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });

                if(allVals.length <=0)
                {
                    swal("Please select row.");
                }  else {
                    var join_selected_values = allVals.join(",");
                    var check = confirm("Are you sure you want to download this row?");
                    if(check == true){

                       window.location.replace($(this).data('url')+'?ids='+join_selected_values);
                        return;
                    }
                }
            });

            $('.delete-all').on('click', function(e) {

                var allVals = [];
                $(".sub_chk:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });

                var url = $(this).data('url');

                if(allVals.length <=0)
                {
                    swal('Please select row')
                }  else {
                    var join_selected_values = allVals.join(",");

                        swal({   title: "Are you sure?",
                                    text: "Are you sure you want to delete this!",
                                    type: "info",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55 ",
                                    confirmButtonText: "Yes, Delete it!",
                                    closeOnConfirm: false
                                },
                                function(){



                                    var request_data = {};

                                    request_data['_token']  = '{{ csrf_token() }}';
                                    request_data['ids'] = join_selected_values;

                                    $.ajax({
                                        url: url,
                                        type: 'POST',
                                        data: request_data,
                                        success: function (data) {
                                            console.log(data);
                                            if(data.status == 'success') {
                                                // console.log('success');
                                                location.reload();
                                            }else if(data.status == 'fail'){
                                                swal("Error!", data.message)
                                            }
                                        }
                                    });

                                    swal("Verified!", "Deleted Successfully!", "success");
                                });

                        return false;

                }
            });


        });
    </script>


@stop