@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Assets to be approved by Social Media
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Image Information</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($images as $image)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            @if (pathinfo(URL::asset('storage/backend/images/bank/'.$image->image), PATHINFO_EXTENSION) == 'psd')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/psd.jpg') }}" alt="" width="100px">
                                                </a>

                                            @elseif(pathinfo(URL::asset('storage/backend/images/bank/'.$image->image), PATHINFO_EXTENSION) == 'tif')
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/tif.png') }}" alt="" width="100px">
                                                </a>

                                            @else

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}" alt="" width="100px">
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            Title: {{ $image->title }}<br>
                                            Tags: {{ $image->tags }}<br>
                                            Source: {{ $image->source }}<br>
                                            Product Featured: {{ $image->product_featured }}<br>
                                            {{--Global Rights: @if($image->global_rights == 1) <span--}}
                                                    {{--class="btn btn-success" style="float: none">Yes</span> @else <span--}}
                                                    {{--class="btn btn-danger" style="float: none">No</span> @endif--}}
                                        </td>


                                        <td>
                                            @if($image->user)
                                                {{ $image->user->full_name }}<br>
                                                ({{ $image->user->country->name }})
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            {{ $image->created_at }}
                                        </td>

                                        <td>
                                            <a href="{{ url('admin/image/'.$image->id.'/edit' ) }}" class=" btn btn-primary btn-sm">
                                                <i class="flaticon-edit"></i>
                                            </a>

                                            <form action="{{ url('admin/approve-image-by-social-media/'.$image->id) }}"
                                                  method="POST" class="approval-by-sm">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i>Approve</i>
                                                </button>
                                            </form>

                                            <form action="{{ url('admin/reject-image-by-social-media/'.$image->id) }}"
                                                  method="POST" class="rejected-by-sm">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-delete">
                                                    <i>Reject</i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach


                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>

@stop