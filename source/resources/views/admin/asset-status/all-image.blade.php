@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Asset Records
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Image Information</th>
                                    <th>Approved By Social </th>
                                    <th>Approved By Brand</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded At</th>

                                    {{--<th>Actions</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($images as $image)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            @if (pathinfo(URL::asset('storage/backend/images/bank/'.$image->image), PATHINFO_EXTENSION) == 'psd')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/psd.jpg') }}" alt="" width="100px">
                                                </a>

                                            @elseif(pathinfo(URL::asset('storage/backend/images/bank/'.$image->image), PATHINFO_EXTENSION) == 'tif')
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                    <img src="{{ URL::asset('storage/backend/images/bank/tif.png') }}" alt="" width="100px">
                                                </a>

                                            @else

                                            <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}">
                                                <img src="{{ URL::asset('storage/backend/images/bank/'.$image->image) }}" alt="" width="100px">
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            Title: {{ $image->title }}<br>
                                            Tags: {{ $image->tags }}<br>
                                            Source: {{ $image->source }}<br>
                                            Product Featured: {{ $image->product_featured }}<br>
                                            {{--Global Rights: @if($image->global_rights == 1) <span--}}
                                                    {{--class="btn btn-success" style="float: none">Yes</span> @else <span--}}
                                                    {{--class="btn btn-danger" style="float: none">No</span> @endif--}}
                                        </td>

                                        <td>
                                            @if($image->approved_by_media == 'Pending')
                                                <span class="btn" style="background-color: #FF7E00">Pending</span>
                                            @elseif($image->approved_by_media == 'Approved')
                                                <span class="btn btn-success">Approved</span>
                                            @else
                                                <span class="btn btn-danger">Rejected</span>
                                            @endif
                                        </td>


                                        <td>
                                            @if($image->approved_by_gtm == 'Pending')
                                                <span class="btn" style="background-color: #FF7E00">Pending</span>
                                            @elseif($image->approved_by_gtm == 'Approved')
                                                <span class="btn btn-success">Approved</span>
                                            @else
                                                <span class="btn btn-danger">Rejected</span>
                                            @endif
                                        </td>

                                        <td>
                                            @if($image->user)
                                            {{ $image->user->full_name }}<br>
                                             ({{ $image->user->country->name }})
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                      <td>
                                          {{ $image->created_at }}
                                      </td>

                                        {{--<td>--}}

                                            {{--<button type="button" class="btn btn-primary btn-sm edit-user-form"--}}
                                                    {{--data-url="{{ route('user.edit', array($user->id)) }}">--}}
                                                {{--<i class="flaticon-edit"></i>--}}
                                            {{--</button>--}}

                                            {{--<form action="{{ route('user.destroy', array($user->id)) }}"--}}
                                                  {{--method="DELETE" class="delete-user-form">--}}
                                                {{--{!! csrf_field() !!}--}}

                                                {{--<button type="submit" class="btn btn-sm btn-danger">--}}
                                                    {{--<i class="flaticon-delete-button"></i>--}}
                                                {{--</button>--}}
                                            {{--</form>--}}
                                        {{--</td>--}}
                                    </tr>
                                @endforeach


                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>

@stop