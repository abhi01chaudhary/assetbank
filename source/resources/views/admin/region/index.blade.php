@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Market Regions
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($regions as $region)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $region->name }}</td>

                                        <td>
                                            @if($region->status == 1)
                                                Active
                                            @else
                                                InActive
                                            @endif
                                        </td>

                                        <td>

                                            <button type="button" class="btn btn-primary btn-sm edit-user-form"
                                                    data-url="{{ route('market-region.edit', array($region->id)) }}">
                                                <i class="flaticon-edit"></i>
                                            </button>

                                            <form action="{{ route('market-region.destroy', array($region->id)) }}"
                                                  method="DELETE" class="delete-user-form">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="flaticon-delete-button"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach


                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 100,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>

@stop