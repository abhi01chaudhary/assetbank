@extends('admin.main')

@section('content')
    <section class="content-header">
        <h1>
            Upload Image
            {{--<a id="hide-123" href="{{ url('admin/image/create') }}" type="button" class="btn btn-warning">--}}
                {{--<span>Complete process</span>--}}
            {{--</a>--}}
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

               

            {{--@include('flash.message')--}}
            {{--@include('errors.list')--}}

            <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">

                            <div class="col-md-12">
                                {{ Form::open(['url'=>'admin/image/store', 'class'=>'form-horizontal','files'=>true]) }}


                                @include('admin.asset.form')


                                {{ Form::close() }}
                            </div>


                        </div>
                    </div>
                </div><!-- /.box -->


            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->


    {{--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>--}}

    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="{{ URL::asset('backend/file-uploader/js/vendor/jquery.ui.widget.js') }}"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
    {{--<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--}}
    <!-- blueimp Gallery script -->
    <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-image.js') }}"></script>
    <!-- The File Upload audio preview plugin -->
    {{--<script src="js/jquery.fileupload-audio.js"></script>--}}
    {{--<!-- The File Upload video preview plugin -->--}}
    {{--<script src="js/jquery.fileupload-video.js"></script>--}}
    <!-- The File Upload validation plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-validate.js') }}"></script>
    <!-- The File Upload user interface plugin -->
    <script src="{{ URL::asset('backend/file-uploader/js/jquery.fileupload-ui.js') }}"></script>
    <!-- The main application script -->
    <script src="{{ URL::asset('backend/file-uploader/js/main.js') }}"></script>



    <script src="{{ URL::asset('backend/file-uploader/js/cors/jquery.xdr-transport.js') }}"></script>


@stop