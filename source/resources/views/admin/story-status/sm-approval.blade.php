@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stories to be approved by Social Media
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <table id="example1" class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Story</th>
                                    <th>View</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($data as $datum)
                                    <tr>
                                        <td>{{ $i++ }}</td>

                                        <td>
                                            {{ 'Story-'.$datum->id.'-'.$datum->slug }}
                                        </td>

                                        <td>
                                            <a href="{{ url('admin/story/'.$datum->id) }}" class="btn btn-primary">View this Story</a>
                                        </td>
                                        
                                        <td>
                                            @if($datum->user)
                                                {{ $datum->user->full_name }}<br>
                                                ({{ $datum->user->country->name }})
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            {{ $datum->created_at }}
                                        </td>

                                        <td>
                                           <a href="{{ url('admin/story/'.$datum->id.'/edit' ) }}" class=" btn btn-primary btn-sm">
                                                <i class="flaticon-edit"></i>
                                            </a>

                                            <form action="{{ url('admin/approve-story-by-social-media/'.$datum->id)}}"
                                                  method="POST" class="approval-by-sm">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i>Approve</i>
                                                </button>
                                            </form>

                                            <form action="{{ url('admin/reject-story-by-social-media/'.$datum->id) }}"
                                                  method="POST" class="rejected-by-sm">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-delete">
                                                    <i>Reject</i>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script>
        $(function () {
            $('#example1').DataTable({
                "pageLength": 25,
                "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
            });
        });
    </script>
    <style>
       /* .btn-success:hover {
            background: none;
            cursor: none;
        }*/
    </style>

@stop