@extends('admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Downloadable Stories
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                @include('admin.flash.message')

                <div class="box">
                    <div class="box-body">
                        <div class="table-reponsive">
                            <button style="margin-bottom: 10px" class="btn btn-primary downloadable" data-url="{{ url('admin/story/create-zip') }}">Download All Selected</button>
                           @if(Auth::user()->user_role_id == 1) <button style="margin-bottom: 10px" class="btn btn-danger delete-all" data-url="{{ url('admin/bulk-story-delete') }}">Delete All Selected</button>@endif

                            <table  class="table table-bordered table-striped user-list">
                                <thead>
                                <tr>
                                    <th>Select all <input type="checkbox" id="master"></th>
                                    <th>#</th>
                                    <th>Story</th>
                                    <th>View Story</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($stories as $story)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="sub_chk" data-id="{{$story->id}}">
                                        </td>
                                        <td>{{ $i++ }}</td>
                                         <td>
                                            {{ 'Story-'.$story->id.'-'.$story->slug }}
                                        </td>

                                        <td>
                                            <a href="{{ url('admin/story/'.$story->id) }}" class="btn btn-primary">View this Story</a>
                                        </td>

                                        <td>
                                            @if($story->user)
                                                {{ $story->user->full_name }}<br>
                                                ({{ $story->user->country->name }})
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            {{ $story->created_at }}
                                        </td>
                                        <td>
                                             <a href="{{ url('admin/download-story/'.$story->id) }}"  type="button" class="btn btn-primary btn-sm">
                                                Download
                                            </a>

                                            <form action="{{ url('admin/delete-story/'.$story->id) }}"
                                                  method="DELETE" class="delete-user-form">
                                                {!! csrf_field() !!}

                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="flaticon-delete-button"></i>
                                                </button>
                                            </form>
                                        </td>
                                     
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </table>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->

        </div><!-- /.row -->
    </section><!-- /.content -->

    <script type="text/javascript">
        $(document).ready(function () {

            $('#master').on('click', function(e) {
                if($(this).is(':checked',true))
                {
                    $(".sub_chk").prop('checked', true);
                } else {
                    $(".sub_chk").prop('checked',false);
                }
            });

            $('.downloadable').on('click', function(e) {

                var allVals = [];
                $(".sub_chk:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });

                if(allVals.length <=0)
                {
                    swal("Please select row.");
                }  else {
                    var join_selected_values = allVals.join(",");
                    var check = confirm("Are you sure you want to download this row?");
                    if(check == true){

                       window.location.replace($(this).data('url')+'?ids='+join_selected_values);
                        return;
                    }
                }
            });

            $('.delete-all').on('click', function(e) {

                var allVals = [];
                $(".sub_chk:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });

                var url = $(this).data('url');

                if(allVals.length <=0)
                {
                    swal('Please select row')
                }  else {
                    var join_selected_values = allVals.join(",");

                        swal({   title: "Are you sure?",
                                    text: "Are you sure you want to delete this!",
                                    type: "info",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55 ",
                                    confirmButtonText: "Yes, Delete it!",
                                    closeOnConfirm: false
                                },
                                function(){



                                    var request_data = {};

                                    request_data['_token']  = '{{ csrf_token() }}';
                                    request_data['ids'] = join_selected_values;

                                    $.ajax({
                                        url: url,
                                        type: 'POST',
                                        data: request_data,
                                        success: function (data) {
                                            console.log(data);
                                            if(data.status == 'success') {
                                                // console.log('success');
                                                location.reload();
                                            }else if(data.status == 'fail'){
                                                swal("Error!", data.message)
                                            }
                                        }
                                    });

                                    swal("Verified!", "Deleted Successfully!", "success");
                                });

                        return false;

                }
            });


        });
    </script>


@stop