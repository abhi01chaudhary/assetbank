@extends('admin.main')
@section('content')

<section class="content-header">
    <h1>
        Edit Story 

        <span class="pull-right" ><a href="{{ URL::previous() }}" style="font-size: 15px; background:#b6bd00;padding: 9px 35px; color: #fff;">Back</a></span>

    </h1>

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">

            @include('admin.flash.message')
            {{--@include('errors.list')--}}

            <!-- Horizontal Form -->
            <div class="box box-info">
                <!-- form start -->
                <div class="box-body">
                    <div class="row">

                        @if(count($story->bank)) 

                        @foreach($story->bank as $sto)
        
                        <?php 
                            // dd($sto);
                            $path = URL::asset('storage/backend/stories/bank/'.'Story-'.$story->id.'-'.$story->slug.'/'.$sto->story);
                                     
                        ?>

                        {!! Form::model($sto, [
                            'url' => 'admin/story/'.$sto->id.'/update',
                            'class' => 'form-horizontal',
                            'method'=> 'POST',
                            'files'=> true
                            ])
                            !!}

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="box-body">

                                        <div class="form-group">
                                            <label for="title" class="col-sm-3 control-label">Story : Media</label>
                                            <div class="col-sm-2">

                                            @if ( pathinfo( $path, PATHINFO_EXTENSION) == 'psd')

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'tif')
                                                
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @elseif( pathinfo( $path, PATHINFO_EXTENSION) == 'mp4')

                                                <a href="{{ URL::asset($path) }}" target="_blank">

                                                    <video src="{{ URL::asset($path) }}" width="100px" height="100px" controls></video>
                                                   
                                                </a>

                                            @else

                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ URL::asset($path) }}">
                                                    <img src="{{ URL::asset($path) }}" alt="" width="100px">
                                                </a>

                                            @endif                                  

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="title" class="col-sm-3 control-label">Title<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
                                        <div class="col-sm-8">
                                            {!! Form::text('title', null , ['class'=> 'form-control', 'placeholder' => 'Title', 'id'=>"title"]) !!}
                                            @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tags" class="col-sm-3 control-label">Tags<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
                                        <div class="col-sm-8">
                                            {!! Form::text('tags', null , ['class'=> 'form-control', 'placeholder' => 'Tags', 'id'=>"tags"]) !!}
                                            @if ($errors->has('tags'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('tags') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="source" class="col-sm-3 control-label">Source<span class=help-block" style="color: #b30000">&nbsp;* </span></label>
                                        <div class="col-sm-8">
                                            {!! Form::text('source', null , ['class'=> 'form-control', 'placeholder' => 'Source', 'id'=>"source"]) !!}
                                            @if ($errors->has('source'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('source') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="source" class="col-sm-3 control-label">Product Featured</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('product_featured', null , ['class'=> 'form-control', 'placeholder' => 'Product featured', 'id'=>"source"]) !!}
                                            @if ($errors->has('product_featured'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('product_featured') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{ Form::hidden('url', URL::previous() ) }}

                        <div class="text-right border-top">
                            <button type="submit" class="btn btn-warning">Update</button>
                        </div>


                        {!! Form::close() !!}

                        @endforeach

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

@stop