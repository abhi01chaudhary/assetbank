<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Content Toolbox</title>

    <!-- styles -->
    {{ Html::style('Toolbox/kenwood/jquery-ui-1.10.4.custom.min.css') }}
    {{ Html::style('Toolbox/kenwood/default.css') }}
    {{ Html::style('Toolbox/kenwood/print.css') }}
    {{ Html::style('Toolbox/kenwood/responsive.css') }}
    {{ Html::style('Toolbox/kenwood/fonts.css') }}

    {{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}

    <!-- Custom Css -->
    {{ Html::style('https://fonts.googleapis.com/css?family=Montserrat:300,400,700,700i') }}
    {{ Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}
    {{ Html::style('Toolbox/css/style.min.css') }}

    <!-- custom js-->
    {{ Html::script('Toolbox/js/main.js') }}

  </head>
  <body>

    <!-- custom html start -->
    <div class="wrap">

        <div class="login">
            <div class="login-banner" id="login-banner">
                <div class="login-content">
                    <div class="col6">
                        <div class="login-desc">
                            <h2>Content <span>Toolbox</span></h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>
                    </div>
                    <div class="col6">
                        <div class="login-block">

                            {{ Html::image('Toolbox/images/kenwood-logo.png') }}
                            <!-- <img src="images/kenwood-logo.png"> -->
                            <p>Please enter your email address and password to login, or register here</p>

                            {{ Form::open(['url'=>'login', 'class'=>'form-horizontal update-user']) }}

                                {!! Form::email('email', null , ['class'=> 'form-control', 'placeholder' => 'email address', 'id'=>"email"]) !!}

                                <div>
                                    <p style="color: red">{{ $errors->first('email') }}</p>
                                </div>

                                {!! Form::password('password', null , ['class'=> 'form-control', 'placeholder' => 'password', 'id'=>"password"]) !!}
            
                                <button type="submit">login</button>
                       
                            {{ Form::close() }}

                            <!-- <a href="#">Forgotten your password?</a> -->

                            <div class="register">
                                <a href="{{ url('toolbox-user/register') }}">Register for an account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- login content end -->

    </div><!-- wrap end -->
    <!-- custom html end -->
  </body>
</html>