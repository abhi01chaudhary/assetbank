<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ $title }}</title>

    <!-- styles -->
    {{ Html::style('Toolbox/kenwood/jquery-ui-1.10.4.custom.min.css') }}
    {{ Html::style('Toolbox/kenwood/default.css') }}
    {{ Html::style('Toolbox/kenwood/print.css') }}
    {{ Html::style('Toolbox/kenwood/responsive.css') }}
    {{ Html::style('Toolbox/kenwood/fonts.css') }}

    {{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}
    <!-- Custom Css -->
    {{ Html::style('https://fonts.googleapis.com/css?family=Montserrat:300,400,700,700i') }}
    {{ Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}
    {{ Html::style('Toolbox/css/style.min.css') }}

    <!-- custom js-->
    {{ Html::script('Toolbox/js/main.js') }}

  </head>
  <body>

    <!-- custom html start -->
    <div class="wrap">

        <div class="login">
            <div class="login-banner" id="login-banner">
                <div class="login-content">
                    <div class="col6">
                        <div class="login-desc">
                            <h2>Content <span>Toolbox</span></h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>
                    </div>
                    <div class="col6">
                        <div class="login-block">

                            {{ Html::image('Toolbox/images/kenwood-logo.png') }}
            
                            <p>Please register here</p>

                              @include('admin.flash.message')

                          {!! Form::open(['url'=>'register-user/toolbox','method'=>'POST','class'=>'form-horizontal']) !!}
                                <div class="form-section">
                                    <!-- <label>Name :</label> -->
                                    {!! Form::text('full_name', null , ['placeholder' => 'Full Name']) !!}

                                   @if($errors->has('full_name'))
                                        <span class="error" style="color:red;">{{ $errors->first('full_name') }}</span>
                                   @endif

                                </div>
                                
                                <div class="form-section">
                                    <!-- <label>Email :</label> -->

                                    {!! Form::text('email', null , ['placeholder' => 'Email','required'=>'required']) !!}
                                    
                                    @if($errors->has('email'))
                                        <span class="error" style="color:red;">{{ $errors->first('email') }}</span>
                                    @endif

                                </div>

                                <div class="form-section">
                                    <!-- <label>Password :</label> -->
                                    {!! Form::input('password', 'password', null , ['placeholder' => 'Password']) !!}
                                    
                                    @if($errors->has('password'))
                                        <span class="error" style="color:red;">{{ $errors->first('password') }}</span>
                                    @endif

                                </div>
                                <div class="form-section">
                                    <!-- <label>Confirm Password :</label> -->
                                    {!! Form::input('password', 'password_confirmation', null , [ 'placeholder' => 'Confirm Password']) !!}
                                    @if($errors->has('password'))
                                        <span class="error" style="color:red;">{{ $errors->first('password') }}</span>
                                    @endif

                                </div>

                                <button type="submit" class="sign-up">Register</button>
                                
                            {!! Form::close() !!}
                                
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- login content end -->

    </div><!-- wrap end -->
    <!-- custom html end -->
  </body>
</html>