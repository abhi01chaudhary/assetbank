@extends('frontend.partial.master')

@section ('content')
    <div class="section">
        <div class="content-wrapper">

            @foreach( $themes as $theme )

                <div class="col4">
                    <div class="content-block yellow">
                        <a href="{{ url('theme/'.$theme->id) }}">
                            <img src="{{ asset($theme->block_image) }}" alt="">
                            <h5>{{ $theme->name }}</h5>
                        </a>
                    </div>
                </div>

            @endforeach

        </div>
    </div><!-- main content end -->
@endsection