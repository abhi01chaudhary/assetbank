@extends('frontend.partial.master')

@section('content')
    <div class="section">
        <div class="individual-content">
            <div class="individual-block" id="title-individual">
                <a>
                    <img src="{{ asset($theme->sub_block_image) }}" alt="spring">
                    <h5>{{ $theme->name }}</h5>
                </a>
            </div>

            @foreach($theme->subTheme as $subtheme)
                <?php
                    $resource_groups = App\Models\ResourceGroup::where('sub_theme_id', $subtheme->id)->get();
                ?>

                <div class="individual-block toggle green">
                    <a href="#" class="toggle-head"><img src="{{ asset($subtheme->block_image) }}" alt="spring">
                        <h5>{{ $subtheme->name }}</h5></a>
                    <div class="toggle-body">
                        <div class="toggle-wrapper">
                            <div class="toggle-col-12">
                                <div class="toggle-content">
                                    <ul>
                                        @foreach ($resource_groups as $resource_group)
                                            @php($resources = \App\Models\Resource::where('resource_group_id', $resource_group->id)->get())
                                            <li class="list-toggle-wrapper">{{ $resource_group->name  }} {{ $resources->count() }} (zip
                                                file) <a
                                                        href="javascript:void(0)" class="list-toggle"><i
                                                            class="fa fa-angle-down" aria-hidden="true"></i></a><a
                                                        href="{{ url('resource/download/' . $resource_group->id)  }}">Download</a>
                                                <ol class="list-body">
                                                    @foreach ($resources as $resource)
                                                        <li>{{ $resource->title }}</li>
                                                    @endforeach
                                                </ol>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <form class="comment-submit-form"
                                          action="{{ url('theme/' . $theme->id . '/subtheme/' . $subtheme->id . '/add-comment') }}"
                                          method="post">
                                        {{ csrf_field() }}
                                        <textarea name="comment" rows="8"></textarea>
                                        <div class="error" style="color: red;margin-top: 5px;display: none;"></div>
                                        <button type="submit">Comment</button>
                                    </form>
                                    <div class="comment-list">
                                        @foreach ($subtheme->comments()->orderBy('id', 'desc')->get() as $comment)
                                            <div class="comment-block">
                                                <div class="comment-wrapper">
                                                    {{--<div class="comment-col-2">
                                                        <div class="comment-user">
                                                            <img src="{{ asset('Toolbox/images/user-img.jpg') }}"
                                                                 alt="user">
                                                        </div>
                                                    </div>--}}
                                                    <div class="comment-col-10">
                                                        <div class="comment-content">
                                                            <div class="comment-head">
                                                                <span class="name">{{ $comment->user->full_name }}</span>
                                                                <span class="date">{{ $comment->created_at->format('Y M d') }}</span>
                                                            </div>
                                                            <div class="comment">
                                                                <p>{{ $comment->comment }}</p>
                                                            </div>
                                                            <div class="like" style="margin-top: 15px;">
                                                                <button type="button"
                                                                        class="fa {{ Auth::user()->canLike($comment->id) ? 'fa-thumbs-o-up' : 'fa-thumbs-up' }} btn-like-comment"
                                                                        data-href="{{ url('theme/' . $theme->id . '/subtheme/' . $subtheme->id . '/comment/' . $comment->id . '/like') }}"
                                                                        style="font-size: 16px; border: none; background-color: transparent; cursor: pointer;"></button>
                                                                <span>&middot;</span>
                                                                <span class="like-count">{{ $comment->likes->count() }}</span>
                                                                <span>Likes</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    @endforeach
@endsection