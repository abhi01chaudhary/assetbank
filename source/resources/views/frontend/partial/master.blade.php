<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ (isset($page_title)) ? $page_title . ' - Content Toolbox' : 'Content Toolbox' }}</title>

    <!-- styles -->
{{ Html::style('Toolbox/kenwood/jquery-ui-1.10.4.custom.min.css') }}
{{ Html::style('Toolbox/kenwood/default.css') }}
{{ Html::style('Toolbox/kenwood/print.css') }}
{{ Html::style('Toolbox/kenwood/responsive.css') }}
{{ Html::style('Toolbox/kenwood/fonts.css') }}

{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}

<!-- Custom Css -->
{{ Html::style('https://fonts.googleapis.com/css?family=Montserrat:400,700') }}
{{ Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}
{{ Html::style('Toolbox/css/style.min.css') }}

<!-- custom js-->
    {{ Html::script('Toolbox/js/main.js') }}
    {{ Html::script('Toolbox/js/script.js') }}

    @stack('header_scripts')
    @stack('styles')

</head>
<body>

<!-- custom html start -->
<div class="wrap">

    <div class="header">
        <div class="section">
            <div class="header-wrapper">
                <div class="col6">
                    <div class="content-logo">
                        <a href="{{ url('/') }}">
                            {{ Html::image('Toolbox/images/kenwood-logo.png') }}
                        </a>
                    </div>
                </div>
                <div class="col6">
                    <div class="user-block">
                        <div class="user">
                            Welcome {{ Auth::user()->full_name }}
                            (<a href="{{ url('logout') }}">Logout</a>)
                        </div>
                        {{--<div class="search-block">
                            <form>
                                <div class="search-float search">
                                    <input type="text" name="search" placeholder="Search">
                                </div>
                                <div class="search-float type">
                                    <input type="text" name="type" placeholder="Any Type">
                                </div>
                                <div class="search-float search-btn">
                                    <button>{{ Html::image('Toolbox/images/search-icon.png') }}</button>
                                </div>
                            </form>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- header end -->

    <div class="main-content">

        @yield('content')

        <div class="footer">
            <div class="section">
                <div class="footer-content">
                    <ul>
                        <li>© 2018 Content Toolbox. </li>
                        <li><a href="#">Terms & Conditions |</a></li>
                        <li><a href="#">Privacy Policy |</a></li>
                        <li><a href="#">About Content Toolbox</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- footer end -->

    </div><!-- wrap end -->
    <!-- custom html end -->
</div>
@stack('scripts')
</body>
</html>