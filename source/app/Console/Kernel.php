<?php

namespace App\Console;

use App\Console\Commands\assetApprovalAlertToBrand;
use App\Console\Commands\assetBankAlertToSuperAdmin;
use App\Console\Commands\assetUploadAlertToSM;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        assetUploadAlertToSM::class,
        assetApprovalAlertToBrand::class,
        assetBankAlertToSuperAdmin::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('email:alertToSM')
           ->everyMinute();
        $schedule->command('email:alertToSuperAdmin')
            ->everyMinute();
        $schedule->command('email:alertToBrand')
            ->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
