<?php

namespace App\Console\Commands;

use App\Models\ImageBank;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class assetBankAlertToSuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:alertToSuperAdmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email notification to superadmin if any new activity happened in asset bank';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('user_role_id',1)->get();
        $countPendingBank =  ImageBank::where('approved_by_media','Pending')->where('sm_mail_sent_to_admin',0)->count();
        $countBrand = ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->where('brand_mail_sent_to_admin',0)->count();
        $countDownload = ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Approved')->where('download_mail_sent_to_admin',0)->count();

        foreach($users as $user) {
            // Send the email to social media

            if($countPendingBank > 0 || $countBrand > 0 || $countDownload > 0 ){
                Mail::send('admin.partials.alertToSuperAdmin', ['user' => $user,'countPendingBank'=>$countPendingBank,'countBrand'=>$countBrand,'countDownload'=>$countDownload], function ($mail) use ($user,$countPendingBank) {
                    $mail->to($user['email'])
                        ->subject('Upload Hub Notification');
                });

            }
        }
        if($countPendingBank > 0){
            $banks =  ImageBank::where('approved_by_media','Pending')->where('sm_mail_sent_to_admin',0)->get();
            foreach ($banks as $bank){
                $bank->update([
                    'sm_mail_sent_to_admin'=>1
                ]);
            }
        }
        if($countDownload > 0){
            $banks =  ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Approved')->where('download_mail_sent_to_admin',0)->get();
            foreach ($banks as $bank){
                $bank->update([
                    'download_mail_sent_to_admin'=>1
                ]);
            }
        }
        if($countBrand > 0){
            $banks =  ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->where('brand_mail_sent_to_admin',0)->get();
            foreach ($banks as $bank){
                $bank->update([
                    'brand_mail_sent_to_admin'=>1
                ]);
            }
        }

        $this->info('Email is sent to superadmin for notification!');
    }
}
