<?php

namespace App\Console\Commands;

use App\Models\ImageBank;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class assetApprovalAlertToBrand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:alertToBrand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email Notification to brand once social media approves newly uploaded asset.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('user_role_id',3)->get();

        $countPendingBank =  ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->where('mail_sent_to_brand',0)->count();

        foreach($users as $user) {
            // Send the email to social media
            if($countPendingBank > 0){
                Mail::send('admin.partials.alertToBrand', ['user' => $user,'countPendingBank'=>$countPendingBank], function ($mail) use ($user,$countPendingBank) {
                    $mail->to($user['email'])
                        ->subject($countPendingBank.' assets approved by Social Media');
                });

            }
        }
        $banks = ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Pending')->where('mail_sent_to_brand',0)->get();
        foreach($banks as $bank){
            $bank->update([
                'mail_sent_to_brand' => 1
            ]);
        }

        $this->info('Email is sent to brand for approval notification!');
    }
}
