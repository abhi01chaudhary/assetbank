<?php

namespace App\Console\Commands;

use App\Models\ImageBank;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class assetUploadAlertToSM extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:alertToSM';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email sent to social media once asset uploaded';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('user_role_id',2)->get();
        $countPendingBank =  ImageBank::where('approved_by_media','Pending')->where('mail_sent_to_sm',0)->count();

        foreach($users as $user) {
            // Send the email to social media
            if($countPendingBank > 0){
                Mail::send('admin.partials.alertToSM', ['user' => $user,'countPendingBank'=>$countPendingBank], function ($mail) use ($user,$countPendingBank) {
                    $mail->to($user['email'])
                        ->subject($countPendingBank.' assets uploaded');
                });

            }
        }
        $banks =  ImageBank::where('approved_by_media','Pending')->where('mail_sent_to_sm',0)->get();
        foreach($banks as $bank){
            $bank->update([
                'mail_sent_to_sm'=>1
            ]);
        }

        $this->info('Email is sent to social media for approval notification!');
    }
}
