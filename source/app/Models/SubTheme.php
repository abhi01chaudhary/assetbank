<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubTheme extends Model
{

    protected $fillable = ['theme_id', 'name', 'slug', 'block_image', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function theme()
    {
        return $this->belongsTo(Theme::class, 'theme_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function resourceGroup()
    {
        return $this->hasMany(ResourceGroup::class, 'sub_theme_id', 'id' );
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'stid', 'id')->where('status', 1);
    }

}
