<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageBank extends Model
{
    protected $fillable = ['user_id','image','file_name','title','tags','source','product_featured','global_rights',
        'approved_by_media','approved_by_gtm','status','mail_sent_to_sm','mail_sent_to_brand','download_mail_sent_to_admin',
    'brand_mail_sent_to_admin','sm_mail_sent_to_admin'];


    public function user(){
        return $this->belongsTo(User::class);
    }
}

