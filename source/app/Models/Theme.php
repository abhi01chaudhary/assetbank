<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    protected $fillable = ['name', 'slug', 'block_image', 'sub_block_image', 'status'];

    /*public function subTheme(){
    	return $this->belongsToMany(SubTheme::class, 'theme_subtheme_pivot');
    }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subTheme()
    {
        return $this->hasMany(SubTheme::class, 'theme_id', 'id');
    }
}
