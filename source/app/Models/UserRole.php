<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $fillable = ['role_type'];

    public function user()
    {
        return $this->hasMany('App\Models\User');
    }
}
