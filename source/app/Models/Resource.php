<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $fillable = ['user_id','sub_theme_id', 'resource','file_name','title','source'];

    public function subTheme(){
    	return $this->belongsTo(subTheme::class);
    }

}
