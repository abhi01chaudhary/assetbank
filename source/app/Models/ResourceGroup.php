<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceGroup extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subtheme()
    {
        return $this->belongsTo(SubTheme::class, 'sub_theme_id', 'id');
    }

    public function resource()
    {
        return $this->hasMany(Resource::class, 'resource_group_id', 'id');
    }
}
