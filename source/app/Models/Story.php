<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $fillable = ['id','user_id','slug','global_rights','story_approved'];

    public function bank(){
    	return $this->hasMany(StoryBank::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
