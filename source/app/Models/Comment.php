<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['uid', 'stid', 'comment', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'uid', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subtheme()
    {
        return $this->belongsTo(\App\Models\SubTheme::class, 'stid', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany(\App\Models\LikeComment::class, 'cid','id');
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        if ($this->status == 1) {
            return 'Enabled';
        } else {
            return 'disabled';
        }
    }
}
