<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketRegion extends Model
{
    protected $fillable = ['name','status'];

//    public function country(){
//        return $this->hasMany('App\Models\Country');
//    }
}
