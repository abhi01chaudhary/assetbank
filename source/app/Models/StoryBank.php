<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoryBank extends Model
{
    protected $fillable = ['story_id','title','story','filetype','filename','source','product_featured','order','link','status',];

    public function stories(){
    	return $this->belongsTo(Story::class);
    }
    
}
