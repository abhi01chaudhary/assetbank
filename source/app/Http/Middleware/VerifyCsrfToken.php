<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'admin/image/*','admin/story/*','admin/theme/{theme_id}/sub-theme/{sub_theme_id}/resource-group/{resource_group_id}/resource/{id}/delete'
    ];
}
