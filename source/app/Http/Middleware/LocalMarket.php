<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class LocalMarket
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->user_role_id != 4){
            return response('Unauthorized.',401);
        }
        return $next($request);
    }
}
