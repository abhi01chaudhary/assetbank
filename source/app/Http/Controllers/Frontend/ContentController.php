<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Theme;

class ContentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $themes = Theme::all();
        return view('frontend.index', compact('themes'));
    }

    public function getLogin()
    {
        return view('frontend.login');
    }

    public function postLogin(Request $request)
    {
        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ])) {
            return redirect('/');
        }

        Auth::logout();

        return redirect('/login')
            ->withInput($request->only('email'))
            ->withErrors([
                'email' => 'Invalid email/password'
            ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doLogout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
