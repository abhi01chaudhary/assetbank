<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class ResourceController extends Controller
{
    public function store(Request $request){

		$input = $request->all();

		$validator = Validator::make( $input, [
            'file' => 'required|mimes:jpeg,png,jpg,gif,psd,tiff,svg',
            'title' => 'required|min:3',
            'source' => 'required'
        ]);

        if ($validator->passes()) {

            $title              = $request->title;
            $source             = $request->source;

            $file = $request->file('file');

            $destinationPath = 'storage/backend/resource/bank/';

            $image = str_random(6) . '_' .time().'.'.$request->file('file')->getClientOriginalExtension();
            $inputs['file_name'] = $request->file('file')->move($destinationPath, $image);
            $inputs['file_name'] = str_replace('\\','/',$inputs['file_name']);

            $size = filesize($destinationPath.'/'.$image);

            $this->image->user_id     = Auth::user()->id;
            $this->image->image       = $image;
            $this->image->file_name   = $file->getClientOriginalName();
            $this->image->title       = $title;
            $this->image->source      = $source;

            $this->image->save();
            $ext = $file->getClientOriginalExtension();

            if($ext == 'psd'){
                $thumbnailUrl = url('/').'/storage/backend/images/bank/psd.jpg';
            }elseif($ext == 'tif'){
                $thumbnailUrl = url('/').'/storage/backend/images/bank/tif.png';
            }else{
                $thumbnailUrl =  URL::to( $inputs['file_name'] );
            }
            $output = [[
                'name' => $file->getClientOriginalName(),
                'title' => $title,
                'source' => $source,
                'size' => $size,
                'type' => 'image/jpeg',
                'url'  => URL::to( $inputs['file_name'] ),
                'thumbnailUrl' => $thumbnailUrl,
                'deleteUrl'=>URL::to( '/admin/image/delete?name='.$image ),
                'deleteType'=>'DELETE',
                'message'=>'Thank you for uploading your Social Media images! High Five!Your images have been submitted for approval. '
            ]];

            return ['files' => $output ];
        }

    }
}
