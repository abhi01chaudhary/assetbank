<?php

namespace App\Http\Controllers\Frontend;

use App\Models\ResourceGroup;
use App\Models\SubTheme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Resource;
use Illuminate\Support\Facades\File;
use ZipArchive;
use URL;

class ResourceZipController extends Controller
{
    public function __construct( Resource $resource )
    {
        $this->middleware('auth');
        $this->resource = $resource;
    }

    public function downloadResource( $id, Request $request )
    {

        $files = Resource::where('resource_group_id', $request->id)->get();

        $resource_group = ResourceGroup::where('id', $request->id)->first();

//        dd($resource_group);

        $subtheme = SubTheme::find($resource_group->sub_theme_id);

//        dd($subtheme);

        $public_dir 	= realpath(base_path().'/..');

        $download_id 	= str_random(6) . '_' .time();

        $zip_file_name 	= $subtheme->name . '-' . $resource_group->name."-{$download_id}.zip";

        $zip = new ZipArchive();

        if ( $zip->open( $public_dir . '/storage/downloads/' . $zip_file_name, ZipArchive::CREATE ) === TRUE ) {

            foreach( $files as $file ) {
                $type = File::extension('image/resources/'.$file->resource);
                $fileName = $file->title. '.' . $type;
                $zip->addFile('image/resources/'.$file->resource, $fileName);
            }
            $zip->close();
        }

        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );

        $file_to_path =	'storage/downloads/'.$zip_file_name;

        if( file_exists( $file_to_path ) ){

            return response()->download( $file_to_path, $zip_file_name, $headers );
        }


    }



}
