<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Comment;
use App\Models\LikeComment;
use App\Models\SubTheme;
use App\Models\Theme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SubThemeController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        $theme = Theme::find($id);
        if (!$theme) {
            abort(404);
        }
        return view('frontend.sub-theme', [
            'theme' => $theme,
            'page_title' => $theme->name
        ]);
    }

    /**
     * @param $id
     * @param $sub_theme_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_comment($id, $sub_theme_id, Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $theme = Theme::find($id);
        if(!$theme) {
            return response()->json([
                'status' => 0,
                'message' => 'Theme doesn\'t exit'
            ], 404);
        }
        $subtheme = SubTheme::find($sub_theme_id);
        if(!$subtheme) {
            return response()->json([
                'status' => 0,
                'message' => 'SubTheme doesn\'t exit'
            ], 404);
        }
        $validator = Validator::make($request->all(), [
            'comment' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first()
            ], 400);
        }
        $comment = Comment::create([
            'uid' => Auth::user()->id,
            'stid' => $subtheme->id,
            'comment' => $request->input('comment')
        ]);

        return response()->json([
            'status' => 1,
            'message' => 'Comment has been added and will be visible after moderation!',
//            'comment' => $comment->toArray(),
//            'user' => Auth::user()->toArray(),
//            'created_at' => $comment->created_at->format('Y M d'),
//            'comment_like_url' => url('theme/' . $theme->id . '/subtheme/' . $subtheme->id . '/comment/' . $comment->id . '/like')
        ]);
    }

    /**
     * @param $id
     * @param $sub_theme_id
     * @param $comment_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function like($id, $sub_theme_id, $comment_id, Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $theme = Theme::find($id);
        if(!$theme) {
            return response()->json([
                'status' => 0,
                'message' => 'Theme doesn\'t exit'
            ], 404);
        }
        $subtheme = SubTheme::find($sub_theme_id);
        if(!$subtheme) {
            return response()->json([
                'status' => 0,
                'message' => 'SubTheme doesn\'t exit'
            ], 404);
        }
        $comment = Comment::find($comment_id);
        if(!$comment) {
            return response()->json([
                'status' => 0,
                'message' => 'Comment doesn\'t exit'
            ], 404);
        }
        if (Auth::user()->canLike($comment->id)) {
            LikeComment::create([
                'uid' => Auth::user()->id,
                'cid' => $comment->id
            ]);
            return response()->json([
                'status' => 1,
                'count' => LikeComment::where('cid', $comment->id)->count()
            ]);
        } else {
            LikeComment::where('uid', Auth::user()->id)->where('cid', $comment->id)->delete();
            return response()->json([
                'status' => 0,
                'count' => LikeComment::where('cid', $comment->id)->count()
            ]);
        }
    }
}
