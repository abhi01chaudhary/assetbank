<?php

namespace App\Http\Controllers\Admin;

use App\Models\SubTheme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Theme;

class ThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $themes = Theme::latest()->get();
        return view('admin.theme.index', [
            'active_menu' => 'theme'
        ], compact('themes'))->with('title', 'All Theme');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        return view('admin.theme.create', [
            'active_menu' => 'theme'
        ])->with('title', 'Create a Theme');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
//            'slug'=>'required|unique:sub_themes',
            'block_image'=>'required',
            'sub_block_image'=>'required',
        ],[
            'block_image.required'=> 'Theme Block image is required!',
            'sub_block_image.required'=>'Theme header block image is required'
        ]);

        $input = $request->all();

        $destinationPath = 'image/blockImage';
        $subDestinationPath = 'image/SubBlockImage';

        if($request->block_image){
            $image = str_random(6) . '_' . time() . "-" .
                $request->file('block_image')->getClientOriginalName();
            $input['block_image'] = $request->file('block_image')->move($destinationPath, $image);
            $input['block_image'] = str_replace('\\','/',$input['block_image']);

        }

        if($request->sub_block_image){
            $file = str_random(6) . '_' . time() . "-" .
                $request->file('sub_block_image')->getClientOriginalName();
            $input['sub_block_image'] = $request->file('sub_block_image')->move($subDestinationPath, $file);
            $input['sub_block_image'] = str_replace('\\','/',$input['sub_block_image']);
        }

        $theme = Theme::create($input);

        session()->flash('message','Theme has been created.');
        return redirect('admin/theme/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $theme = Theme::find($id);

        if (!$theme) {
            abort(404);
        }

        $subthemes = SubTheme::where('theme_id', $theme->id)->latest()->get();

        return view('admin.theme.sub-theme.index', [
            'active_menu' => 'theme'
        ], compact('theme', 'subthemes'))->with('title', 'All Subtheme of ' . $theme->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $theme = Theme::find($id);
        if (!$theme) {
            abort(404);
        }
        return view('admin.theme.edit', [
            'active_menu' => 'theme'
        ], compact('theme'))->with('title', 'Edit a Theme');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $theme = Theme::find($id);
        if (!$theme) {
            abort(404);
        }

        $this->validate($request,[
            'name'=>'required',
//            'slug'=>'required',
            'block_image'=>'sometimes',
            'sub_block_image'=>'sometimes',
        ]);


        // if ($request->input('slug') != $theme->slug) {
        //     if(Theme::where('slug', $request->input('slug'))->count()) {
        //         return back()->withErrors([
        //             'slug' => 'Slug is already in use!'
        //         ]);
        //     }
        // }

        $destinationPath = 'image/blockImage';
        $subDestinationPath = 'image/SubBlockImage';

        $theme_block_image = $theme->block_image;
        if($request->block_image){
            $image = str_random(6) . '_' . time() . "-" .
                $request->file('block_image')->getClientOriginalName();
            $theme_block_image = $request->file('block_image')->move($destinationPath, $image);
            $theme_block_image = str_replace('\\','/', $theme_block_image);
        }

        $theme_sub_block_image = $theme->sub_block_image;
        if($request->sub_block_image){
            $file = str_random(6) . '_' . time() . "-" .
                $request->file('sub_block_image')->getClientOriginalName();
            $theme_sub_block_image = $request->file('sub_block_image')->move($subDestinationPath, $file);
            $theme_sub_block_image = str_replace('\\','/',$theme_sub_block_image);
        }

        $theme->update([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'block_image' => $theme_block_image,
            'sub_block_image' => $theme_sub_block_image
        ]);

        session()->flash('message','Theme (' . $theme->name . ') has been updated!');
        return redirect('admin/theme/');
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        if (!request()->ajax()) {
            return false;
        }

        $theme = Theme::find($id);
        if (!$theme) {
            return false;
        }
        $theme->delete();
        session()->flash('message', 'Theme has been deleted.');
        return response()->json(array(
            'status' => 'success',
        ));
    }
}
