<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ZipArchive;
use URL;
use App\Models\ImageBank;


class ZipArchiveController extends Controller
{
    
	public function __construct( ImageBank $image )
	{
		$this->middleware('auth');

        $this->image = $image;
	}

    public function getIndex( Request $request )
    {

      	$ids = $request->ids;

      	$files 			= $this->image->whereIn('id',explode(",",$ids))->get();

      	$csv_file_name 	= $this->create_array_to_csv( $files );

    	$public_dir 	= realpath(base_path().'/..');

    	$download_id 	= str_random(6) . '_' .time();
    	
        $zip_file_name 	= "images-{$download_id}.zip";

        $zip = new ZipArchive();

        if ( $zip->open( $public_dir . '/storage/downloads/' . $zip_file_name, ZipArchive::CREATE ) === TRUE ) {

        	$zip->addFile($csv_file_name , 'image-summary.csv' );

		    foreach( $files as $file ) {

		        $zip->addFile( 'storage/backend/images/bank/'.$file->image , $file->image );
		    }

		    $zip->close();
		}

        // Set Header
        $headers = array(

            'Content-Type' => 'application/octet-stream',
        );

        $file_to_path			=	'storage/downloads/'.$zip_file_name;

        // return $file_to_path;

        
        if( file_exists( $file_to_path ) ){

            return response()->download( $file_to_path, $zip_file_name, $headers );
        }
       
    }


    private function create_array_to_csv( $data = [] ){

    	$download_id 	= str_random(6) . '_' .time();

    	$file_name 		= "storage/downloads/image-summary-{$download_id}.csv";


		$file = fopen( $file_name, 'w');

		fputcsv( $file, 
				[
					'Image'
					, 'File Name'
					, 'Title'
					, 'Tags'
					, 'Source'
					, 'Product Featured?'
				]
		 );
 
	
		// save each row of the data

		foreach ( $data as $row )
		{
			$output 	= [ 
							$row->image
							, $row->file_name
							, $row->title
							, $row->tags
							, $row->source 
							, $row->product_featured
						] ;

		
		    fputcsv($file, $output);
		}
		 
		// Close the file
		fclose($file);

		// Make sure nothing else is sent, our file is done

		return $file_name;
		exit;

    }

    private function download_csv( $rows ){

    	$file_name = 'image-summary.csv';
 
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Description: File Transfer');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename={$file_name}");
		header("Expires: 0");
		header("Pragma: public");

		$fh = @fopen( 'php://output', 'w' );

		$header_displayed = false;

		foreach ( $rows as $data ) {
		    // Add a header row if it hasn't been added yet
		    if ( !$headerDisplayed ) {
		        // Use the keys from $data as the titles
		        fputcsv($fh, array_keys($data));

		        $header_displayed = true;
		    }
		 
		    // Put the data into the stream
		    fputcsv($fh, $data);
		}
		// Close the file
		fclose($fh);
		// Make sure nothing else is sent, our file is done
		exit;

    }

}
