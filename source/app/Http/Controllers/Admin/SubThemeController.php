<?php

namespace App\Http\Controllers\Admin;

use App\Models\ResourceGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubTheme;
use App\Models\Theme;

class SubThemeController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($theme_id)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        return view('admin.theme.sub-theme.create', [
            'active_menu' => 'theme'
        ], compact('theme'))->with('title', 'Add a Subtheme');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($theme_id, Request $request)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }

        $this->validate($request, [
            'name' => 'required',
            // 'slug'=>'required|unique:sub_themes',
            'block_image' => 'required'
        ], [
            'name.required' => 'The Sub theme name is required!',
        ]);

        $input = $request->all();

        $destinationPath = 'image/subThemeImage';

        $image = str_random(6) . '_' . time() . "-" .
            $request->file('block_image')->getClientOriginalName();
        $input['block_image'] = $request->file('block_image')->move($destinationPath, $image);
        $input['block_image'] = str_replace('\\', '/', $input['block_image']);
        $input['theme_id'] = $theme->id;

        $subTheme = SubTheme::create($input);
        session()->flash('message', 'Sub Theme has been created.');
        return redirect('admin/theme/' . $theme->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($theme_id, $id)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $subtheme = $theme->subTheme->find($id);

        if (!$subtheme) {
            abort(404);
        }

        $resource_groups = ResourceGroup::where('sub_theme_id', $subtheme->id)->latest()->get();

        return view('admin.theme.sub-theme.resource-group.index', [
            'active_menu' => 'theme'
        ], compact('theme', 'subtheme', 'resource_groups'))->with('title', 'All Resource Group of ' . $subtheme->name);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($theme_id, $id, Request $request)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $subtheme = $theme->subTheme->find($id);

        if (!$subtheme) {
            abort(404);
        }

        return view('admin.theme.sub-theme.edit', [
            'active_menu' => 'theme'
        ], compact(['theme', 'subtheme']))->with('title', 'Edit Subtheme');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($theme_id, $id, Request $request)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }
        $subtheme = SubTheme::find($id);
        if (!$subtheme) {
            abort(404);
        }
        $this->validate($request, [
            'name' => 'required',
            // 'slug'=>'required',
            'block_image' => 'sometimes'
        ], [
            'name.required' => 'The Sub theme name is required!'
        ]);

        $subtheme_block_image = $subtheme->block_image;
        if ($request->hasFile('block_image')) {
            $destinationPath = 'image/subThemeImage';

            $image = str_random(6) . '_' . time() . "-" .
                $request->file('block_image')->getClientOriginalName();
            $block_image = $request->file('block_image')->move($destinationPath, $image);
            $subtheme_block_image = str_replace('\\', '/', $block_image);
        }

        $subtheme->update([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'block_image' => $subtheme_block_image
        ]);

        session()->flash('message', 'Sub Theme (' . $subtheme->name . ') is updated!');
        return redirect('admin/theme/' . $theme->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($theme_id, $id)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }
        $subtheme = SubTheme::find($id);
        if (!$subtheme) {
            abort(404);
        }
        $subtheme->delete();
        session()->flash('message', 'Sub theme has been deleted.');
        return response()->json(array(
            'status' => 'success',
        ));
    }
}
