<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Theme;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Hash;

class DashboardController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth',['only'=>'index','getProfile','updatePassword','updateBasicInformation']);
        $this->middleware('guest',['only'=>'getLogin']);
    }

    public function index(){

        $themes = Theme::latest()->take(5)->get();

        $comments = Comment::latest()->take(3)->get();

        return view('admin.dashboard.index', [
            'active_menu' => 'dashboard'
        ], compact('themes','comments'))->with('title','Kenwood Social Media Asset Upload Hub');
    }

    public function getLogin(){
        return view('admin.dashboard.login')->with('title','Kenwood Social Media Asset Upload Hub | Admin Login');
    }

    public function postLogin(Request $request){
        // $this->validate($request,[
        //     'g-recaptcha-response' => 'required',
        // ],[
        //     'g-recaptcha-response.required' => 'Captcha is required'
        // ]);
        if(Auth::attempt([
            'email'=>$request->email,
            'password'=>$request->password
        ]) && Auth::user()->status == 1 )
        {

            if(Auth::user()->user_role_id == 4){
                return redirect('admin/image/create');
            }
            return redirect('admin/dashboard');
        }

        Auth::logout();
        return redirect('/')
            ->withInput($request->only('email'))
            ->withErrors([
                'email' => 'Invalid email/password'
            ]);
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/admin')->with('title','Kenwood Social Media Asset Upload Hub | Admin Login');
    }

    public function getProfile()
    {
        $user = Auth::user();

        return view('admin.dashboard.profile',compact('user'))->with('title','Kenwood Social Media Asset Upload Hub | Profile');
    }

    public function updatePassword(Request $request)
    {
        if (!request()->ajax()) {
            return false;
        }

        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|min:6|alpha_dash|confirmed',
            'password_confirmation' => 'required|min:6|alpha_dash'
        ]);

        if ($validator->fails()) {

            return response()->json(array(
                'status' => 'fails',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($validator->passes()) {

            if (Hash::check($request->current_password, Auth::user()->password)) {


                Auth::user()->update([
                    'password' => bcrypt($request->password)
                ]);


                session()->flash('message', 'Password Successfully Changed.');

                return response()->json(array(
                    'status' => 'success',
                    'url' => url('admin/profile')
                ));

            } else {
                $validator->errors()->add('current_password',
                    'Your current password is incorrect, please try again.');
                return response()->json(array(
                    'status' => 'fails',
                    'errors' => $validator->getMessageBag()->toArray()
                ));
            }
        }
    }


    public function updateBasicInformation(Request $request){

        if (!request()->ajax()) {
            return false;
        }

        $validator = Validator::make($request->all(), [
            'full_name'=>'required',
        ]);

        if ($validator->fails()) {

            return response()->json(array(
                'status' => 'fails',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($validator->passes()) {

            $inputs = $request->all();

            Auth::user()->update($inputs);

            session()->flash('basic_message', 'Basic Information Successfully Changed.');

            return response()->json(array(
                'status' => 'success',
                'url' => url('admin/profile')
            ));

        }
    }

    public function change_view_toolbox(Request $request)
    {
        $request->session()->put('view', 'toolbox');
        return redirect('/admin/dashboard/');
    }

    public function change_view_content_hub(Request $request)
    {
        $request->session()->put('view', 'content-hub');
        return redirect('/admin/dashboard/');
    }
}
