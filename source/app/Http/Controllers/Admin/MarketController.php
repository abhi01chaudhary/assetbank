<?php

namespace App\Http\Controllers\Admin;

use App\Models\MarketRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MarketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('super-admin');
    }

    public function index()
    {
        $regions = MarketRegion::latest()->get();
        return view('admin.region.index', compact('regions'))->with('title','All Market Regions');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.region.create')->with('title','Create Market Region');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->ajax()){
            return false;
        }
        $validator = Validator::make($request->all(),[
            'name'=>'required|unique:market_regions',
        ]);

        if($validator->fails()){
            return response()->json(array(
                'status'=>'fails',
                'errors'=>$validator->getMessageBag()->toArray()
            ));
        }
        if($validator->passes()){
            $inputs = $request->all();
            MarketRegion::create($inputs);
            session()->flash('message','Market Region Created');
            return response()->json(array(
                'status' => 'success',
                'url'    => url('admin/market-region')
            ));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!request()->ajax()){
            return false;
        }


        $region = MarketRegion::find($id);

        return view('admin.region.edit',
            compact(
                'region'

            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->ajax()){
            return false;
        }
        $validator = Validator::make($request->all(),[
            'name'=>"required|unique:market_regions,name,$id,id",
        ]);

        if($validator->fails()){

            return response()->json(array(
                'status'    => 'fails',
                'errors'    => $validator->getMessageBag()->toArray()
            ));
        }
        if($validator->passes()){
            $region = MarketRegion::find($id);
            $region->update($request->all());

            session()->flash('message','Market Region Updated');

            return response()->json(array(
                'status' => 'success',
                'url'    => url('admin/market-region')
            ));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        $region = MarketRegion::find($id);

        $region->delete();

        session()->flash('message', 'Market Region Deleted.');

        return response()->json(array(
            'status' => 'success',
            'url'    => url('admin/market-region')
        ));
    }
}
