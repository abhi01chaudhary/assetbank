<?php

namespace App\Http\Controllers\Admin;

use App\Models\ResourceGroup;
use App\Models\SubTheme;
use App\Models\Theme;
use App\Models\Resource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResourceGroupController extends Controller
{
    /**
     * @param $theme_id
     * @param $sub_theme_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($theme_id, $sub_theme_id)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }
        $subtheme = SubTheme::find($sub_theme_id);
        if (!$subtheme) {
            abort(404);
        }
        return view('admin.theme.sub-theme.resource-group.create', [
            'active_menu' => 'theme'
        ], compact('theme', 'subtheme'))->with('title', 'Add a Resource Group');
    }

    /**
     * @param $theme_id
     * @param $sub_theme_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($theme_id, $sub_theme_id, Request $request)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }
        $subtheme = SubTheme::find($sub_theme_id);
        if (!$subtheme) {
            abort(404);
        }

        $this->validate($request, [
            'name' => 'required'
        ]);

        ResourceGroup::create([
            'name' => $request->input('name'),
            'sub_theme_id' => $subtheme->id
        ]);

        session()->flash('message', 'Content Group has been created.');
        return redirect('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id);
    }

    /**
     * @param $theme_id
     * @param $sub_theme_id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($theme_id, $sub_theme_id, $id)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }
        $subtheme = SubTheme::find($sub_theme_id);
        if (!$subtheme) {
            abort(404);
        }
        $resource_group = ResourceGroup::find($id);
        if (!$resource_group) {
            abort(404);
        }
        return view('admin.theme.sub-theme.resource-group.edit', [
            'active_menu' => 'theme'
        ], compact('theme', 'subtheme', 'resource_group'))->with('title', 'Edit a Resource Group');
    }

    /**
     * @param $theme_id
     * @param $sub_theme_id
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($theme_id, $sub_theme_id, $id, Request $request)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }
        $subtheme = SubTheme::find($sub_theme_id);
        if (!$subtheme) {
            abort(404);
        }
        $resource_group = ResourceGroup::find($id);
        if (!$resource_group) {
            abort(404);
        }
        $this->validate($request,[
            'name'=>'required'
        ]);
        $resource_group->update([
            'name' => $request->input('name')
        ]);

        session()->flash('message','Content group has been updated.');
        return redirect('admin/theme/' . $theme->id . '/sub-theme/' . $subtheme->id);
    }

    /**
     * @param $theme_id
     * @param $sub_theme_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($theme_id, $sub_theme_id, $id)
    {
        $theme = Theme::find($theme_id);
        if (!$theme) {
            abort(404);
        }
        $subtheme = SubTheme::find($sub_theme_id);
        if (!$subtheme) {
            abort(404);
        }
        $resource_group = ResourceGroup::find($id);
        if (!$resource_group) {
            abort(404);
        }
        $resource_group->delete();
        session()->flash('message', 'Content Group has been deleted.');
        return response()->json(array(
            'status' => 'success',
        ));
    }

    public function show($theme_id, $sub_theme_id, $id)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $subtheme = $theme->SubTheme->find($sub_theme_id);

        if (!$subtheme) {
            abort(404);
        }

        $resource_group = $subtheme->ResourceGroup->find($id);

        if (!$resource_group) {
            abort(404);
        }

        $resources = Resource::where('resource_group_id', $resource_group->id)->latest()->get();

        return view('admin.theme.sub-theme.resource-group.resource.index', [
            'active_menu' => 'theme'
        ], compact('theme', 'subtheme', 'resource_group', 'resources'))->with('title', 'All Resources of ' . $resource_group->name);
    }
}
