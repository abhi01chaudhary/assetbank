<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('super-admin');
    }

    public function index()
    {
        $users = User::where('status', 1)->latest()->get();
        return view('admin.user.index', [
            'active_menu' => 'user'
        ], compact('users'))->with('title', 'All Users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = UserRole::orderBy('role_type', 'ASC')->pluck('role_type', 'id');
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id');
        return view('admin.user.create', [
            'active_menu' => 'user'
        ], compact('roles', 'countries'))->with('title', 'Create User');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax()) {
            return false;
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'full_name' => 'required',
            'user_role_id' => 'required',
            'country_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'status' => 'fails',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($validator->passes()) {
            $inputs = $request->all();
            $inputs['password'] = bcrypt($request->password);
            User::create([
                'full_name' => $request->full_name,
                'email' => $request->email,
                'country_id' => $request->country_id,
                'password' => bcrypt($request->password),
                'contact_number' => $request->contact_number,
                'user_role_id' => $request->user_role_id,
                'status' => 1
            ]);
            $name = $request->full_name;
            $email = $request->email;
            $password = $request->password;
            Mail::send('admin.partials.account-created',
                ['name' => $name,
                    'email' => $email,
                    'password' => $password
                ],
                function ($message)
                use ($name, $email, $password) {
                    $message->to($email)->subject('Account Created');
                });
            session()->flash('message', 'User Created');
            return response()->json(array(
                'status' => 'success',
                'url' => url('admin/user')
            ));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!request()->ajax()) {
            return false;
        }


        $user = User::find($id);

        $roles = UserRole::orderBy('role_type', 'ASC')->pluck('role_type', 'id');
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id');

        return view('admin.user.edit', [
            'active_menu' => 'user'
        ],
            compact(
                'user',
                'roles',
                'countries'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->ajax()) {
            return false;
        }
        $validator = Validator::make($request->all(), [
            'email' => "required|unique:users,email,$id,id",
            'full_name' => 'required',
            'user_role_id' => 'required',
            'country_id' => 'required'
        ]);

        if ($validator->fails()) {

            return response()->json(array(
                'status' => 'fails',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($validator->passes()) {
            $user = User::find($id);
            $user->update($request->all());

            session()->flash('message', 'User Updated');

            return response()->json(array(
                'status' => 'success',
                'url' => url('admin/user')
            ));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!request()->ajax()) {
            return false;
        }

        $user = User::find($id);

        $user->delete();

        session()->flash('message', 'User Deleted.');

        return response()->json(array(
            'status' => 'success',
            'url' => url('admin/user')
        ));
    }

    public function getApprovalRequest()
    {
        $users = User::where('status', 0)->get();
        return view('admin.user.approve-user', compact('users'))->with('title', 'Approve User');
    }

    public function approveUserRequest($id)
    {
        if (!request()->ajax()) {
            return false;
        }

        $user = User::find($id);

        $user->update([
            'status' => 1
        ]);

        $name = $user->full_name;

        $email = $user->email;

        Mail::send('admin.partials.local-market-account-approved',
            ['name' => $name,
                'email' => $email,
            ],
            function ($message)
            use ($email, $name) {
                $message->to($email)->subject('Account Approved');
            });

        session()->flash('message', 'User Approved.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}
