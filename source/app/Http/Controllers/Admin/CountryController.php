<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\MarketRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('super-admin');
    }

    public function index()
    {
        $countries = Country::latest()->get();
        return view('admin.country.index', [
            'active_menu' => 'country'
        ], compact('countries'))->with('title','All Countries');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = MarketRegion::orderBy('name','ASC')->pluck('name','id');
        return view('admin.country.create', [
            'active_menu' => 'country'
        ], compact('regions'))->with('title','Create Country');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->ajax()){
            return false;
        }
        $validator = Validator::make($request->all(),[
            'name'=>'required|unique:countries',
        ]);

        if($validator->fails()){
            return response()->json(array(
                'status'=>'fails',
                'errors'=>$validator->getMessageBag()->toArray()
            ));
        }
        if($validator->passes()){
            $inputs = $request->all();
            Country::create($inputs);
            session()->flash('message','Country Created');
            return response()->json(array(
                'status' => 'success',
                'url'    => url('admin/country')
            ));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!request()->ajax()){
            return false;
        }


        $country = Country::find($id);

        $regions = MarketRegion::orderBy('name','ASC')->pluck('name','id');

        return view('admin.country.edit',
            compact(
                'country',
                'regions'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->ajax()){
            return false;
        }
        $validator = Validator::make($request->all(),[
            'name'=>"required|unique:countries,name,$id,id",
        ]);

        if($validator->fails()){

            return response()->json(array(
                'status'    => 'fails',
                'errors'    => $validator->getMessageBag()->toArray()
            ));
        }
        if($validator->passes()){
            $country = Country::find($id);
            $country->update($request->all());

            session()->flash('message','Country Updated');

            return response()->json(array(
                'status' => 'success',
                'url'    => url('admin/country')
            ));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        $country = Country::find($id);

        $country->delete();

        session()->flash('message', 'Country Deleted.');

        return response()->json(array(
            'status' => 'success',
            'url'    => url('admin/country')
        ));
    }
}
