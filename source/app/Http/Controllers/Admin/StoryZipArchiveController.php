<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Story;
use ZipArchive;
use URL;

class StoryZipArchiveController extends Controller
{
    public function __construct( Story $story )
	{
		$this->middleware('auth');

        $this->story = $story;
	}

    public function getIndex( Request $request )
    {

      	$ids = $request->ids;

      	$files = $this->story->whereIn('id',explode(",",$ids))->get();

      	$data = [];

      	foreach($files as $file){

      		$data[] = $file->bank;

      	}

      	$csv_file_name 	= $this->create_array_to_csv( $data );

    	$public_dir 	= realpath(base_path().'/..');

    	$download_id 	= str_random(6) . '_' .time();
    	
        $zip_file_name 	= "story-{$download_id}.zip";

        $zip = new ZipArchive();

        if ( $zip->open( $public_dir . '/storage/downloads/' . $zip_file_name, ZipArchive::CREATE ) === TRUE ) {

        	$zip->addFile($csv_file_name , 'story-summary.csv');

		    foreach( $files as $file ) {

		    	$stories = Story::find($file->id);

		    	foreach($stories->bank as $data)
		    	{
		    		$zip->addFile('storage/backend/stories/bank/'.'Story-'.$stories->id.'-'.$stories->slug.'/'.$data->story, 'stories/'.'Story-'.$stories->id.'-'.$stories->slug.'/'.$data->story);
		    	}

		    }

		    $zip->close();
		}

        // Set Header

        $headers = array(

            'Content-Type' => 'application/octet-stream',
        );

        $file_to_path =	'storage/downloads/'.$zip_file_name;

        if( file_exists( $file_to_path ) ){

            return response()->download( $file_to_path, $zip_file_name, $headers );
        }
       
    }


    private function create_array_to_csv( $data = [] ){

    	$download_id 	= str_random(6) . '_' .time();

    	$file_name 		= "storage/downloads/story-summary-{$download_id}.csv";

		$file = fopen( $file_name, 'w');

		fputcsv( $file, 
				[	
					'Story Id'
					, 'Story Media'
					, 'File Name'
					, 'Title'
					, 'Tags'
					, 'Source'
					, 'Product Featured?'
				]
		 );

		foreach ( $data as $row )
		{
			
			foreach($row as $new){

				$output 	= [ 
							'Story '.$new->story_id
							,$new->story
							, $new->file_name
							, $new->title
							, $new->tags
							, $new->source 
							, $new->product_featured
						] ;
				
		    fputcsv($file, $output);

			}
			
		}
		 
		// Close the file
		fclose($file);

		// Make sure nothing else is sent, our file is done

		return $file_name;

		exit;

    }

    private function download_csv( $rows ){

    	$file_name = 'full-story-summary.csv';
 
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Description: File Transfer');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename={$file_name}");
		header("Expires: 0");
		header("Pragma: public");

		$fh = @fopen( 'php://output', 'w' );

		$header_displayed = false;

		foreach ( $rows as $data ) {
		    // Add a header row if it hasn't been added yet
		    if ( !$headerDisplayed ) {
		        // Use the keys from $data as the titles
		        fputcsv($fh, array_keys($data));

		        $header_displayed = true;
		    }
		 
		    // Put the data into the stream
		    fputcsv($fh, $data);
		}
		// Close the file
		fclose($fh);
		// Make sure nothing else is sent, our file is done
		exit;

    }
}
