<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * CommentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('super-admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $comments = Comment::all();
        return view('admin.comment.index', [
            'active_menu' => 'comments'
        ], compact('comments'))->with('title', 'All Comments');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function enable($id)
    {
        $comment = Comment::find($id);
        if (!$comment) {
            abort(404);
        }
        $comment->update([
            'status' => 1
        ]);

        session()->flash('message','Comment has been enabled!');

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function disable($id)
    {
        $comment = Comment::find($id);
        if (!$comment) {
            abort(404);
        }
        $comment->update([
            'status' => 0
        ]);

        session()->flash('message','Comment has been disabled!');

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        if (!$comment) {
            abort(404);
        }
        $comment->delete();
        session()->flash('message', 'Comment has been deleted.');
        return response()->json(array(
            'status' => 'success',
        ));
    }
}
