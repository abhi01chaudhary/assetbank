<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Theme;

use App\Models\Resource;
use Validator;
use Illuminate\Support\Facades\URL;
use File;

class ResourceController extends Controller
{

    public function __construct( Resource $resource )
    {
        $this->middleware('auth');
        $this->resource = $resource;
    }

//    public function index()
//    {
//        $resources = Resource::all();
//        return view('admin.resource.index', [
//            'active_menu' => 'resources'
//        ], compact('resources'))->with('title','All Resources');
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create($theme_id, $sub_theme_id, $resource_group_id)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $subtheme = $theme->subTheme->find($sub_theme_id);

        if (!$subtheme) {
            abort(404);
        }

        $resource_group = $subtheme->resourceGroup->find($resource_group_id);

        if (!$resource_group) {
            abort(404);
        }

        $url = \Illuminate\Support\Facades\URL::to('/').'admin/theme/'.$theme->id.'/sub-theme/'.$subtheme->id.'/resource-group/'.$resource_group->id.'/resource/store';

        return view('admin.theme.sub-theme.resource-group.resource.create', [
            'active_menu' => 'theme'
        ],compact('url','resource_group','theme','subtheme','resource_group') )->with('title','Create Resource');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($theme_id, $sub_theme_id, $resource_group_id, Request $request)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $sub_theme = $theme->subTheme->find($sub_theme_id);

        if (!$sub_theme) {
            abort(404);
        }

        $resource_group = $sub_theme->resourceGroup->find($resource_group_id);

        if (!$resource_group) {
            abort(404);
        }

        $input = $request->all();

        // $sub_theme_id = $request->header('id');

        $validator = Validator::make( $input, [
            'file' => 'required|mimes:jpeg,png,jpg,gif,psd,tiff,svg,doc,pdf,docx,pptx,mp4,mov|max:400000',
            'title' => 'required|min:3',
        ]);

        if ($validator->passes()) {

            $title = $request->title;

            $file = $request->file('file');

            $destinationPath = 'image/resources/';

            $image = str_random(6) . '_' .time().'.'.$request->file('file')->getClientOriginalExtension();
            $inputs['file_name'] = $request->file('file')->move($destinationPath, $image);
            $inputs['file_name'] = str_replace('\\','/',$inputs['file_name']);

            $size = filesize($destinationPath.'/'.$image);

            $this->resource->user_id = $request->user_id;
            $this->resource->resource    = $image;
            $this->resource->file_name   = $file->getClientOriginalName();
            $this->resource->title       = $title;
            $this->resource->resource_group_id = $resource_group_id;

            $inputs = [
                'user_id'=> $this->resource->user_id,
                'resource'=>$this->resource->resource,
                'file_name'=>$this->resource->file_name,
                'title'=>$this->resource->title,
                'resource_group_id'=>$this->resource->resource_group_id
            ];

            $this->resource->save();

            $ext = $file->getClientOriginalExtension();

            if($ext == 'psd'){
                $thumbnailUrl = url('/').'/storage/backend/images/bank/psd.jpg';
            }elseif($ext == 'tif'){
                $thumbnailUrl = url('/').'/storage/backend/images/bank/tif.png';
            }elseif($ext == 'mp4' || $ext == 'mov' || $ext == 'MOV'){
                $thumbnailUrl = url('/').'/storage/backend/stories/bank/video.png';
            }else{
                $thumbnailUrl = url('/').'/image/resources/'.$this->resource->resource;
            }

            $output = [[
                'name' => $file->getClientOriginalName(),
                'title' => $title,
                'resource_group_id' => $resource_group_id,
                'size' => $size,
                'type' => 'image/jpeg',
                'url'  => URL::to( $inputs['file_name'] ),
                'thumbnailUrl' => $thumbnailUrl,
                'deleteUrl'=>URL::to('/admin/theme/'.$theme->id.'/sub-theme/'.$sub_theme->id.'/resource-group/'.$resource_group->id.'/resource/'.$this->resource->id.'/delete'),
                'deleteType'=>'DELETE',
                'message'=>'Uploaded successfully!'
            ]];

            return ['files' => $output ];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($theme_id, $sub_theme_id, $resource_group_id, $resource_id)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $sub_theme = $theme->subTheme->find($sub_theme_id);

        if (!$sub_theme) {
            abort(404);
        }

        $resource_group = $sub_theme->resourceGroup->find($resource_group_id);

        if (!$resource_group) {
            abort(404);
        }

        $resource = $resource_group->resource->find($resource_id);

        if (!$resource) {
            abort(404);
        }

        return view('admin.theme.sub-theme.resource-group.resource.edit', [
            'active_menu' => 'resources'
        ], compact('resource','resource_group','sub_theme','theme'))->with('title','Edit Resources');
    }


    public function update($theme_id, $sub_theme_id, $resource_group_id, $resource_id, Request $request)
    {
        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $sub_theme = $theme->subTheme->find($sub_theme_id);

        if (!$sub_theme) {
            abort(404);
        }

        $resource_group = $sub_theme->resourceGroup->find($resource_group_id);

        if (!$resource_group) {
            abort(404);
        }

        $resource = $resource_group->resource->find($resource_id);

        if (!$resource) {
            abort(404);
        }

        $this->validate($request,[
            'title'=>'required',
            'resource'=>'sometimes',
            'resource_group_id'=>'sometimes'
        ],[
            'name.required'=>'The Sub theme name is required!'
        ]);

        $resource_image = $resource->resource;

        if ($request->hasFile('resource')) {
            $destinationPath = 'image/resources/';
            $image = str_random(6) . '_' . time() . "-" .
                $request->file('resource')->getClientOriginalName();
            $block_image = $request->file('resource')->move($destinationPath, $image);
            $resource_image = str_replace('\\','/', $block_image);
        }

        $resource->update([
            'title' => $request->input('title'),
            'resource' => $resource_image
        ]);

        session()->flash('message','Resource (' . $resource->title . ') is updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id)
    {
        if(!request()->ajax()){
            return false;
        }

        $resource = Resource::find($id);

        $resource->delete();

        session()->flash('message', 'Content Deleted.');

        return response()->json(array(
            'status' => 'success',
            'url'    => url('admin/resource')
        ));
    }

    /**
     * @param $theme_id
     * @param $sub_theme_id
     * @param $resource_group_id
     * @param $resource_id
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function delete($theme_id, $sub_theme_id, $resource_group_id, $resource_id, Request $request ){

        if(!request()->ajax()){
            return false;
        }

        $theme = Theme::find($theme_id);

        if (!$theme) {
            abort(404);
        }

        $sub_theme = $theme->subTheme->find($sub_theme_id);

        if (!$sub_theme) {
            abort(404);
        }

        $resource_group = $sub_theme->resourceGroup->find($resource_group_id);

        if (!$resource_group) {
            abort(404);
        }

        $resource = $resource_group->resource->find($resource_id);

        if (!$resource) {
            abort(404);
        }

        $resource->delete();

        session()->flash('message','Content'. '( '. $resource->title .' )' . ' has been Deleted.');

        return response()->json(array(
            'status' => 'success',
            'url'    => url('admin/theme'.$theme->id.'/sub-theme/'.$sub_theme->id.'/resource-group/'.$resource_group->id)
        ));

        // if( $resource == TRUE ){

        //     $url = app_path('image/resources/'.$resource->resource);

        //     return $url;

        //     $response = File::delete($url);

        //     if( $response == TRUE ){
        //         return 1;
        //     }
        // }

        // return 1;

    }
}
