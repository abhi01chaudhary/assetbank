<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Story;
use App\Models\StoryBank;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Models\ImageBank;
use Validator;
use Input;
use File;
use URL;
use Auth;
use Carbon;
use ZipArchive;
use \RecursiveIteratorIterator;
use \RecursiveDirectoryIterator;
use RecursiveArrayIterator;
use FFMpeg;

class StoryController extends Controller
{
   public function __construct( StoryBank $story )
    {
        $this->middleware('auth');
        $this->story = $story;
    }

    public function create(){

        $now = Carbon\Carbon::now();

        $storyInput = [
            'user_id' => Auth::user()->id,
            'slug' => str_replace([':',' '],'-',$now)
        ];

        $newStory = Story::create($storyInput);

    	return view('admin.story.create', [
            'active_menu' => 'instagram-story'
        ], compact('newStory'))->with('title','Create Story');
    }

    public function approvalBySocialMedia(){

        $stories = Story::where('approved_by_media','Pending')->get();
        
        $data = [];

        foreach($stories as $story){

            if(count($story->bank) == 0){

                $id = $story->id;

                Story::destroy($id);

            }else{

                $data[] = $story;
                
            }
        }

        return view('admin.story-status.sm-approval', [
            'active_menu' => 'instagram-story'
        ], compact('data'))->with('title','Approval By Social Media');
    }

    public function approvedBySM($id){

        if (!request()->ajax()) {
            return false;
        }

        $story = Story::find($id);

        if($story->approved_by_media == 'Pending' || $story->approved_by_media == 'Rejected'){
            $story->approved_by_media = 'Approved';
        }

        $story->update([
            'approved_by_media'=>$story->approved_by_media,
            'approved_by_gtm'=>'Pending'
        ]);

        session()->flash('message', 'Story Approved.');

        return response()->json(array(
            'status' => 'success',
        ));
    }

    public function rejectedStoryByMedia(){
        $stories = Story::where('approved_by_media','Rejected')->latest()->get();
        return view('admin.story-status.rejected-by-media', [
            'active_menu' => 'instagram-story'
        ], compact('stories'))->with('title','Rejected Story by social media ');
    }

     public function rejectedBySM($id){

        if (!request()->ajax()) {
            return false;
        }

        $story = Story::find($id);

        if($story->approved_by_media == 'Pending' || $story->approved_by_media == 'Approved'){
            $story->approved_by_media = 'Rejected';
        }

        $story->update([
            'approved_by_media'=>$story->approved_by_media,
        ]);

        session()->flash('message', 'Story Rejected.');

        return response()->json(array(
            'status' => 'success',
        ));
    }

    public function approvalByGTM(){

        $stories = Story::where('approved_by_media','Approved')
                   ->where('approved_by_gtm','Pending')
                   ->get();

        return view('admin.story-status.gtm-approval', [
            'active_menu' => 'instagram-story'
        ], compact('stories'))->with('title','Approval By Brand');
    }

    public function approvedByGTM($id){

        if (!request()->ajax()) {
            return false;
        }

        $story = Story::find($id);

        if($story->approved_by_gtm == 'Pending' || $story->approved_by_gtm == 'Rejected'){
             $story->approved_by_gtm = 'Approved';
        }

        $story->update([
            'approved_by_brand'=> $story->approved_by_gtm
        ]);

        session()->flash('message', 'Story Approved.');

        return response()->json(array(
            'status' => 'success',
        ));
    }


    public function rejectedByGTM($id){

        if (!request()->ajax()) {
            return false;
        }

        $story = Story::find($id);

        if($story->approved_by_gtm == 'Pending' || $story->approved_by_gtm == 'Approved'){
            $story->approved_by_gtm = 'Rejected';
        }

        $story->update([
            'approved_by_gtm'=> $story->approved_by_gtm
        ]);

        session()->flash('message', 'Story Rejected.');

        return response()->json(array(
            'status' => 'success',
        ));
    }

    public function rejectedStoryByGTM(){

        $stories = Story::where('approved_by_media','Approved')->where('approved_by_gtm','Rejected')->latest()->get();

        return view('admin.story-status.rejected-by-gtm', [
            'active_menu' => 'instagram-story'
        ], compact('stories'))->with('title','Rejected Image by gtm');
    }


    public function index(){

        $stories = Story::latest()->get();

        $data = [];

        foreach($stories as $story){

            if(count($story->bank) == 0){

                $id = $story->id;

                Story::destroy($id);

            }else{

                $data[] = $story;
                
            }
            
        }

        return view('admin.story.stories', [
            'active_menu' => 'instagram-story'
        ], compact('data'))->with('title','Story Collection');
    }

    public function viewStory($id){

        $stories = Story::find($id);

        return view('admin.story.index', [
            'active_menu' => 'instagram-story'
        ], compact('stories'))->with('title','All Stories');
        
    }

    public function store(Request $request){
      
    	$input = $request->all();

        $validator = Validator::make( $input, [
            'file' => 'required|mimes:jpeg,png,jpg,gif,psd,tiff,svg,mp4,mov|max:4000000',
            'title' => 'required|min:3',
            'tags' => 'required',
            'source' => 'required',
            'global_rights' => 'required',
            'order'=>'required|numeric'
        ]);


        if ($validator->fails()) {

            $errors = $validator->errors();

            $output = [[
                'error' => $errors->first('file'),
                'title_error' => $errors->first('title'),
                'tags_error' => $errors->first('tags'),
                'source_error' => $errors->first('source'),
                'order_error'=>$errors->first('order')
            ]];

            return ['files' => $output ];

        }

        if ($validator->passes()) {
            $storyId            = $request['story_id'];
            $title              = $request->title;
            $tags               = $request->tags;
            $source             = $request->source;
            $product_featured   = $request->product_featured;
            $global_rights      = $request->global_rights;
            $order              = $request->order;
            
            $findStory = Story::find($storyId);

            $path = 'Story-'.$storyId.'-'.$findStory->slug;

            $destinationPath = 'storage/backend/stories/bank/'.$path;

            $file = $request->file('file');

            $file_type = $file->getMimeType();

            $story = str_random(6) . '_' .time().'.'.$request->file('file')->getClientOriginalExtension();

            $inputs['file_name'] = $request->file('file')->move($destinationPath, $story);
            $inputs['file_name'] = str_replace('\\','/',$inputs['file_name']);

            $size = filesize($destinationPath.'/'.$story);
            $imageWidth = $size[0];

            $this->story->story_id    = $storyId;
            $this->story->story       = $story;
            $this->story->file_name   = $file->getClientOriginalName();
            $this->story->file_type   =	$file_type;
            $this->story->title       = $title;
            $this->story->tags        = $tags;
            $this->story->source      = $source;
            $this->story->order       = $order ;      
            $this->story->product_featured  = $product_featured;
            $this->story->global_rights  = $global_rights;

            $this->story->save();

            $ext = $file->getClientOriginalExtension();
            if($ext == 'psd'){
                $thumbnailUrl = url('/').'/storage/backend/stories/bank/psd.jpg';
            }elseif($ext == 'tif'){
                $thumbnailUrl = url('/').'/storage/backend/stories/bank/tif.png';
            }elseif($ext == 'mp4' || $ext == 'mov' || $ext == 'MOV'){
                $thumbnailUrl = url('/').'/storage/backend/stories/bank/video.png';
                // return $thumbnailUrl;
            }else{
                $thumbnailUrl =  URL::to( $inputs['file_name'] );
             }
            
            $output = [[
                'name' => $file->getClientOriginalName(),
                'title' => $title,
                'tags' => $tags,
                'source' => $source,
                'product_featured' => $product_featured,
                'global_rights' => $global_rights,
                'order'=>$order,
                'imageWidth'=>$imageWidth,
                'size' => $size,
                'type' => 'story/jpeg',
                'url'  => URL::to( $inputs['file_name'] ),
                'thumbnailUrl' => $thumbnailUrl,
                'deleteUrl'=>URL::to( '/admin/story/delete?name='.$story ),
                'deleteType'=>'DELETE',
                'message'=>'Thank you for uploading your Social Media stories! High Five!Your storys have been submitted for approval.'
            ]];

            return ['files' => $output ];
        }
    }

    public function show(){

    	$stories = StoryBank::get();

        foreach ($stories as $key => $story) {
            
            $output[] = [
                'name' => $story->file_name,
                'title' => $story->title,
                'tags' => $story->tags,
                'source' => $story->source,
                'product_featured' => $story->product_featured,
                'global_rights' => $story->global_rights,
                'size' => 2345,
                'type' => 'story/jpeg',
                'url'  => URL::to( 'storage/backend/story/bank/'.$story->story ),
                'thumbnailUrl' => URL::to( 'storage/backend/story/bank/'.$story->story ),
                'deleteUrl'=>URL::to( '/admin/story/delete?name='.$story->story ),
                'deleteType'=>'DELETE',
            
            ];
        }

        $output = [];

        return ['files' => $output ];
    }

    public function edit($id){
        $story = Story::find($id);
        return view('admin.story-status.edit', [
            'active_menu' => 'instagram-story'
        ], compact('story'))->with('title','Edit Story');
    }

    public function update(Request $request, $id){

        $story = StoryBank::find($id);

        $this->validate($request,[
            'title' => 'required|min:3',
            'tags' => 'required',
            'source' => 'required',
        ]);

        $story->update($request->all());

        session()->flash('message','Story Updated');

        return redirect()->back();
    }

    public function removeStory($id){

        $story = story::find($id);

        File::delete('storage/backend/images/bank/'.'Story-'.$story->id.'-'.$story->slug.'/'.$story->story);

        $story->delete();

        session()->flash('message', 'Story Deleted.');

        return response()->json(['status'=>"success"]);
    }


    public function deleteBulkStory(Request $request){

        $ids = $request->ids;

        $stories = Story::whereIn('id',explode(",",$ids))->get();

        foreach($stories as $story){

            $storyNoBank = Story::find($story->id);

            if( count($story->bank) != 0 ){

                foreach($story->bank as $data){

                    $datum = StoryBank::find($data->id);

                    File::delete('storage/backend/images/bank/'.'Story-'.$story->id.'-'.$story->slug.'/'.$data->story);

                    $datum->delete();

                }

            }

            $storyNoBank->delete();

        }

        session()->flash('message', 'Selected Story Deleted.');
        
        return response()->json(['status'=>"success"]);
    }

    public function getDownloadableStory(){
        $stories = Story::where('approved_by_media','approved')
                    ->where('approved_by_gtm','approved')
                    ->get();
        return view('admin.story-status.downloadable-story', [
            'active_menu' => 'instagram-story'
        ], compact('stories'))->with('title','Downloadable Story');

    }

     public function downloadStory($id){

        $story = Story::find($id);

        foreach($story->bank as $data){

            $ids[] = $data->id;

        }

        $files  = StoryBank::whereIn('id', $ids)->get();

        $public_dir     = realpath(base_path().'/..');

        $download_id    = str_random(6) . '_' .time();
        
        $zip_file_name  = "story-{$download_id}.zip";

        $zip = new ZipArchive();

        if ( $zip->open( $public_dir . '/storage/downloads/' . $zip_file_name, ZipArchive::CREATE ) === TRUE ) {

            foreach( $files as $file ) {

                $zip->addFile( 'storage/backend/stories/bank/'.'Story-'.$story->id.'-'.$story->slug.'/'.$file->story , $file->story );
            }

            $zip->close();
        }

         // Set Header
        $headers = array(

            'Content-Type' => 'application/octet-stream',
        );

        $file_to_path  = 'storage/downloads/'.$zip_file_name;

        
        if( file_exists( $file_to_path ) ){

            return response()->download( $file_to_path, $zip_file_name, $headers );

        }else{

            return 'Download failed due to some error.';

        } 
         
    }

    public function fileUnlink( Request $request ){

        $story = StoryBank::where('story','=',$request->name)->delete();

        if( $story == TRUE ){

            $response = File::delete( 'storage/backend/story/bank/'.$request->name );
     
            if( $response == TRUE ){
                return 1;
            }
        }
       
        return 0;
        
    }


    public function storyOrder(Request $request){

        $rows = $request['data'];
      
        $i=1;

        foreach($rows as $row){

            $story = StoryBank::find($row);

            $story->update([
                'order'=> $i
            ]);

            $i++; 
        }  
    }


}
