<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Input;
use File;
use Image;
use URL;
use Auth;
use App\Models\ImageBank;
use Validator;


class ImageBankController extends Controller
{
    public function __construct( ImageBank $image )
    {

        $this->middleware('auth');
        $this->image = $image;
    }

    public function index(){
        $images = ImageBank::latest()->get();
        return view('admin.asset-status.all-image', [
            'active_menu' => 'image'
        ], compact('images'))->with('title','All Images');
    }

    public function approvalBySocialMedia(){

        $images = ImageBank::where('approved_by_media','Pending')->get();
        return view('admin.asset-status.sm-approval', [
            'active_menu' => 'image'
        ], compact('images'))->with('title','Approval Social Media');
    }

    public function approvedBySM($id){
        if (!request()->ajax()) {
            return false;
        }

        $asset = ImageBank::find($id);

        $asset->update([
            'approved_by_media'=>'Approved',
            'approved_by_gtm'=>'Pending'
        ]);

        session()->flash('message', 'Image Approved.');

        return response()->json(array(
            'status' => 'success',
        ));
    }

    public function rejectedBySM($id){
        if (!request()->ajax()) {
            return false;
        }

        $asset = ImageBank::find($id);

        $asset->update([
            'approved_by_media'=>'Rejected',
        ]);

        session()->flash('message', 'Image Rejected.');

        return response()->json(array(
            'status' => 'success',
        ));
    }

    public function approvalByGTM(){

        $images = ImageBank::where('approved_by_media','Approved')
                   ->where('approved_by_gtm','Pending')
                   ->get();
        return view('admin.asset-status.gtm-approval', [
            'active_menu' => 'image'
        ], compact('images'))->with('title','Approval GTM');
    }

    public function approvedByGTM($id){
        if (!request()->ajax()) {
            return false;
        }

        $asset = ImageBank::find($id);

        $asset->update([
            'approved_by_gtm'=>'Approved'
        ]);

        session()->flash('message', 'Image Approved.');

        return response()->json(array(
            'status' => 'success',
        ));
    }

    public function rejectedByGTM($id){
        if (!request()->ajax()) {
            return false;
        }

        $asset = ImageBank::find($id);

        $asset->update([
            'approved_by_gtm'=>'Rejected'
        ]);

        session()->flash('message', 'Image Rejected.');

        return response()->json(array(
            'status' => 'success',
        ));
    }


    public function create(){

        return view('admin.asset.create', [
            'active_menu' => 'image'
        ])->with('title','Upload Image');
    }

    public function show(){

        $images = ImageBank::get();

        foreach ($images as $key => $image) {
            
            $output[] = [
                'name' => $image->file_name,
                'title' => $image->title,
                'tags' => $image->tags,
                'source' => $image->source,
                'product_featured' => $image->product_featured,
                'global_rights' => $image->global_rights,
                'size' => 2345,
                'type' => 'image/jpeg',
                'url'  => URL::to( 'storage/backend/images/bank/'.$image->image ),
                'thumbnailUrl' => URL::to( 'storage/backend/images/bank/'.$image->image ),
                'deleteUrl'=>URL::to( '/admin/image/delete?name='.$image->image ),
                'deleteType'=>'DELETE',
            ];
        }

        $output = [];

        return ['files' => $output ];
    }

    public function store(Request $request){

        $input = $request->all();

        $validator = Validator::make( $input, [
            'file' => 'required|mimes:jpeg,png,jpg,gif,psd,tiff,svg|dimensions:min_width=1080,min_height=1080',
            'title' => 'required|min:3',
            'tags' => 'required',
            'source' => 'required',
            'global_rights' => 'required',
        ],[
            'file.dimensions' => 'The image dimension of the file must be a minimum of 1080px x 1080px'
        ]);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $output = [[
                'error' => $errors->first('file'),
                'title_error' => $errors->first('title'),
                'tags_error' => $errors->first('tags'),
                'source_error' => $errors->first('source'),
            ]];
            return ['files' => $output ];

        }

        if ($validator->passes()) {


            $title              = $request->title;
            $tags               = $request->tags;
            $source             = $request->source;
            $product_featured   = $request->product_featured;
            $global_rights      = $request->global_rights;


            $file = $request->file('file');

            $destinationPath = 'storage/backend/images/bank/';

            $image = str_random(6) . '_' .time().'.'.$request->file('file')->getClientOriginalExtension();

            $inputs['file_name'] = $request->file('file')->move($destinationPath, $image);
            $inputs['file_name'] = str_replace('\\','/',$inputs['file_name']);

            $size = filesize($destinationPath.'/'.$image);

            $this->image->user_id     = Auth::user()->id;
            $this->image->image       = $image;
            $this->image->file_name   = $file->getClientOriginalName();
            $this->image->title       = $title;
            $this->image->tags        = $tags;
            $this->image->source      = $source;
            $this->image->product_featured  = $product_featured;
            $this->image->global_rights  = $global_rights;

            $this->image->save();
            $ext = $file->getClientOriginalExtension();

            if($ext == 'psd'){
                $thumbnailUrl = url('/').'/storage/backend/images/bank/psd.jpg';
            }elseif($ext == 'tif'){
                $thumbnailUrl = url('/').'/storage/backend/images/bank/tif.png';
            }else{
                $thumbnailUrl =  URL::to( $inputs['file_name'] );
            }
            $output = [[
                'name' => $file->getClientOriginalName(),
                'title' => $title,
                'tags' => $tags,
                'source' => $source,
                'product_featured' => $product_featured,
                'global_rights' => $global_rights,
                'size' => $size,
                'type' => 'image/jpeg',
                'url'  => URL::to( $inputs['file_name'] ),
                'thumbnailUrl' => $thumbnailUrl,
                'deleteUrl'=>URL::to( '/admin/image/delete?name='.$image ),
                'deleteType'=>'DELETE',
                'message'=>'Thank you for uploading your Social Media images! High Five!Your images have been submitted for approval. '
            ]];

            return ['files' => $output ];
        }


    }


    public function fileUnlink( Request $request ){

        $image = ImageBank::where('image','=',$request->name)->delete();

        if( $image == TRUE ){

            $response = File::delete( 'storage/backend/images/bank/'.$request->name );
     
            if( $response == TRUE ){
                return 1;
            }
        }
       
        return 0;
        
    }

    public function rejectedImageByMedia(){
        $images = ImageBank::where('approved_by_media','Rejected')->latest()->get();
        return view('admin.asset-status.rejected-by-media', [
            'active_menu' => 'image'
        ], compact('images'))->with('title','Rejected Image by social media ');
    }

    public function rejectedImageByGTM(){
        $images = ImageBank::where('approved_by_media','Approved')->where('approved_by_gtm','Rejected')->latest()->get();
        return view('admin.asset-status.rejected-by-gtm', [
            'active_menu' => 'image'
        ], compact('images'))->with('title','Rejected Image by gtm');
    }

    public function getDownloadableImage(){
        $images = ImageBank::where('approved_by_media','approved')
                    ->where('approved_by_gtm','approved')
                    ->get();
        return view('admin.asset-status.downloadable-image', [
            'active_menu' => 'image'
        ], compact('images'))->with('title','Downloadable Image');

    }

    public function deleteBulkImage(Request $request){
        $ids = $request->ids;
        $banks = ImageBank::whereIn('id',explode(",",$ids))->get();
        foreach($banks as $bank){
            File::delete('storage/backend/images/bank/'.$bank->image);
            $bank->delete();
        }
        session()->flash('message', 'Selected Image Deleted.');
        return response()->json(['status'=>"success"]);
    }

    public function removeImage($id){
        $image = ImageBank::find($id);
        File::delete('storage/backend/images/bank/'.$image->image);
        $image->delete();
        session()->flash('message', 'Image Deleted.');
        return response()->json(['status'=>"success"]);
    }

    public function downloadImage($id){
        $image = ImageBank::find($id);
        $filePath = $image->image;
        return Response::download('storage/backend/images/bank/'.$filePath,$image->file_name);
    }

    public function uploadedByLocalMarket($id){
        $images = ImageBank::where('user_id',$id)->latest()->get();
        return view('admin.asset-status.market-upload', [
            'active_menu' => 'image'
        ], compact('images'))->with('title','Image uploaded by market');
    }

    public function edit($id){
        $image = ImageBank::find($id);
        return view('admin.asset-status.edit', [
            'active_menu' => 'image'
        ], compact('image'))->with('title','Edit Asset Upload');
    }

    public function update(Request $request,$id){

        $image = ImageBank::find($id);

        $this->validate($request,[
            'title' => 'required|min:3',
            'tags' => 'required',
            'source' => 'required',
        ]);

        $image->update($request->all());

        session()->flash('message','Asset Upload Updated');
        
        return redirect($request->url);
    }
}
