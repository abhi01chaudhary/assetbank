<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function getRegister()
    {
        $countries = Country::pluck('name', 'id');
        return view('admin.dashboard.sign-up', compact('countries'))->with('title', 'Sign Up');
    }

    public function postRegister(Request $request)
    {
        if (!$request->ajax()) {
            return false;
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'full_name' => 'required',
            'country_id' => 'required',
            'g-recaptcha-response' => 'required',
        ],[
                'g-recaptcha-response.required' => 'Captcha is required'
            ]);

        if ($validator->fails()) {
            return response()->json(array(
                'status' => 'fails',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($validator->passes()) {
            $inputs = $request->all();
            $inputs['password'] = bcrypt($request->password);
            $name = $request->full_name;
            $email = $request->email;
            User::create([
                'full_name' => $request->full_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'contact_number' => $request->contact_number,
                'user_role_id' => 4,
                'country_id' => $request->country_id,
                'status' => 0
            ]);
            Mail::send('admin.partials.local-market-account-created',
                ['name' => $name,
                    'email' => $email,
                ],
                function ($message)
                use ($email, $name) {
                    $message->to($email)->subject('Account Created');
                });
            $emailOfAdmins = User::where('user_role_id',1)->get();
            foreach($emailOfAdmins as $admin){
                $adminEmail = $admin->email;
                $adminName = $admin->full_name;
                Mail::send('admin.partials.account-approval-request',
                    [   'name' => $name,
                        'adminEmail' => $adminEmail,
                        'email'=>$email,
                        'adminName'=>$adminName
                    ],
                    function ($message)
                    use ($adminEmail,$name,$email,$adminName) {
                        $message->to($adminEmail)->subject('Approve New Upload Hub User');
                    });
            }

            session()->flash('message', 'Thank you. We will review and confirm your registration process');
            return response()->json(array(
                'status' => 'success',
                'url' => url('user/register')
            ));
        }
    }

    public function getPasswordResetForm()
    {
        return view('admin.password.forget')->with('title', 'Forget Password');
    }

    public function sendResetEmail(Request $request)
    {
        if (!request()->ajax()) {
            return view('errors.404');
        }


        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {

            return response()->json(array(
                'status' => 'fails',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($validator->passes()) {

            $email = $request->email;

            $user = User::where('email', $request->email)->first();

            if ($user && $user->status == 1) {

                $remember_token = md5(time() . mt_rand(0, 900));

                $name = $user->full_name;

                $user->update([
                    'confirmation_code' => $remember_token
                ]);

                $link = '/password/reset/' . $remember_token;

                Mail::send('admin.partials.sendForgetEmail',
                    ['name' => $name,
                        'email' => $email,
                        'link' => $link
                    ],
                    function ($message)
                    use ($email, $name, $link) {
                        $message->to($email)->subject('Password Reset');
                    });


                session()->flash('message', 'Check Your Email For Password Resetting');

                return response()->json(array(
                    'status' => 'success',
                ));

            } else {
                $validator->errors()->add('email',
                    'Sorry, No such email is registered or your email is not activated yet');
                return response()->json(array(
                    'status' => 'fails',
                    'errors' => $validator->getMessageBag()->toArray()
                ));
            }


        }


    }

    public function getPasswordLink($remember_token)
    {
        $user = User::where('confirmation_code', $remember_token)->first();

        if (count($user) > 0 && $remember_token == $user->confirmation_code) {

            $email = $user->email;

            return view('admin.password.reset', compact('email', 'remember_token'))->with('title', 'Password Reset');
        }

        Auth::logout();

        return view('errors.404');
    }

    public function passwordReset(Request $request)
    {

        if (!request()->ajax()) {
            return view('errors.404');
        }


        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|alpha_dash|confirmed',
            'password_confirmation' => 'required|min:6|alpha_dash'
        ]);

        if ($validator->fails()) {

            return response()->json(array(
                'status' => 'fails',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($validator->passes()) {

            $user = User::where('email', $request->email)->first();

            $user->update([
                'password' => bcrypt($request->password)
            ]);

            $user->update([
                'confirmation_code' => ''
            ]);


            session()->flash('message', 'Password Reset Successfull!! Please Login');

            return response()->json(array(
                'status' => 'success',
                'url' => url('/')
            ));

        }


    }

    public function RegisterToolboxUser(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'full_name' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        if($validator->passes()){

            $inputs = $request->all();

            $inputs['password'] = bcrypt($request->password);

            $name = $request->full_name;

            $email = $request->email;

            User::create([
                'full_name' => $request->full_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'user_role_id' => 5,
                'status' => 1
            ]);
            
            session()->flash('message', 'Thank you. You are registered now!');
            
            return redirect()->back();
            
        }
      
    }
}
